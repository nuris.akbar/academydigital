<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@index');


// =================== Proses Belanja ========================
Route::get('cart','CartController@index');
Route::post('cart','CartController@store');
Route::post('cart/checkout','CartController@checkout');
Route::delete('cart/{id}','CartController@destroy');
// =================== End Proses Belanja =====================

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Auth::routes();
Route::get('courses','CourseController@index');
Route::get('course/{slug}','CourseController@show');
Route::get('page/verification','PageController@verification');



Route::get('play/{courseSlug}/{lessonSlug?}','CourseController@play');



// ======================== Halaman Admin =================================
Route::prefix('adminzone')->name('adminzone.')->group(function () {
    Route::resource('category','Admin\CategoryController');
    Route::resource('discount','Admin\DiscountController');
    Route::resource('course','Admin\CourseController');
    Route::get('order','Admin\OrderController@index')->name('order.index');
    Route::get('order/{invoice?}', 'Admin\OrderController@show')->where('invoice', '(.*)')->name('order.show');
    Route::put('order/{invoice?}', 'Admin\OrderController@update')->where('invoice', '(.*)')->name('order.update');
    Route::get('user/{id}/orders','Admin\UserController@orders')->name('user.orders');
    Route::get('user/member','Admin\UserController@member')->name('user.member');
    Route::get('user/{id}/follow','Admin\CourseController@follow')->name('course.follow');
    Route::resource('user','Admin\UserController');
    Route::resource('section','Admin\SectionController');
    Route::resource('lesson','Admin\LessonController');
    Route::get('login','Admin\AuthController@formLogin');
    Route::post('login','Admin\AuthController@login')->name('login');
    Route::get('logout','Admin\AuthController@logout')->name('logout');
    Route::get('dashboard','Admin\PageController@dashboard');
});
// ======================== End Halaman Admin =================================

// ======================== Endpoint API  =================================
Route::prefix('api')->name('api.')->group(function () {
    Route::get('course','Api\CourseController@index');
    Route::get('course/{slug}','Api\CourseController@show');
});
// ======================== End Endpoint API =================================


Route::get('/home', 'HomeController@index')->name('home');
