import Vue from 'vue'
import App from './App.vue'

import Router from 'vue-router'
import Home from './pages/Home.vue'
import Courses from './pages/Courses.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/courses',
            name: 'courses',
            component: Courses,
        }
    ]
});

new Vue({
    el: '#app',
    router,
    components: {
        App
    }
})