<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://static.datacamp.com/styles.d9ac58b605bc2c5048c3.css" rel="stylesheet">
    <meta name="generator" content="Gatsby 2.19.27">
    <title>Data Science Courses: R &amp; Python Analysis Tutorials | DataCamp</title>
    <link data-react-helmet="true" rel="manifest" href="https://www.datacamp.com/manifest.json">
    <link data-react-helmet="true" rel="shortcut icon" type="image/x-icon" href="https://static.datacamp.com/static/favicon-d811b61119bd98ec2518939e56e7bf08.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script src="https://connect.facebook.net/signals/config/286618111707433?v=2.9.15&amp;r=stable" async=""></script>
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script>
    <script type="text/javascript" src="//bat.bing.com/bat.js"></script>
    <script type="text/javascript" src="https://snap.licdn.com/li.lms-analytics/insight.min.js"></script>
    <script type="text/javascript"src="https://www.google-analytics.com/analytics.js"></script>
    <script src="https://www.googletagmanager.com/gtm.js?id=GTM-TGGWB2P"></script>
    <script src="//cdn.datacamp.com/sp/2.10.2.js"></script>
    <link rel="canonical" href="https://www.datacamp.com/courses/" data-baseprotocol="https:" data-basehost="www.datacamp.com">
    <script src="https://compliance.datacamp.com/base.js" type="text/javascript" async=""></script>
    <link as="script" rel="preload" href="https://static.datacamp.com/webpack-runtime-121c5ac5e0e18e603c04.js">
    <link as="script" rel="preload" href="https://static.datacamp.com/app-45104b863df799fb691d.js">
    <link as="script" rel="preload" href="https://static.datacamp.com/styles-34324f605e168d25bcfe.js">
    <link as="script" rel="preload" href="https://static.datacamp.com/commons-2ded691a13a7ca543921.js">
    <link as="script" rel="preload" href="https://static.datacamp.com/component---src-pages-courses-jsx-d992502e7d99958b6efe.js">
    <link as="fetch" rel="preload" href="https://www.datacamp.com/page-data/courses/page-data.json" crossorigin="anonymous">
    <link as="fetch" rel="preload" href="https://static.datacamp.com/page-data/app-data.json" crossorigin="anonymous">
    <script src="https://cdn.onesignal.com/sdks/OneSignalPageSDKES6.js?v=150801" async=""></script>
    <script charset="utf-8" src="https://static.datacamp.com/55-e0ad6bb09ac78e091f18.js"></script>
    <script src="https://compliance.datacamp.com/scripts/gtm.js" async=""></script>
    <style data-emotion-css="1ig1800">.css-1ig1800{padding-bottom:64px;padding-top:64px;}.css-1ig1800 > *{padding-bottom:16px;padding-top:16px;}@media screen and (min-width:768px){.css-1ig1800{-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;}.css-1ig1800 > *:first-child{margin-right:16px;}.css-1ig1800 > *:last-child{margin-left:16px;}}</style>
    <style data-emotion-css="1s0ruqf">.css-1s0ruqf{color:#ffffff;max-width:450px;}.css-1s0ruqf h1,.css-1s0ruqf h2,.css-1s0ruqf h3,.css-1s0ruqf h4,.css-1s0ruqf h5,.css-1s0ruqf h6{color:#ffffff;margin:0;}</style>
    <style data-emotion-css="1b3f1in">.css-1b3f1in{margin:-32px auto 0;max-width:1140px;padding:0 16px;position:relative;z-index:20;}@media screen and (min-width:1200px){.css-1b3f1in{padding:0;}}</style>
    <style data-emotion-css="lsu2gc">.css-lsu2gc{height:32px;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;left:24px;position:absolute;top:23px;width:32px;}.css-lsu2gc > span{position:relative;top:-6px;}</style>
    <style data-emotion-css="w24rtu">.css-w24rtu{background-color:#ffffff;height:77px;font-size:1.25rem;text-indent:40px;}@media screen and (min-width:480px){.css-w24rtu{font-size:1.5rem;text-indent:48px;}}</style>
    <style data-emotion-css="kxr9d5">.css-kxr9d5{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end;position:absolute;right:24px;top:24px;}@media screen and (max-width:992px){.css-kxr9d5{position:relative;right:12px;top:12px;}}</style>
    <style data-emotion-css="cssveg">.css-cssveg{position:relative;}</style>
    <style data-emotion-css="6incze">.css-6incze{padding:64px 0 32px;}</style>
    <style data-emotion-css="1k1n1og">.css-1k1n1og{margin:0 0 32px 0;text-align:center;}</style>
    <style data-emotion-css="1ewp9ay">.css-1ewp9ay{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;}.css-1ewp9ay > a{margin:36px 16px 16px 16px;}</style>
    <style data-emotion-css="179cao1">.css-179cao1{border:1px solid #e6eaeb;border-radius:4px;color:#4d5356;font-weight:400;margin-top:20px;max-width:358px;width:inherit;position:relative;text-align:center;-webkit-transition:-webkit-transform 0.3s cubic-bezier(0.77,0,0.175,1);-webkit-transition:transform 0.3s cubic-bezier(0.77,0,0.175,1);transition:transform 0.3s cubic-bezier(0.77,0,0.175,1);}.css-179cao1:active,.css-179cao1:focus,.css-179cao1:hover{border-bottom-color:#e6eaeb;color:#4d5356;-webkit-transform:translate(0,-4px);-ms-transform:translate(0,-4px);transform:translate(0,-4px);z-index:10;}@media screen and (min-width:768px){.css-179cao1{max-width:358px;}}</style>
    <style data-emotion-css="11khr5q">.css-11khr5q{left:calc(50% - 20px);position:absolute;top:-20px;}</style>
    <style data-emotion-css="1hi4crg">.css-1hi4crg{min-height:216px;margin:40px 24px 0;}</style>
    <style data-emotion-css="1a7hu85">.css-1a7hu85{margin:0;min-height:56px;}</style>
    <style data-emotion-css="z6sjuf">.css-z6sjuf{margin:24px 0;min-height:72px;}@media screen and (min-width:768px){.css-z6sjuf{min-width:inherit;}}</style>
    <style data-emotion-css="4acvv2">.css-4acvv2{-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;}</style>
    <style data-emotion-css="3guzbk">.css-3guzbk{-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;background-color:#ebf4f7;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;height:112px;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;padding:0 32px;text-align:left;}</style>
    <style data-emotion-css="3xxt4t">.css-3xxt4t{border-radius:50%;margin-right:12px;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;}</style>
    <style data-emotion-css="1ogvh3l">.css-1ogvh3l{font-size:16px;margin-bottom:4px;text-transform:uppercase;}</style>
    <style data-emotion-css="f85llx">.css-f85llx{margin:0;font-size:0.875rem;}</style>
    <style> .card{} .card:hover{margin-top: -1em} </style>
    <style data-emotion="css"></style>
    <meta property="og:url" content="www.datacamp.com/courses/" data-react-helmet="true">
  
  </head>
  <body>
    <div id="___gatsby">
      <div style="outline:none" tabindex="-1" id="gatsby-focus-wrapper">
        <div class=" site-wrap">
          <header class="dc-header null">
            <div class="container dc-u-pos-relative">
              <div class="dc-u-fx dc-u-fx-jcsb js-nav-container dc-u-h-48">
                <div class="navbar-with-small-logo dc-u-fx dc-u-fxi-fg-1 dc-u-fx-aic">
                  <div class="logo-block header__logo-block logo-block--without-hiring dc-mv-auto">
                    <span class="dc-icon dc-icon--primary dc-icon--size-64 logo-block__img-small">
                      <svg class="dc-icon__svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="#FFF" d="M5 3h12v16H5z"></path>
                        <path d="M16.788 13.04c.185.312.238.312-.107.629.265.229.265.416-.131.594.009.635.138 1.201-.675 1.25-.812.05-1.844-.49-2.11.964-.268 1.454-.199.909-.278 1.274h-5.74c.715-1.95 1.32-4.104 0-5.162-1.318-1.058-3.487-5.496.782-7.77a6.275 6.275 0 0 1 1.528-.57l-.178-.475.055-.022c.019-.01 1.836-.7 3.41.064l.051.025-.188.428c1.013.226 1.77.597 2.048.77.636.397 1.481 1.016 1.56 1.608.08.593-.687.294-.687.294.476 1.146.502 1.966.29 2.839-.21.872.979 1.767.95 2.407-.078.29-.634.398-.634.398s-.132.145.054.455zM11.93.04a.741.741 0 0 0-.482 0L1.493 3.544a.732.732 0 0 0-.489.771l1.403 13.262c.026.24.165.451.375.568l8.663 4.764a.735.735 0 0 0 .716-.004l8.446-4.766a.738.738 0 0 0 .37-.549L22.59 4.327a.74.74 0 0 0-.485-.785L11.93.04zm-1.173 8.405a.945.945 0 1 1 1.62-.974.945.945 0 0 1-1.62.974zm4.807-1.356c-.358-1.403-1.529-2.033-1.529-2.033l-.199.276c-.248-.254-.848-.524-.89-.541l.155-.422a.315.315 0 0 0-.09-.05c-.904-.336-1.734-.34-2.865-.015l.534 1.418c-.76.243-1.321.91-1.38.986l-.741-.524c-.409.536-.7 1.504-.7 2.25h-.952c.009.669.13 1.836.933 2.805l.705-.535c.648.94 1.997 1.622 3.121 1.507l-.016-1.955.015.026c.285.083.873-.095 1.13-.297l.536.71c.573-.187 1.006-1.27 1.05-1.387l.85.29a4.44 4.44 0 0 0 .022-2.4l.31-.109z"></path>
                      </svg>
                    </span>
                    <img class="logo-block__img" alt="AcademyDIgital" src="{{ asset('image/logo.png') }}"><a href="/" class="shim"></a>
                  </div>
                  <div class="dc-u-mr-8 dc-u-fxi-fg-1 dc-maxw-340 dc-u-z-50 dc-navbar-search dc-u-pos-relative ">
                    <div class="dc-u-w-100pc null">
                      <div role="combobox" aria-expanded="false" aria-haspopup="listbox" aria-labelledby="downshift-228-label">
                        <input aria-autocomplete="list" aria-labelledby="downshift-228-label" autocomplete="off" value="" id="downshift-228-input" placeholder="Search" class="dc-input--text dc-navbar-search__input ais-search-box--input">
                        <span class="dc-icon--white dc-icon--size-18 u-spacing-r1 dc-navbar-search__icon">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M9.129 1.742A5.814 5.814 0 0 0 3.322 7.55a5.813 5.813 0 0 0 5.807 5.806 5.812 5.812 0 0 0 5.806-5.806A5.813 5.813 0 0 0 9.13 1.742M5.278 14.038a7.503 7.503 0 0 0 3.851 1.058c4.162 0 7.548-3.386 7.548-7.547C16.677 3.387 13.291 0 9.13 0S1.58 3.387 1.58 7.549c0 2.13.888 4.058 2.313 5.432l-2.72 3.625a.872.872 0 0 0 1.395 1.046l2.71-3.614z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        <div class="dc-navbar-search__overlay js-navbar-search-overlay "></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="dc-u-fx dc-u-fx-aic">
                  <ul class="dc-u-fx-aic dc-u-lst-none dc-u-d-none dc-u-fx@md dc-u-pl-0  dc-u-mt-0 dc-u-mb-0">
                    <li class="header__nav-item dc-u-mh-16 dc-u-p-0 js-learn-nav-link">
                      <a href="#" class="dc-header__nav-link dc-header__nav-link--has-dropdown dc-u-ws-nowrap">Learn</a>
                      <div class="dc-u-pb-4 dc-u-ph-16 dc-u-pt-24 dc-u-pos-absolute dc-u-ta-left dc-u-w-100pc dc-u-ws-nowrap dc-u-z-999 nav-dropdown-wrapper">
                        <div class="dc-u-bgc-white dc-u-brad-all dc-u-bs-xxl dc-u-mt-8">
                          <div class="dc-u-fx">
                            <div class="dc-dropdown--nav__courses dc-u-p-24">
                              <h5 class="dc-chapeau-title dc-u-color-grey-dark dc-u-mb-12">Courses</h5>
                              <ul class="dc-dropdown__list dc-dropdown__list--bordered">
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/courses/intro-to-python-for-data-science">Introduction to Python</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/courses/free-introduction-to-r">Introduction to R</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/courses/introduction-to-data-engineering">Introduction to Data Engineering</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/courses/introduction-to-sql">Introduction to SQL</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/courses/introduction-to-deep-learning-in-python">Introduction to Deep Learning in Python</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/courses/joining-data-in-postgresql">Joining Data in SQL</a></li>
                              </ul>
                              <a class="dc-link--grey-dark dc-u-d-b dc-u-mt-16" href="/courses">See all courses (328)</a>
                            </div>
                            <div class="dc-dropdown--nav__tracks dc-u-p-24 dc-u-bl">
                              <h5 class="dc-chapeau-title dc-u-color-grey-dark dc-u-mb-12">Tracks</h5>
                              <ul class="dc-dropdown__list dc-dropdown__list--bordered">
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fx dc-u-fx-jcsb dc-u-fw-regular" href="/tracks/data-engineer-with-python">Data Engineer with Python<span class="dc-tag dc-tag--hue dc-u-bgc-primary-lightest dc-u-color-grey dc-u-fw-regular dc-show-above-lg">career</span></a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fx dc-u-fx-jcsb dc-u-fw-regular" href="/tracks/foundational-data-skills-for-business-leaders">Data Skills for Business<span class="dc-tag dc-tag--hue dc-u-bgc-primary-lightest dc-u-color-grey dc-u-fw-regular dc-show-above-lg">skills</span></a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fx dc-u-fx-jcsb dc-u-fw-regular" href="/tracks/data-scientist-with-r">Data Scientist  with R<span class="dc-tag dc-tag--hue dc-u-bgc-primary-lightest dc-u-color-grey dc-u-fw-regular dc-show-above-lg">career</span></a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fx dc-u-fx-jcsb dc-u-fw-regular" href="/tracks/data-scientist-with-python">Data Scientist  with Python<span class="dc-tag dc-tag--hue dc-u-bgc-primary-lightest dc-u-color-grey dc-u-fw-regular dc-show-above-lg">career</span></a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fx dc-u-fx-jcsb dc-u-fw-regular" href="/tracks/machine-learning-scientist-with-r">Machine Learning Scientist with R<span class="dc-tag dc-tag--hue dc-u-bgc-primary-lightest dc-u-color-grey dc-u-fw-regular dc-show-above-lg">career</span></a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fx dc-u-fx-jcsb dc-u-fw-regular" href="/tracks/machine-learning-scientist-with-python">Machine Learning Scientist with Python<span class="dc-tag dc-tag--hue dc-u-bgc-primary-lightest dc-u-color-grey dc-u-fw-regular dc-show-above-lg">career</span></a></li>
                              </ul>
                              <div class="dc-u-fx"><a class="dc-link--grey-dark dc-u-mt-16 dc-show-above-lg" href="/tracks/skill">See all skill tracks (43)</a><a class="dc-link--grey-dark dc-u-mt-16 dc-u-bl dc-u-ml-8 dc-u-pl-8 dc-show-above-lg" href="/tracks/career">See all career tracks (14)</a><a class="dc-link--grey-dark dc-u-mt-16 dc-show-below-lg" href="/tracks">See all tracks (57)</a></div>
                            </div>
                            <div class="dc-dropdown--nav__instructors dc-u-p-24 dc-u-bl">
                              <h5 class="dc-chapeau-title dc-u-color-grey-dark dc-u-mb-12">Instructors</h5>
                              <ul class="dc-dropdown__list dc-dropdown__list--bordered">
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/instructors/scavetta">Rick Scavetta</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/instructors/dansbecker">Dan Becker</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/instructors/cwickham">Charlotte Wickham</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/instructors/katharinecc6b90c27e1b40129c2745c9215cc689">Katharine Jarmul</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/instructors/bois">Justin Bois</a></li>
                                <li class="dc-dropdown__item"><a class="dc-dropdown--nav__link dc-u-ph-4 dc-u-pv-8 dc-u-fw-regular" href="/instructors/ilyakipnis">Ilya  Kipnis</a></li>
                              </ul>
                              <a class="dc-link--grey-dark dc-u-d-b dc-u-mt-16" href="/instructors">Meet all instructors (275)</a>
                            </div>
                          </div>
                          <div class="dc-u-bgc-primary-lightest dc-u-brad-b dc-u-ph-8 dc-u-pv-16 dc-u-fx dc-u-fx-jcsb">
                            <div>
                              <a class="dc-link--grey-dark dc-u-ph-16" href="/learn/r-programming">
                                <span class="dc-icon dc-icon--primary dc-icon--size-18 dc-u-mr-8">
                                  <svg class="dc-icon__svg" height="24" viewBox="0 0 24 24" width="24">
                                    <g transform="translate(0.000000, 3.000000)" fill-rule="nonzero">
                                      <path d="M20.3234669,12.3330729 C21.7494352,11.460056 22.5917065,10.1646055 22.5917065,8.39962455 C22.5917065,4.6927825 18.8741884,3.05596855 13.8368091,3.05596855 C8.79939653,3.05596855 4.715768,5.44840683 4.715768,8.39962455 C4.715768,10.3644831 6.52588373,12.0816563 9.2224536,13.0100051 L9.2224536,15.4224672 C3.9342868,14.6061929 0,11.5106006 0,7.81647005 C0,3.49954143 5.37260333,0 12,0 C18.62743,0 24,3.49954143 24,7.81647005 C24,9.83467322 22.8257283,11.6742288 20.8979457,13.0610623 L20.8595379,12.9980795 C20.8388233,12.9576185 20.8110809,12.9083464 20.7758911,12.8523886 C20.7002111,12.7320442 20.6092641,12.6122723 20.490714,12.4882118 C20.4382123,12.4339682 20.3821953,12.3822517 20.3234669,12.3330729 Z M14.6468261,13.7286879 C14.8677509,13.7205839 15.0857116,13.7090882 15.3004864,13.6941603 C15.3167424,13.7136098 15.3344356,13.7354495 15.3530973,13.7593069 C15.3885989,13.8046928 15.4264587,13.8556759 15.4659783,13.9111599 C15.5376495,14.011783 15.6107509,14.1214248 15.6806659,14.2312908 C15.7052289,14.2698896 15.7275563,14.3056312 15.7638523,14.364836 L16.1900272,15.1432039 C15.6906105,15.2644323 15.1754591,15.3646158 14.6471143,15.4420987 L14.6468261,13.7286879 Z M18.2753649,12.1065636 C18.2753649,12.1065636 19.0016613,12.3197234 19.4236203,12.5273801 C19.5700311,12.5994367 19.8233461,12.7432265 20.0060685,12.9320112 C20.1850632,13.1169114 20.2723307,13.3042718 20.2723307,13.3042718 L23.1346481,17.9979931 L18.5083443,18 L16.3449648,14.0487931 C16.3449648,14.0487931 15.9019712,13.3085123 15.6293853,13.0938636 C15.4019975,12.9148224 15.3050448,12.851085 15.0802197,12.851085 C14.9237243,12.851085 13.9810233,12.851085 13.9810233,12.851085 L13.9818887,17.9971515 L9.88810893,17.9988347 L9.88810893,4.85356672 L18.1089511,4.85356672 C18.1089511,4.85356672 21.8532619,4.91924639 21.8532619,8.38395716 C21.8532619,11.8486679 18.2753649,12.1065636 18.2753649,12.1065636 Z M16.4947372,7.70495376 L14.0164028,7.70339995 L14.0151713,9.93858244 L16.4947372,9.93780553 C16.4947372,9.93780553 17.6429924,9.93434181 17.6429924,8.80079198 C17.6429924,7.64445332 16.4947372,7.70495376 16.4947372,7.70495376 Z"></path>
                                    </g>
                                  </svg>
                                </span>
                                R
                              </a>
                              <a class="dc-link--grey-dark dc-u-bl dc-u-bc-grey-light dc-u-ph-16" href="/learn/python">
                                <span class="dc-icon dc-icon--primary dc-icon--size-18 dc-u-mr-8">
                                  <svg class="dc-icon__svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <path d="M6.25714091,5.41882078 C6.25714091,5.41882078 6.25714091,3.46621011 6.25767865,2.44912563 C6.25767865,-0.568029016 17.2584574,-1.20163693 17.7800025,2.86488838 C17.7800025,4.54863794 17.7800025,6.65361114 17.7800025,8.57179845 C17.7800025,10.4899859 16.134513,11.6232609 14.7450536,11.6232609 C13.3555941,11.6232609 12.9722,11.6232609 11.6987665,11.6232609 C6.59133825,11.6232609 5.17474011,11.4713389 5.46669611,17.8892404 C4.41484892,17.8892404 4.58764825,17.8892404 3.61670798,17.8892404 C-1.03493695,17.8892404 -1.37323602,6.43164273 3.61670798,6.23486973 C7.91180211,6.23486973 12.0600288,6.23486973 12.0600288,6.23486973 L12.0600288,5.41882078 L6.25714091,5.41882078 Z M8.75879411,1.87872939 C8.25179571,1.8912029 7.68658545,2.27585623 7.68658545,2.9471777 C7.68658545,3.61849904 8.19480865,3.98165806 8.70303171,3.96915453 C9.21125491,3.956651 9.77046918,3.59265036 9.77046918,2.89590906 C9.77046918,2.19916775 9.26579265,1.86625603 8.75879411,1.87872939 Z M17.7558384,18.5811792 C17.7558384,18.5811792 17.7558384,20.5337899 17.7553008,21.5508744 C17.7553008,24.568029 6.75452198,25.2016369 6.23297678,21.1351116 C6.23297678,19.4513619 6.23297678,17.6725281 6.23297678,15.4282014 C6.23297678,13.1838749 7.67902025,12.3767391 9.26792571,12.3767391 C10.8568313,12.3767391 11.0407793,12.3767391 12.3142128,12.3767391 C16.9227314,12.3767391 18.8026128,12.3767391 18.5462832,6.11075956 C19.6232633,6.11075956 19.4253312,6.11075956 20.3962713,6.11075956 C25.0126412,6.11075956 25.3862154,17.5683573 20.3962713,17.7651303 C16.1011772,17.7651303 11.9529506,17.7651303 11.9529506,17.7651303 L11.9529506,18.5811792 L17.7558384,18.5811792 Z M15.2541852,22.1212706 C15.7611836,22.1087971 16.3263938,21.7241436 16.3263938,21.0528223 C16.3263938,20.3815008 15.8181708,20.0183419 15.3099476,20.0308455 C14.8017244,20.043349 14.2425101,20.4073496 14.2425101,21.1040909 C14.2425101,21.8008322 14.7471868,22.133744 15.2541852,22.1212706 Z" id="Combined-Shape"></path>
                                  </svg>
                                </span>
                                Python
                              </a>
                              <a class="dc-link--grey-dark dc-u-bl dc-u-bc-grey-light dc-u-ph-16" href="/learn/sql/">
                                <span class="dc-icon dc-icon--primary dc-icon--size-18 dc-u-mr-8">
                                  <svg class="dc-icon__svg" height="24" viewBox="0 0 24 24" width="24">
                                    <path d="m17.6477143 15.8740909c-1.0838572 1.8586364-4.545 3.2195455-8.6477143 3.2195455-4.10271429 0-7.56385714-1.3609091-8.649-3.2195455-.22885714-.3954545-.351-.1840909-.351.0095455v2.73c0 2.6454545 4.02942857 5.3863636 9 5.3863636 4.9705714 0 9-2.7409091 9-5.385 0 0 0-2.5336364 0-2.73 0-.195-.1234286-.4063636-.3522857-.0109091zm.0141428-6.97636363c-1.0671428 1.64318183-4.5411428 2.85000003-8.6618571 2.85000003-4.12071429 0-7.596-1.2068182-8.66314286-2.85000003-.21985714-.33818182-.33685714-.15409091-.33685714-.00272727v3.2140909c0 2.4027273 4.02942857 4.3486364 9 4.3486364 4.9705714 0 9-1.9472728 9-4.3486364 0 0 0-3.05999999 0-3.2140909 0-.15136364-.1182857-.33545455-.3381429.00272727zm-8.6618571-8.89772727c-4.97057143 0-9 1.60909091-9 3.59045455v1.71818181c0 2.10136364 4.02942857 3.80590909 9 3.80590909 4.9705714 0 9-1.70454545 9-3.80590909v-1.71818181c0-1.98136364-4.0294286-3.59045455-9-3.59045455z" transform="translate(3)"></path>
                                  </svg>
                                </span>
                                SQL
                              </a>
                            </div>
                            <div>
                              <a class="dc-link--grey-dark dc-u-b-l dc-u-ph-16" href="/community/">
                                <span class="dc-icon dc-icon--primary dc-icon--size-18 dc-u-mr-8">
                                  <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path d="M9.915 11.345A5.629 5.629 0 0 0 5.797 9.55h-.161A5.644 5.644 0 0 0 0 15.186v1.84a.847.847 0 1 0 1.695 0v-1.84a3.975 3.975 0 0 1 3.974-3.974h.162a3.975 3.975 0 0 1 3.974 3.974v1.822c0 .469.38.848.848.848a.847.847 0 0 0 .78-.83v-1.84a5.628 5.628 0 0 0-.413-2.112c.055-.193.121-.382.2-.566a2.703 2.703 0 0 1 2.288-1.483h.12a2.678 2.678 0 0 1 2.677 2.678v1.34a.847.847 0 0 0 1.695.016v-1.356a4.347 4.347 0 0 0-4.322-4.339h-.119a4.322 4.322 0 0 0-3.644 1.98zM5.763 8.78a3.89 3.89 0 1 1 3.89-3.89 3.898 3.898 0 0 1-3.89 3.89zm0-6.119A2.229 2.229 0 1 0 7.992 4.89a2.229 2.229 0 0 0-2.23-2.204v-.025zm7.864 5.958a3.068 3.068 0 1 1 0-6.136 3.068 3.068 0 0 1 0 6.136zm0-4.466a1.398 1.398 0 1 0 1.398 1.398 1.407 1.407 0 0 0-1.398-1.365v-.033z" fill-rule="nonzero"></path>
                                  </svg>
                                </span>
                                Community
                              </a>
                              <a class="dc-link--grey-dark dc-u-b-l dc-u-ph-16 dc-u-bl dc-u-bc-grey-light ds-snowplow-li-main-menu-projects" href="/projects">
                                <span class="dc-icon dc-icon--primary dc-icon--size-18 dc-u-mr-8">
                                  <svg class="dc-icon__svg" width="24px" height="24px" viewBox="0 0 24 24">
                                    <path d="M11.5407436,12.4651207 C11.2052016,12.6525311 10.7950427,12.6525311 10.4595007,12.4651207 L2.2002199,7.85206107 L2.2002199,16.7462715 L11.0001222,21.6612847 L19.8000244,16.7462715 L19.8000244,7.85206107 L11.5407436,12.4651207 Z M20.3593908,5.03952693 C21.0926632,4.62997186 22,5.15431853 22,5.98762893 L22,17.3819504 C22,17.7747095 21.7862424,18.1370194 21.4406336,18.3300524 L11.5407436,23.8594423 C11.2052016,24.0468526 10.7950427,24.0468526 10.4595007,23.8594423 L0.559610689,18.3300524 C0.214001872,18.1370194 0.000244341054,17.7747095 0.000244341054,17.3819504 L0.000244341054,5.98762893 C0.000244341054,5.15431853 0.907581157,4.62997186 1.64085355,5.03952693 L11.0001222,10.2669632 L20.3593908,5.03952693 Z M1.58822137,6.96329587 C1.04376843,7.23002947 0.383921882,7.00943867 0.114412862,6.47059266 C-0.155096157,5.93174666 0.0677899314,5.27869533 0.612242874,5.01196186 L10.6119532,0.112992785 C10.9227751,-0.0392822816 11.2879556,-0.0375612149 11.5972981,0.117636519 L21.3619923,5.0166056 C21.9038537,5.2884588 22.1204457,5.9435812 21.8457638,6.47986253 C21.5710818,7.01614387 20.9091426,7.23050533 20.367281,6.95865213 L11.094084,2.30626826 L1.58822137,6.96329587 Z M12.0504313,11.9046447 C12.051149,11.9003619 12.05176,11.8963021 12.0522933,11.8924153 C12.0520762,11.8939972 12.0518584,11.8957475 12.0516408,11.8976624 L12.0504313,11.9046447 Z M9.94963739,11.9080409 C9.94908106,11.9017453 9.94851353,11.8965149 9.94795101,11.8924153 C9.94815142,11.893876 9.94836282,11.8953611 9.94858673,11.8968733 L9.94963739,11.9080409 Z M12.1551093,22.4263918 C12.1551093,22.6137864 12.1464536,22.7759994 12.1279175,22.9110899 C12.1141834,23.0111832 12.0950099,23.1026466 12.0619147,23.1991252 C12.0220133,23.3113772 12.0220133,23.3113772 11.9060443,23.5022684 C11.6914487,23.7824091 11.6914487,23.7824091 11.0001222,23.9659892 C10.3087956,23.7824091 10.3087956,23.7824091 10.0942,23.5022684 C9.97823096,23.3113772 9.97823096,23.3113772 9.9383296,23.1991252 C9.90523438,23.1026466 9.88606091,23.0111832 9.87232685,22.9110899 C9.85379073,22.7759994 9.84513498,22.6137864 9.84513498,22.4263918 L9.84513498,12.0841237 C9.84513498,11.8967291 9.85379073,11.7345161 9.87232685,11.5994256 C9.88606091,11.4993323 9.90523438,11.4078689 9.9383296,11.3113903 C9.97823096,11.1991383 9.97823096,11.1991383 10.0942,11.0082471 C10.3087956,10.7281064 10.3087956,10.7281064 11.0001222,10.5445263 C11.6914487,10.7281064 11.6914487,10.7281064 11.9060443,11.0082471 C12.0220133,11.1991383 12.0220133,11.1991383 12.0619147,11.3113903 C12.0950099,11.4078689 12.1141834,11.4993323 12.1279175,11.5994256 C12.1464536,11.7345161 12.1551093,11.8967291 12.1551093,12.0841237 L12.1551093,22.4263918 Z"></path>
                                  </svg>
                                </span>
                                Projects
                              </a>
                              <a class="dc-link--grey-dark dc-u-b-l dc-u-ph-16 dc-u-bl dc-u-bc-grey-light" href="/community/podcast">
                                <span class="dc-icon dc-icon--primary dc-icon--size-18 dc-u-mr-8">
                                  <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path d="M9.415 11.077h-.369a2.777 2.777 0 0 1-2.769-2.77V2.77A2.777 2.777 0 0 1 9.047 0h.368a2.777 2.777 0 0 1 2.77 2.77v5.538a2.777 2.777 0 0 1-2.77 2.769zm5.008-7.615c.573 0 1.039.464 1.039 1.038v3.462c0 3.08-2.25 5.64-5.193 6.136v1.825h2.077a1.038 1.038 0 1 1 0 2.077h-6.23a1.038 1.038 0 1 1 0-2.077h2.076v-1.825C5.25 13.602 3 11.042 3 7.962V4.5a1.038 1.038 0 1 1 2.077 0v3.462a4.158 4.158 0 0 0 4.154 4.153 4.158 4.158 0 0 0 4.154-4.153V4.5c0-.574.465-1.038 1.038-1.038z" fill-rule="evenodd"></path>
                                  </svg>
                                </span>
                                Podcast
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="dc-u-mh-16"><a class="dc-header__nav-link ds-snowplow-li-main-menu-assessment dc-u-ws-nowrap" href="/signal">Assessment</a></li>
                    <li class="dc-u-mh-16"><a class="dc-header__nav-link ds-snowplow-li-main-menu-pricing dc-u-ws-nowrap" href="/pricing">Pricing</a></li>
                    <li class="dc-u-mh-16"><a class="dc-header__nav-link ds-snowplow-li-main-menu-for-business dc-u-ws-nowrap" href="/groups/business">For Business</a></li>
                    <li class="dc-u-ml-16"><a class="dc-btn dc-btn--tertiary dc-u-color-white dc-btn--sign-in" href="/users/sign_in?redirect=%2Fcourses%2F">Sign in</a></li>
                    <li class="dc-u-ml-16"><a class="dc-btn dc-btn--white dc-btn--shadowed dc-u-color-primary" href="/users/sign_up?redirect=%2Fcourses%2F">Create Free Account</a></li>
                  </ul>
                  <div class="mobile-nav fade in modal dc-u-z-80 mobile-nav--ds-none" style="overflow-y:scroll" tabindex="-1" role="dialog" id="mobile_nav">
                    <div class="mobile-nav__container" role="document">
                      <div class="mobile-nav__body dc-u-z-100">
                        <button type="button" class="close" aria-label="Close">
                          <span class="dc-icon dc-icon--primary dc-icon--size-12 ">
                            <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                              <path d="M6.86 9l-5.417 5.416a1.514 1.514 0 0 0 2.14 2.14L9 11.14l5.416 5.417a1.514 1.514 0 0 0 2.14-2.14L11.14 9l5.417-5.416a1.514 1.514 0 0 0-2.14-2.14L9 6.86 3.584 1.443a1.514 1.514 0 0 0-2.14 2.14L6.86 9z" fill-rule="nonzero"></path>
                            </svg>
                          </span>
                        </button>
                        <div class="dc-u-p-16 dc-u-bb">
                          <h5 class="dc-chapeau-title">Learn</h5>
                          <ul class="list-unstyled">
                            <li class="mobile-nav__item"><a class="dc-link--grey dc-u-fw-regular" href="/signal">Assessment</a></li>
                            <li class="mobile-nav__item">
                              <a class="dc-link--grey dc-u-fw-regular" href="/courses">
                                Courses (<!-- -->328<!-- -->)
                              </a>
                            </li>
                            <li class="mobile-nav__item">
                              <a class="dc-link--grey dc-u-fw-regular" href="/tracks/skill">
                                Skill Tracks (<!-- -->43<!-- -->)
                              </a>
                            </li>
                            <li class="mobile-nav__item">
                              <a class="dc-link--grey dc-u-fw-regular" href="/tracks/career">
                                Career Tracks (<!-- -->14<!-- -->)
                              </a>
                            </li>
                            <li class="mobile-nav__item">
                              <a class="dc-link--grey dc-u-fw-regular" href="/instructors">
                                Instructors (<!-- -->275<!-- -->)
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div class="dc-u-p-16">
                          <h5 class="dc-chapeau-title">Pricing</h5>
                          <ul class="list-unstyled">
                            <li class="mobile-nav__item"><a class="dc-link--grey dc-u-fw-regular ds-snowplow-li-main-menu-pricing" href="/pricing">See our plans</a></li>
                          </ul>
                        </div>
                        <div class="dc-u-pb-16 dc-u-ph-16 dc-u-pt-0">
                          <h5 class="dc-chapeau-title">Plans</h5>
                          <ul class="list-unstyled">
                            <li class="mobile-nav__item"><a class="dc-link--grey dc-u-fw-regular ds-snowplow-li-main-menu-for-business" href="/groups/business">For Business</a></li>
                            <li class="mobile-nav__item"><a class="dc-link--grey dc-u-fw-regular ds-snowplow-li-main-menu-for-students" href="/groups/education">For Students</a></li>
                          </ul>
                        </div>
                        <div class="dc-u-bt">
                          <div class="dc-u-pt-16 dc-u-ph-16"><a class="dc-btn dc-btn--primary-light mobile-nav__create-account" href="/users/sign_up?redirect=%2Fcourses%2F">Create Free Account</a></div>
                          <div class="dc-u-bgc-primary-lightest dc-u-brad-b dc-u-ta-center"><a class="dc-u-d-b dc-u-p-16 dc-link--grey-dark" href="/users/sign_in?redirect=%2Fcourses%2F">Sign in</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-backdrop fade in dc-u-z-90 dc-u-d-none"></div>
                  </div>
                  <a aria-label="Close navigation" class="header__mobile-nav hidden-lg hidden-md" data-toggle="modal" data-target="#mobile_nav" href="#"></a>
                </div>
              </div>
              <section class="css-1ig1800">
                <div class="css-1s0ruqf">
                  <h1>Learn</h1>
                  <p>Acquire new skills fast in courses that combine short expert videos with immediate hands-on-keyboard exercises.</p>
                </div>
                <img alt="Illustrations of DataCamp badges" class="dc-u-maxw-100pc" src="https://static.datacamp.com/static/badges-fd0682e18c309069f2bf87d38f106080.svg" width="540">
              </section>
            </div>
          </header>
          <form data-remote="true" class="css-1b3f1in">
            <button class="dc-btn dc-btn--unstyled css-lsu2gc">
              <span class="dc-icon dc-icon--primary dc-icon--size-24 ">
                <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                  <path d="M9.129 1.742A5.814 5.814 0 0 0 3.322 7.55a5.813 5.813 0 0 0 5.807 5.806 5.812 5.812 0 0 0 5.806-5.806A5.813 5.813 0 0 0 9.13 1.742M5.278 14.038a7.503 7.503 0 0 0 3.851 1.058c4.162 0 7.548-3.386 7.548-7.547C16.677 3.387 13.291 0 9.13 0S1.58 3.387 1.58 7.549c0 2.13.888 4.058 2.313 5.432l-2.72 3.625a.872.872 0 0 0 1.395 1.046l2.71-3.614z" fill-rule="evenodd"></path>
                </svg>
              </span>
            </button>
            <input type="text" class="dc-input--text css-w24rtu" placeholder="What do you want to learn?" name="query" id="query" required="">
          </form>

          <section class="css-6incze">
            <h2 class="css-1k1n1og">Recommended Courses</h2>
            <nav class="css-1ewp9ay">
              @foreach($courses as $course)
                <a href="/course/{{ $course->slug }}" class="css-179cao1"  style="text-decoration:none">
                <span class="css-11khr5q"><img alt="R Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzdERDRFNyIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzcwOTlENyIvPjwvbGluZWFyR3JhZGllbnQ+PHBhdGggaWQ9ImQiIGQ9Ik0xNjUgNTUuMDIzYy0uMDUtNC4xMTktLjg2Ny04LjIxNS0yLjM1LTEyLjA3My0xLjQ2Ni0zLjg3NS0zLjc1OC03LjQxNS02LjM4My0xMC42NDktNS4zMzUtNi40NDktMTIuMzc3LTExLjQ0NS0xOS45ODMtMTUuMTA4LTcuNjI2LTMuNjc4LTE1Ljg2LTYuMDU3LTI0LjE5NC03LjQxOC04LjM0OC0xLjM2Ny0xNi44MjgtMS41ODQtMjUuMTYtMS4wNjctOC4zMzUuNjEtMTYuNTM5IDIuMDItMjQuMzMgNC4zNjQtNy43OTMgMi4zMTItMTUuMjA4IDUuNDU4LTIxLjgzNiA5LjQ1QzM0LjE1OSAyNi41MjEgMjguMzQ5IDMxLjQxIDI0LjIgMzYuOTljLTQuMTggNS41Ny02LjQ4IDExLjc3OS02LjQ4IDE4LjAzNC0uMDA1IDYuMjUyIDIuMjc4IDEyLjQ3NSA2LjQ2MiAxOC4wNDYgNC4xMzYgNS41OTMgOS45NTMgMTAuNDkyIDE2LjU2IDE0LjQ5MSA2LjYyNSA0LjAwNCAxNC4wNDggNy4xNTIgMjEuODQ1IDkuNDYgNy43OTQgMi4zNTUgMTYuMDA1IDMuNzU2IDI0LjM0MiA0LjM2IDguMzM2LjUxNCAxNi44Mi4yOTYgMjUuMTY1LTEuMDgxIDguMzM2LTEuMzY1IDE2LjU3Mi0zLjc0OCAyNC4xOTYtNy40MzUgNy42MDctMy42NjUgMTQuNjQ0LTguNjY2IDE5Ljk3OC0xNS4xMTcgMi42MjQtMy4yMzQgNC45MTktNi43NzUgNi4zOC0xMC42NDkgMS40ODYtMy44NTggMi4zMDItNy45NTYgMi4zNS0xMi4wNzVtMCAwYy4wMzcgOC4yNjctMy4xMjYgMTYuMzgxLTguMjMgMjMuMDc5LTUuMDggNi43Ni0xMS44NDYgMTIuMzAxLTE5LjI4NSAxNi43NTYtNy40NyA0LjQ0Ni0xNS42NSA3Ljg0NS0yNC4xNjUgMTAuMzE4LTguNTE0IDIuNTA3LTE3LjM5NiAzLjk3OC0yNi4zODkgNC41ODUtMTcuOTQgMS4wNzctMzYuNzMxLTEuMzcyLTUzLjcyNy05LjYtOC40MzktNC4xMi0xNi40My05Ljc2NS0yMi43MDUtMTcuMzgxLTMuMDg5LTMuODItNS44MTItOC4wODctNy42MjQtMTIuODI4QzEuMDYgNjUuMjI4LjA0NyA2MC4xMzIgMCA1NS4wMjNjLjAzLTUuMTA4IDEuMDU0LTEwLjIwNiAyLjg2Ni0xNC45MzIgMS43ODItNC43NTEgNC41My05LjAxNCA3LjYxNC0xMi44MzYgMy4wOTQtMy44MzYgNi43Mi03LjEyNSAxMC41NC0xMC4wMTYgMy44Mi0yLjkwNSA3LjkzNy01LjMxNiAxMi4xNjEtNy4zOTJDNTAuMTgyIDEuNjA5IDY4Ljk4My0uODQ0IDg2LjkzMS4yNDNjOC45OTUuNjEgMTcuODgxIDIuMDgzIDI2LjM5NSA0LjU5OSA4LjUxNiAyLjQ4IDE2LjY5NyA1Ljg4MyAyNC4xNjYgMTAuMzM0IDcuNDM4IDQuNDU5IDE0LjIwMyAxMC4wMDYgMTkuMjggMTYuNzY3IDUuMTA1IDYuNyA4LjI2MiAxNC44MTUgOC4yMjggMjMuMDgiLz48cGF0aCBpZD0iZSIgZD0iTTk5LjA3MiA5My4yMzJjMTAuNzI2IDAgMTcuMjM3LTUuNzUxIDE3LjIzNy0xNi44NzkgMC0xMi4wNTUtNi41MTEtMTUuNTgtMTcuMjM3LTE1LjU4SDg2LjgxNXYzMi40NmgxMi4yNTd6bS0yLjg3MyAyMC4wM2gtOS4xOTVWMTYwSDYwVjQwaDQ0LjI0M2MyMS40NSAwIDQwLjk4OCA3LjIzNCA0MC45ODggMzQuMzE0IDAgMTYuMTM0LTkuMDAxIDI3LjgyLTIyLjQxIDMzLjk0TDE1NSAxNjBoLTMwLjA3bC0yOC43MzEtNDYuNzM4eiIvPjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxtYXNrIGlkPSJjIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjx1c2UgZmlsbD0iI0Q4RDhEOCIgeGxpbms6aHJlZj0iI2EiLz48ZyBmaWxsPSJ1cmwoI2IpIiBtYXNrPSJ1cmwoI2MpIj48cGF0aCBkPSJNMCAwaDMwMHYzMDBIMHoiLz48L2c+PGcgZmlsbD0iI0ZGRiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAgNjUpIj48dXNlIHhsaW5rOmhyZWY9IiNkIi8+PHVzZSB4bGluazpocmVmPSIjZSIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                <div class="css-1hi4crg">
                  <h4 class="css-1a7hu85"> {{ $course->title }}</h4>
                  <p class="css-z6sjuf">{!! strip_tags($course->short_description) !!}</p>
                  <div class="css-4acvv2">
                    <span class="css-4acvv2">
                      <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                        <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                          <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                        </svg>
                      </span>
                      4 hours
                    </span>
                  </div>
                </div>
                <footer class="css-3guzbk">
                  <div class="css-3xxt4t gatsby-image-wrapper" style="position:relative;overflow:hidden;display:inline-block;width:50px;height:50px">
                    <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAUDBAH/xAAVAQEBAAAAAAAAAAAAAAAAAAACAP/aAAwDAQACEAMQAAABp8/JjVJgYi+AdEB//8QAHBAAAQQDAQAAAAAAAAAAAAAAAgABAxESEyFB/9oACAEBAAEFArZxGTasEUotGEgAqtYtsfi8/8QAGBEAAgMAAAAAAAAAAAAAAAAAABAREiH/2gAIAQMBAT8BjCr/AP/EABgRAAIDAAAAAAAAAAAAAAAAAAAQERIh/9oACAECAQE/AZ0s/wD/xAAcEAACAgIDAAAAAAAAAAAAAAABEQACAxASIXH/2gAIAQEABj8CdolxWjUpzskeRjLFv//EABsQAAMAAwEBAAAAAAAAAAAAAAABESExUUFh/9oACAEBAAE/IeTJUphtkfC+GaoW7kfNOn6FOmPtH8EVaL0TcTGD/9oADAMBAAIAAwAAABDv137/xAAXEQEBAQEAAAAAAAAAAAAAAAABEQAQ/9oACAEDAQE/EAKujkN//8QAGBEAAgMAAAAAAAAAAAAAAAAAABEBECH/2gAIAQIBAT8QaMD0z//EAB0QAQADAQACAwAAAAAAAAAAAAEAESExQWGBofD/2gAIAQEAAT8QVQXeP3YZtTT2uxbdRIzQpVN5yAV408e7hEFoUt9MY6N97Dioo/MFa1ajJ//Z" alt="Photo of Jonathan Cornelissen" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                    <picture>
                      <source srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                        https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                        https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x">
                      <img srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                        https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                        https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x" src="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg" alt="Photo of Jonathan Cornelissen" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                    </picture>
                    <noscript>
                      <picture>
                        <source srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x" />
                        <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x" src="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg" alt="Photo of Jonathan Cornelissen" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                      </picture>
                    </noscript>
                  </div>
                  <div>
                    <h5 class="css-1ogvh3l">{{ $course->user->name }}</h5>
                    <p class="css-f85llx">Instructor Academy Digital</p>
                  </div>
                </footer>
              </a>
              @endforeach
              
              <a href="/courses/intro-to-python-for-data-science" class="css-179cao1"   style="text-decoration:none">
                <span class="css-11khr5q"><img alt="Python Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjMuMjQ5JSIgeTI9Ijk2Ljc3NCUiPjxzdG9wIG9mZnNldD0iMS41MzklIiBzdG9wLWNvbG9yPSIjODJEQTlFIi8+PHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjNDhBRTc2Ii8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48bWFzayBpZD0iYyIgZmlsbD0iI2ZmZiI+PHVzZSB4bGluazpocmVmPSIjYSIvPjwvbWFzaz48dXNlIGZpbGw9IiNEOEQ4RDgiIHhsaW5rOmhyZWY9IiNhIi8+PGcgZmlsbD0idXJsKCNiKSIgbWFzaz0idXJsKCNjKSI+PHBhdGggZD0iTTAgMGgzMDV2MzA1SDB6Ii8+PC9nPjxnIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PHBhdGggZD0iTTE1MC4wODIgNzAuMDAxYy02LjY4NC4wMzEtMTMuMDY3LjU5Ni0xOC42ODMgMS41OC0xNi41NDUgMi44OTUtMTkuNTQ5IDguOTU0LTE5LjU0OSAyMC4xMjd2MTQuNzU4aDM5LjA5OHY0LjkxOUg5Ny4xNzdjLTExLjM2MyAwLTIxLjMxMiA2Ljc2NC0yNC40MjUgMTkuNjMxLTMuNTkgMTQuNzQ5LTMuNzQ5IDIzLjk1MyAwIDM5LjM1M0M3NS41MzIgMTgxLjgzMiA4Mi4xNyAxOTAgOTMuNTMyIDE5MGgxMy40NDJ2LTE3LjY5YzAtMTIuNzgxIDExLjE2Ni0yNC4wNTUgMjQuNDI1LTI0LjA1NWgzOS4wNTJjMTAuODcgMCAxOS41NDktOC44NjQgMTkuNTQ5LTE5LjY3NlY5MS43MWMwLTEwLjQ5NC04LjkzOS0xOC4zNzctMTkuNTQ5LTIwLjEyOC02LjcxNi0xLjEwNy0xMy42ODUtMS42MS0yMC4zNjktMS41OHptLTIxLjE0NCAxMS44N2M0LjAzOSAwIDcuMzM3IDMuMzE5IDcuMzM3IDcuNCAwIDQuMDY4LTMuMjk4IDcuMzU3LTcuMzM3IDcuMzU3LTQuMDUzIDAtNy4zMzYtMy4yOS03LjMzNi03LjM1NiAwLTQuMDgyIDMuMjgzLTcuNDAyIDcuMzM2LTcuNDAyeiIvPjxwYXRoIGQ9Ik0xOTIuODU3IDExMHYxNy4wNjNjMCAxMy4yMy0xMS4zODggMjQuMzY0LTI0LjM3NSAyNC4zNjRoLTM4Ljk3M2MtMTAuNjc1IDAtMTkuNTA5IDguOTk4LTE5LjUwOSAxOS41Mjd2MzYuNTljMCAxMC40MTMgOS4xOTUgMTYuNTM5IDE5LjUxIDE5LjUyNiAxMi4zNSAzLjU3NyAyNC4xOTQgNC4yMjMgMzguOTcyIDAgOS44MjMtMi44IDE5LjUxLTguNDM4IDE5LjUxLTE5LjUyNnYtMTQuNjQ1aC0zOC45NzR2LTQuODgyaDU4LjQ4M2MxMS4zNCAwIDE1LjU2NS03Ljc5IDE5LjUwOC0xOS40ODIgNC4wNzQtMTIuMDM3IDMuOS0yMy42MTIgMC0zOS4wNTNDMjI0LjIwNyAxMTguMzY0IDIxOC44NTQgMTEwIDIwNy41IDExMGgtMTQuNjQzem0tMjEuOTIgOTIuNjYyYzQuMDQ1IDAgNy4zMjIgMy4yNjQgNy4zMjIgNy4zIDAgNC4wNS0zLjI3NyA3LjM0NS03LjMyMSA3LjM0NS00LjAzIDAtNy4zMjItMy4yOTQtNy4zMjItNy4zNDUgMC00LjAzNiAzLjI5MS03LjMgNy4zMjItNy4zeiIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                <div class="css-1hi4crg">
                  <h4 class="css-1a7hu85">Introduction to Python</h4>
                  <p class="css-z6sjuf">Master the basics of data analysis in Python. Expand your skillset by learning scientific computing with numpy.</p>
                  <div class="css-4acvv2">
                    <span class="css-4acvv2">
                      <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                        <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                          <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                        </svg>
                      </span>
                      4 hours
                    </span>
                  </div>
                </div>
                <footer class="css-3guzbk">
                  <div class="css-3xxt4t gatsby-image-wrapper" style="position:relative;overflow:hidden;display:inline-block;width:50px;height:50px">
                    <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEAMQAAABsxk7os81F7hP/8QAHBAAAgICAwAAAAAAAAAAAAAAAQISIQMiMTJC/9oACAEBAAEFAgNpyZ6bhhRYyPtqXH1//8QAFhEAAwAAAAAAAAAAAAAAAAAAABAR/9oACAEDAQE/ASP/xAAWEQADAAAAAAAAAAAAAAAAAAAAEBH/2gAIAQIBAT8BK//EABgQAAMBAQAAAAAAAAAAAAAAAAABERAx/9oACAEBAAY/AkdxxlKMef/EABwQAQEBAAIDAQAAAAAAAAAAAAERACExQXGBof/aAAgBAQABPyG3R8yp9LAuRKfuVEmHFl84fQyRM1RuWf/aAAwDAQACAAMAAAAQGCAD/8QAGREAAwADAAAAAAAAAAAAAAAAAAERECEx/9oACAEDAQE/EFJsQfMf/8QAGREBAAIDAAAAAAAAAAAAAAAAAQAhEBFB/9oACAECAQE/EEd1KQ7j/8QAHBABAQADAQADAAAAAAAAAAAAAREAITFBcYGR/9oACAEBAAE/EJZNNaT3Nv4Z4Au9TuMJuj0YIhY0fqZb1nU69wkdUUix/MIi1U384pkeZ2r7n//Z" alt="Photo of Hugo Bowne-Anderson" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                    <picture>
                      <source srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                        https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                        https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x">
                      <img srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                        https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                        https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" src="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg" alt="Photo of Hugo Bowne-Anderson" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                    </picture>
                    <noscript>
                      <picture>
                        <source srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" />
                        <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" src="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg" alt="Photo of Hugo Bowne-Anderson" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                      </picture>
                    </noscript>
                  </div>
                  <div>
                    <h5 class="css-1ogvh3l">Hugo Bowne-Anderson</h5>
                    <p class="css-f85llx">Data Scientist at DataCamp</p>
                  </div>
                </footer>
              </a>
              <a href="/courses/introduction-to-sql" class="css-179cao1"   style="text-decoration:none">
                <span class="css-11khr5q"><img alt="SQL Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI0I4QkNGOSIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzk4OERFNiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PG1hc2sgaWQ9ImMiIGZpbGw9IiNmZmYiPjx1c2UgeGxpbms6aHJlZj0iI2EiLz48L21hc2s+PHVzZSBmaWxsPSIjRDhEOEQ4IiB4bGluazpocmVmPSIjYSIvPjxnIGZpbGw9InVybCgjYikiIG1hc2s9InVybCgjYykiPjxwYXRoIGQ9Ik0wIDBoMzAwdjMwMEgweiIvPjwvZz48cGF0aCBmaWxsPSIjRkZGIiBkPSJNMTQ4Ljk3IDE4OC44OThhMjg3LjU4NCAyODcuNTg0IDAgMCAwIDU0LjY5LTQuNjE3IDYzLjkxOSA2My45MTkgMCAwIDAgMTkuMjczLTYuMzU5djI1LjgyM2MwIDguNjE1LTMzLjExNCAxNS41OTEtNzMuOTYyIDE1LjU5MS00MC44NDkgMC03My45NjEtNi45NzYtNzMuOTYxLTE1LjU5di0yNS44MjRhNjMuOTA0IDYzLjkwNCAwIDAgMCAxOS4yNzMgNi4zNTkgMjg3LjU2NyAyODcuNTY3IDAgMCAwIDU0LjY4OCA0LjYxN3ptMC00MC43MDFhMjg3LjQyOSAyODcuNDI5IDAgMCAwIDU0LjYzOC00LjU2NiA2My44OTQgNjMuODk0IDAgMCAwIDE5LjI3Mi02LjM1OXYyNC44NDhoLS40MTRjMCA0LjM2LTI1LjgxNiAxMi44NzQtNzMuNDk1IDEyLjg3NC00Ny42NzggMC03My41NDctOC40OS03My41NDctMTIuODIzaC0uNDE0di0yNC44OTlhNjMuOTEgNjMuOTEgMCAwIDAgMTkuMjczIDYuMzA4IDI4Ny41NjcgMjg3LjU2NyAwIDAgMCA1NC42ODggNC42MTd6bTczLjkzNi01Ny42MDZhMy4yOSAzLjI5IDAgMCAxLS4wMjYuNDYxYy4wMTMuMTcyLjAxMy4zNDEgMCAuNTEzdjI4LjIwN2gtLjQxNGMwIDQuMzU4LTI1LjgxNiAxMi44NzItNzMuNDk1IDEyLjg3Mi00Ny42NzggMC03My41NDctOC40ODctNzMuNTQ3LTEyLjgyaC0uNDE0VjkxLjYxNWEzLjI3NSAzLjI3NSAwIDAgMSAwLS41MTEgMy4zMTcgMy4zMTcgMCAwIDEgMC0uNTE0YzAtOC42MTUgMzMuMTM5LTE1LjU5IDczLjk2LTE1LjU5IDQwLjgyNCAwIDczLjkzNiA2Ljk3NSA3My45MzYgMTUuNTl6IiBtYXNrPSJ1cmwoI2MpIi8+PC9nPjwvc3ZnPg==" width="40"></span>
                <div class="css-1hi4crg">
                  <h4 class="css-1a7hu85">Introduction to SQL</h4>
                  <p class="css-z6sjuf">Master the basics of querying tables in relational databases such as MySQL, SQL Server, and PostgreSQL.</p>
                  <div class="css-4acvv2">
                    <span class="css-4acvv2">
                      <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                        <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                          <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                        </svg>
                      </span>
                      4 hours
                    </span>
                  </div>
                </div>
                <footer class="css-3guzbk">
                  <div class="css-3xxt4t gatsby-image-wrapper" style="position:relative;overflow:hidden;display:inline-block;width:50px;height:50px">
                    <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGQABAQADAQAAAAAAAAAAAAAAAAQBAgMF/8QAFwEBAQEBAAAAAAAAAAAAAAAAAgABA//aAAwDAQACEAMQAAABr1mxtc4lzn5E/QAX/8QAHRAAAgIBBQAAAAAAAAAAAAAAAQIAAzEEEBESE//aAAgBAQABBQLmJ22tYrZ6NEsJXUZXKABf/8QAFxEBAAMAAAAAAAAAAAAAAAAAEAERIf/aAAgBAwEBPwHKJP/EABYRAQEBAAAAAAAAAAAAAAAAABABIf/aAAgBAgEBPwHSH//EABkQAAMBAQEAAAAAAAAAAAAAAAABMREQIf/aAAgBAQAGPwKnrZDEUhvFiP/EABsQAAMAAwEBAAAAAAAAAAAAAAABESExYVHw/9oACAEBAAE/IVDaWSPYXdPmR4ZKKaiKc2IjVXgimM+madElBH//2gAMAwEAAgADAAAAEHswP//EABcRAQEBAQAAAAAAAAAAAAAAAAEAETH/2gAIAQMBAT8QAqcuCVG//8QAGBEAAwEBAAAAAAAAAAAAAAAAAAEhEUH/2gAIAQIBAT8Qti07Eof/xAAdEAEAAgIDAQEAAAAAAAAAAAABABEhMUFRYXHx/9oACAEBAAE/EKpDgSWC6WEbvjGoqrRfYHhTd1aSusZ42ewMgS3+pjRWjtUxbpA/IEAiqF7n/9k=" alt="Photo of Nick Carchedi" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                    <picture>
                      <source srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                        https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                        https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x">
                      <img srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                        https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                        https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x" src="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg" alt="Photo of Nick Carchedi" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                    </picture>
                    <noscript>
                      <picture>
                        <source srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x" />
                        <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x" src="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg" alt="Photo of Nick Carchedi" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                      </picture>
                    </noscript>
                  </div>
                  <div>
                    <h5 class="css-1ogvh3l">Nick Carchedi</h5>
                    <p class="css-f85llx">Product Manager at DataCamp</p>
                  </div>
                </footer>
              </a>
            </nav>
          </section>

          <section class="css-6incze" style=" background-color: rgb(235, 244, 247);">
            <h2 class="css-1k1n1og">Browse By Technology</h2>
            <nav class="css-1ewp9ay">
              <div class="card" style="width: 200px">
                <div class="card-body text-center">
                  <a href="/courses/tech:r" class="css-lafe5q"   style="text-decoration:none">
                    <div class="css-1ctspcv">
                      <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzdERDRFNyIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzcwOTlENyIvPjwvbGluZWFyR3JhZGllbnQ+PHBhdGggaWQ9ImQiIGQ9Ik0xNjUgNTUuMDIzYy0uMDUtNC4xMTktLjg2Ny04LjIxNS0yLjM1LTEyLjA3My0xLjQ2Ni0zLjg3NS0zLjc1OC03LjQxNS02LjM4My0xMC42NDktNS4zMzUtNi40NDktMTIuMzc3LTExLjQ0NS0xOS45ODMtMTUuMTA4LTcuNjI2LTMuNjc4LTE1Ljg2LTYuMDU3LTI0LjE5NC03LjQxOC04LjM0OC0xLjM2Ny0xNi44MjgtMS41ODQtMjUuMTYtMS4wNjctOC4zMzUuNjEtMTYuNTM5IDIuMDItMjQuMzMgNC4zNjQtNy43OTMgMi4zMTItMTUuMjA4IDUuNDU4LTIxLjgzNiA5LjQ1QzM0LjE1OSAyNi41MjEgMjguMzQ5IDMxLjQxIDI0LjIgMzYuOTljLTQuMTggNS41Ny02LjQ4IDExLjc3OS02LjQ4IDE4LjAzNC0uMDA1IDYuMjUyIDIuMjc4IDEyLjQ3NSA2LjQ2MiAxOC4wNDYgNC4xMzYgNS41OTMgOS45NTMgMTAuNDkyIDE2LjU2IDE0LjQ5MSA2LjYyNSA0LjAwNCAxNC4wNDggNy4xNTIgMjEuODQ1IDkuNDYgNy43OTQgMi4zNTUgMTYuMDA1IDMuNzU2IDI0LjM0MiA0LjM2IDguMzM2LjUxNCAxNi44Mi4yOTYgMjUuMTY1LTEuMDgxIDguMzM2LTEuMzY1IDE2LjU3Mi0zLjc0OCAyNC4xOTYtNy40MzUgNy42MDctMy42NjUgMTQuNjQ0LTguNjY2IDE5Ljk3OC0xNS4xMTcgMi42MjQtMy4yMzQgNC45MTktNi43NzUgNi4zOC0xMC42NDkgMS40ODYtMy44NTggMi4zMDItNy45NTYgMi4zNS0xMi4wNzVtMCAwYy4wMzcgOC4yNjctMy4xMjYgMTYuMzgxLTguMjMgMjMuMDc5LTUuMDggNi43Ni0xMS44NDYgMTIuMzAxLTE5LjI4NSAxNi43NTYtNy40NyA0LjQ0Ni0xNS42NSA3Ljg0NS0yNC4xNjUgMTAuMzE4LTguNTE0IDIuNTA3LTE3LjM5NiAzLjk3OC0yNi4zODkgNC41ODUtMTcuOTQgMS4wNzctMzYuNzMxLTEuMzcyLTUzLjcyNy05LjYtOC40MzktNC4xMi0xNi40My05Ljc2NS0yMi43MDUtMTcuMzgxLTMuMDg5LTMuODItNS44MTItOC4wODctNy42MjQtMTIuODI4QzEuMDYgNjUuMjI4LjA0NyA2MC4xMzIgMCA1NS4wMjNjLjAzLTUuMTA4IDEuMDU0LTEwLjIwNiAyLjg2Ni0xNC45MzIgMS43ODItNC43NTEgNC41My05LjAxNCA3LjYxNC0xMi44MzYgMy4wOTQtMy44MzYgNi43Mi03LjEyNSAxMC41NC0xMC4wMTYgMy44Mi0yLjkwNSA3LjkzNy01LjMxNiAxMi4xNjEtNy4zOTJDNTAuMTgyIDEuNjA5IDY4Ljk4My0uODQ0IDg2LjkzMS4yNDNjOC45OTUuNjEgMTcuODgxIDIuMDgzIDI2LjM5NSA0LjU5OSA4LjUxNiAyLjQ4IDE2LjY5NyA1Ljg4MyAyNC4xNjYgMTAuMzM0IDcuNDM4IDQuNDU5IDE0LjIwMyAxMC4wMDYgMTkuMjggMTYuNzY3IDUuMTA1IDYuNyA4LjI2MiAxNC44MTUgOC4yMjggMjMuMDgiLz48cGF0aCBpZD0iZSIgZD0iTTk5LjA3MiA5My4yMzJjMTAuNzI2IDAgMTcuMjM3LTUuNzUxIDE3LjIzNy0xNi44NzkgMC0xMi4wNTUtNi41MTEtMTUuNTgtMTcuMjM3LTE1LjU4SDg2LjgxNXYzMi40NmgxMi4yNTd6bS0yLjg3MyAyMC4wM2gtOS4xOTVWMTYwSDYwVjQwaDQ0LjI0M2MyMS40NSAwIDQwLjk4OCA3LjIzNCA0MC45ODggMzQuMzE0IDAgMTYuMTM0LTkuMDAxIDI3LjgyLTIyLjQxIDMzLjk0TDE1NSAxNjBoLTMwLjA3bC0yOC43MzEtNDYuNzM4eiIvPjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxtYXNrIGlkPSJjIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjx1c2UgZmlsbD0iI0Q4RDhEOCIgeGxpbms6aHJlZj0iI2EiLz48ZyBmaWxsPSJ1cmwoI2IpIiBtYXNrPSJ1cmwoI2MpIj48cGF0aCBkPSJNMCAwaDMwMHYzMDBIMHoiLz48L2c+PGcgZmlsbD0iI0ZGRiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAgNjUpIj48dXNlIHhsaW5rOmhyZWY9IiNkIi8+PHVzZSB4bGluazpocmVmPSIjZSIvPjwvZz48L2c+PC9zdmc+" width="110">
                    </div>
                    <hr>
                    <table style="width: 100%">
                      <tr>
                        <td align="left">
                          <h5 class="css-okns0k"> R </h5>
                        </td>
                        <td align="right">
                          <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                            <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                              <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                              </path>
                            </svg>
                          </span>
                        </td>
                      </tr>
                    </table>
                  </a>
                </div>
              </div>

              &nbsp;&nbsp;&nbsp;&nbsp;

              <div class="card" style="width: 200px">
                <div class="card-body text-center">
                  <a href="/courses/tech:r" class="css-lafe5q"   style="text-decoration:none">
                    <div class="css-1ctspcv">
                      <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjMuMjQ5JSIgeTI9Ijk2Ljc3NCUiPjxzdG9wIG9mZnNldD0iMS41MzklIiBzdG9wLWNvbG9yPSIjODJEQTlFIi8+PHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjNDhBRTc2Ii8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48bWFzayBpZD0iYyIgZmlsbD0iI2ZmZiI+PHVzZSB4bGluazpocmVmPSIjYSIvPjwvbWFzaz48dXNlIGZpbGw9IiNEOEQ4RDgiIHhsaW5rOmhyZWY9IiNhIi8+PGcgZmlsbD0idXJsKCNiKSIgbWFzaz0idXJsKCNjKSI+PHBhdGggZD0iTTAgMGgzMDV2MzA1SDB6Ii8+PC9nPjxnIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PHBhdGggZD0iTTE1MC4wODIgNzAuMDAxYy02LjY4NC4wMzEtMTMuMDY3LjU5Ni0xOC42ODMgMS41OC0xNi41NDUgMi44OTUtMTkuNTQ5IDguOTU0LTE5LjU0OSAyMC4xMjd2MTQuNzU4aDM5LjA5OHY0LjkxOUg5Ny4xNzdjLTExLjM2MyAwLTIxLjMxMiA2Ljc2NC0yNC40MjUgMTkuNjMxLTMuNTkgMTQuNzQ5LTMuNzQ5IDIzLjk1MyAwIDM5LjM1M0M3NS41MzIgMTgxLjgzMiA4Mi4xNyAxOTAgOTMuNTMyIDE5MGgxMy40NDJ2LTE3LjY5YzAtMTIuNzgxIDExLjE2Ni0yNC4wNTUgMjQuNDI1LTI0LjA1NWgzOS4wNTJjMTAuODcgMCAxOS41NDktOC44NjQgMTkuNTQ5LTE5LjY3NlY5MS43MWMwLTEwLjQ5NC04LjkzOS0xOC4zNzctMTkuNTQ5LTIwLjEyOC02LjcxNi0xLjEwNy0xMy42ODUtMS42MS0yMC4zNjktMS41OHptLTIxLjE0NCAxMS44N2M0LjAzOSAwIDcuMzM3IDMuMzE5IDcuMzM3IDcuNCAwIDQuMDY4LTMuMjk4IDcuMzU3LTcuMzM3IDcuMzU3LTQuMDUzIDAtNy4zMzYtMy4yOS03LjMzNi03LjM1NiAwLTQuMDgyIDMuMjgzLTcuNDAyIDcuMzM2LTcuNDAyeiIvPjxwYXRoIGQ9Ik0xOTIuODU3IDExMHYxNy4wNjNjMCAxMy4yMy0xMS4zODggMjQuMzY0LTI0LjM3NSAyNC4zNjRoLTM4Ljk3M2MtMTAuNjc1IDAtMTkuNTA5IDguOTk4LTE5LjUwOSAxOS41Mjd2MzYuNTljMCAxMC40MTMgOS4xOTUgMTYuNTM5IDE5LjUxIDE5LjUyNiAxMi4zNSAzLjU3NyAyNC4xOTQgNC4yMjMgMzguOTcyIDAgOS44MjMtMi44IDE5LjUxLTguNDM4IDE5LjUxLTE5LjUyNnYtMTQuNjQ1aC0zOC45NzR2LTQuODgyaDU4LjQ4M2MxMS4zNCAwIDE1LjU2NS03Ljc5IDE5LjUwOC0xOS40ODIgNC4wNzQtMTIuMDM3IDMuOS0yMy42MTIgMC0zOS4wNTNDMjI0LjIwNyAxMTguMzY0IDIxOC44NTQgMTEwIDIwNy41IDExMGgtMTQuNjQzem0tMjEuOTIgOTIuNjYyYzQuMDQ1IDAgNy4zMjIgMy4yNjQgNy4zMjIgNy4zIDAgNC4wNS0zLjI3NyA3LjM0NS03LjMyMSA3LjM0NS00LjAzIDAtNy4zMjItMy4yOTQtNy4zMjItNy4zNDUgMC00LjAzNiAzLjI5MS03LjMgNy4zMjItNy4zeiIvPjwvZz48L2c+PC9zdmc+" width="110">
                    </div>
                    <hr>
                    <table style="width: 100%">
                      <tr>
                        <td align="left">
                          <h5 class="css-okns0k"> Python </h5>
                        </td>
                        <td align="right">
                          <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                            <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                              <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                              </path>
                            </svg>
                          </span>
                        </td>
                      </tr>
                    </table>
                  </a>
                </div>
              </div>

              &nbsp;&nbsp;&nbsp;&nbsp;

              <div class="card" style="width: 200px">
                <div class="card-body text-center">
                  <a href="/courses/tech:r" class="css-lafe5q"   style="text-decoration:none">
                    <div class="css-1ctspcv">
                      <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI0I4QkNGOSIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzk4OERFNiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PG1hc2sgaWQ9ImMiIGZpbGw9IiNmZmYiPjx1c2UgeGxpbms6aHJlZj0iI2EiLz48L21hc2s+PHVzZSBmaWxsPSIjRDhEOEQ4IiB4bGluazpocmVmPSIjYSIvPjxnIGZpbGw9InVybCgjYikiIG1hc2s9InVybCgjYykiPjxwYXRoIGQ9Ik0wIDBoMzAwdjMwMEgweiIvPjwvZz48cGF0aCBmaWxsPSIjRkZGIiBkPSJNMTQ4Ljk3IDE4OC44OThhMjg3LjU4NCAyODcuNTg0IDAgMCAwIDU0LjY5LTQuNjE3IDYzLjkxOSA2My45MTkgMCAwIDAgMTkuMjczLTYuMzU5djI1LjgyM2MwIDguNjE1LTMzLjExNCAxNS41OTEtNzMuOTYyIDE1LjU5MS00MC44NDkgMC03My45NjEtNi45NzYtNzMuOTYxLTE1LjU5di0yNS44MjRhNjMuOTA0IDYzLjkwNCAwIDAgMCAxOS4yNzMgNi4zNTkgMjg3LjU2NyAyODcuNTY3IDAgMCAwIDU0LjY4OCA0LjYxN3ptMC00MC43MDFhMjg3LjQyOSAyODcuNDI5IDAgMCAwIDU0LjYzOC00LjU2NiA2My44OTQgNjMuODk0IDAgMCAwIDE5LjI3Mi02LjM1OXYyNC44NDhoLS40MTRjMCA0LjM2LTI1LjgxNiAxMi44NzQtNzMuNDk1IDEyLjg3NC00Ny42NzggMC03My41NDctOC40OS03My41NDctMTIuODIzaC0uNDE0di0yNC44OTlhNjMuOTEgNjMuOTEgMCAwIDAgMTkuMjczIDYuMzA4IDI4Ny41NjcgMjg3LjU2NyAwIDAgMCA1NC42ODggNC42MTd6bTczLjkzNi01Ny42MDZhMy4yOSAzLjI5IDAgMCAxLS4wMjYuNDYxYy4wMTMuMTcyLjAxMy4zNDEgMCAuNTEzdjI4LjIwN2gtLjQxNGMwIDQuMzU4LTI1LjgxNiAxMi44NzItNzMuNDk1IDEyLjg3Mi00Ny42NzggMC03My41NDctOC40ODctNzMuNTQ3LTEyLjgyaC0uNDE0VjkxLjYxNWEzLjI3NSAzLjI3NSAwIDAgMSAwLS41MTEgMy4zMTcgMy4zMTcgMCAwIDEgMC0uNTE0YzAtOC42MTUgMzMuMTM5LTE1LjU5IDczLjk2LTE1LjU5IDQwLjgyNCAwIDczLjkzNiA2Ljk3NSA3My45MzYgMTUuNTl6IiBtYXNrPSJ1cmwoI2MpIi8+PC9nPjwvc3ZnPg==" width="110">
                    </div>
                    <hr>
                    <table style="width: 100%">
                      <tr>
                        <td align="left">
                          <h5 class="css-okns0k"> SQL </h5>
                        </td>
                        <td align="right">
                          <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                            <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                              <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                              </path>
                            </svg>
                          </span>
                        </td>
                      </tr>
                    </table>
                  </a>
                </div>
              </div>

              &nbsp;&nbsp;&nbsp;&nbsp;

              <div class="card" style="width: 200px">
                <div class="card-body text-center">
                  <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                    <div class="css-1ctspcv">
                      <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9Ijk4LjAxMiUiIHkyPSI1LjUwNyUiPjxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNFQzc3MzkiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNGOEE0NzciLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxtYXNrIGlkPSJjIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjx1c2UgZmlsbD0iI0Q4RDhEOCIgeGxpbms6aHJlZj0iI2EiLz48ZyBmaWxsPSJ1cmwoI2IpIiBtYXNrPSJ1cmwoI2MpIj48cGF0aCBkPSJNLTE1LTVoMzIwdjMyMEgtMTV6Ii8+PC9nPjxwYXRoIGZpbGw9IiNGRkYiIGQ9Ik0yNDEuNTEgMTQxLjk4MWwtODAuNzczLTc4LjU4NmMtNC42NDgtNC41MjctMTIuMTkzLTQuNTI3LTE2Ljg0OCAwbC0xNi43NzMgMTYuMzIgMjEuMjc3IDIwLjcwMmM0Ljk0NS0xLjYyNSAxMC42MTQtLjUzNSAxNC41NTYgMy4zIDMuOTYgMy44NiA1LjA3MyA5LjQyMyAzLjM2MiAxNC4yNWwyMC41MDUgMTkuOTUyYzQuOTYxLTEuNjYzIDEwLjY4Ni0uNTg3IDE0LjY0OCAzLjI3NCA1LjUzNyA1LjM4NiA1LjUzNyAxNC4xMTYgMCAxOS41MDYtNS41NCA1LjM5LTE0LjUxMiA1LjM5LTIwLjA1MyAwLTQuMTY1LTQuMDU3LTUuMTk0LTEwLjAxLTMuMDg1LTE1LjAwM2wtMTkuMTI0LTE4LjYwNi0uMDAyIDQ4Ljk2NWExNC4zMjMgMTQuMzIzIDAgMCAxIDMuNzUgMi42MDljNS41MzcgNS4zODUgNS41MzcgMTQuMTE1IDAgMTkuNTA5LTUuNTQgNS4zODgtMTQuNTE2IDUuMzg4LTIwLjA0NyAwLTUuNTM4LTUuMzk0LTUuNTM4LTE0LjEyNCAwLTE5LjUxYTE0LjIzOCAxNC4yMzggMCAwIDEgNC42NC0zLjAxdi00OS40MmExNC4wMzggMTQuMDM4IDAgMCAxLTQuNjQtMy4wMWMtNC4xOTYtNC4wNzctNS4yMDUtMTAuMDY3LTMuMDU1LTE1LjA4bC0yMC45NzMtMjAuNDEtNTUuMzg0IDUzLjg4NWMtNC42NTUgNC41My00LjY1NSAxMS44NzIgMCAxNi4zOTlsODAuNzc0IDc4LjU4OGM0LjY1IDQuNTI3IDEyLjE5MyA0LjUyNyAxNi44NSAwbDgwLjM5Ni03OC4yMjNhMTEuMzggMTEuMzggMCAwIDAgMC0xNi40IiBtYXNrPSJ1cmwoI2MpIi8+PC9nPjwvc3ZnPg==" width="110">
                    </div>
                    <hr>
                    <table style="width: 100%">
                      <tr>
                        <td align="left">
                          <h5 class="css-okns0k"> Git </h5>
                        </td>
                        <td align="right">
                          <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                            <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                              <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                              </path>
                            </svg>
                          </span>
                        </td>
                      </tr>
                    </table>
                  </a>
                </div>
              </div>

              &nbsp;&nbsp;&nbsp;&nbsp;

              <div class="card" style="width: 200px">
                <div class="card-body text-center">
                  <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                    <div class="css-1ctspcv">
                      <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9Ijk4LjAxMiUiIHkyPSI1LjUwNyUiPjxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiM0MDVGOEYiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM4NkE5REYiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxtYXNrIGlkPSJjIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjx1c2UgZmlsbD0iI0Q4RDhEOCIgeGxpbms6aHJlZj0iI2EiLz48ZyBmaWxsPSJ1cmwoI2IpIiBtYXNrPSJ1cmwoI2MpIj48cGF0aCBkPSJNMCAwaDMwMHYzMDBIMHoiLz48L2c+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTY1IDE4NXYtMjQuMTc4bDQ3LjQ0OS0xOC4yODhMNjUgMTI0LjMxNVYxMDBsNzguMDc0IDMyLjMyOXYyMC4yNzRMNjUgMTg1em04MS44NTItMTVIMjM1djI1aC04OC4xNDh2LTI1eiIgbWFzaz0idXJsKCNjKSIvPjwvZz48L3N2Zz4=" width="110">
                    </div>
                    <hr>
                    <table style="width: 100%">
                      <tr>
                        <td align="left">
                          <h5 class="css-okns0k"> Shell </h5>
                        </td>
                        <td align="right">
                          <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                            <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                              <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                              </path>
                            </svg>
                          </span>
                        </td>
                      </tr>
                    </table>
                  </a>
                </div>
              </div>
            </nav>

            <br>

            <nav class="css-1ewp9ay">
                <div class="card" style="width: 200px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTM0IiBoZWlnaHQ9IjEzNCIgdmlld0JveD0iMCAwIDEzNCAxMzQiPgogICAgPGRlZnM+CiAgICAgICAgPGxpbmVhckdyYWRpZW50IGlkPSJiIiB4MT0iNTIuMjIyJSIgeDI9IjUwJSIgeTE9Ijk2LjM3JSIgeTI9IjYuMTM5JSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiMyREExMUUiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjQTdFQjhBIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgICAgICA8Y2lyY2xlIGlkPSJhIiBjeD0iNjciIGN5PSI2NyIgcj0iNjciLz4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHVzZSBmaWxsPSJ1cmwoI2IpIiB4bGluazpocmVmPSIjYSIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNGRkYiIGQ9Ik0zOCAzM2g1OGE4IDggMCAwIDEgOCA4djUyYTggOCAwIDAgMS04IDhIMzhhOCA4IDAgMCAxLTgtOFY0MWE4IDggMCAwIDEgOC04em0zIDExYTIgMiAwIDAgMC0yIDJ2Ni40MjFhMiAyIDAgMCAwIDIgMmgyMC40NGEyIDIgMCAwIDAgMi0yVjQ2YTIgMiAwIDAgMC0yLTJINDF6bTAgMThhMiAyIDAgMCAwLTIgMnY2LjQyMWEyIDIgMCAwIDAgMiAyaDIwLjQ0YTIgMiAwIDAgMCAyLTJWNjRhMiAyIDAgMCAwLTItMkg0MXptMCAxOGEyIDIgMCAwIDAtMiAydjYuNDIxYTIgMiAwIDAgMCAyIDJoMjAuNDRhMiAyIDAgMCAwIDItMlY4MmEyIDIgMCAwIDAtMi0ySDQxem0zMi0zNmEyIDIgMCAwIDAtMiAydjYuNDIxYTIgMiAwIDAgMCAyIDJoMjAuNDRhMiAyIDAgMCAwIDItMlY0NmEyIDIgMCAwIDAtMi0ySDczem0wIDE4YTIgMiAwIDAgMC0yIDJ2Ni40MjFhMiAyIDAgMCAwIDIgMmgyMC40NGEyIDIgMCAwIDAgMi0yVjY0YTIgMiAwIDAgMC0yLTJINzN6bTAgMThhMiAyIDAgMCAwLTIgMnY2LjQyMWEyIDIgMCAwIDAgMiAyaDIwLjQ0YTIgMiAwIDAgMCAyLTJWODJhMiAyIDAgMCAwLTItMkg3M3oiLz4KICAgIDwvZz4KPC9zdmc+Cg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k"> Spreadsheets </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 200px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTM0IiBoZWlnaHQ9IjEzNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PGRlZnM+PGxpbmVhckdyYWRpZW50IHgxPSI1MCUiIHkxPSIwJSIgeDI9IjUwJSIgeTI9IjEwMCUiIGlkPSJiIj48c3RvcCBzdG9wLWNvbG9yPSIjMzRBNkQwIiBvZmZzZXQ9IjAlIi8+PHN0b3Agc3RvcC1jb2xvcj0iIzNDNUE4OCIgb2Zmc2V0PSIxMDAlIi8+PC9saW5lYXJHcmFkaWVudD48Y2lyY2xlIGlkPSJhIiBjeD0iNjciIGN5PSI2NyIgcj0iNjciLz48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48bWFzayBpZD0iYyIgZmlsbD0iI2ZmZiI+PHVzZSB4bGluazpocmVmPSIjYSIvPjwvbWFzaz48dXNlIGZpbGw9IiNEOEQ4RDgiIHhsaW5rOmhyZWY9IiNhIi8+PHBhdGggZmlsbD0idXJsKCNiKSIgbWFzaz0idXJsKCNjKSIgZD0iTS0zLjM1LS44MzhoMTQwdjEzNmgtMTQweiIvPjxwYXRoIGQ9Ik01MS4yMzkgMjcuNjM3YzYuNjE0IDAgMTIuMDM4IDUuMSAxMi41NiAxMS41ODVsLjA0LS4wMjR2MTEuODg4bC0uMTI1LjAwMmMtNy42NDcuNDA3LTEyLjE2LTIuMzAxLTE0LjEzNC04LjMyN2wtLjEwMS0uMzJhMi4yNDQgMi4yNDQgMCAwMC00LjI5MSAxLjMxN2MyLjUxNyA4LjE5OSA4LjgyOCAxMi4yMSAxOC4zMyAxMS44MzJsLjMyMi0uMDE3Vjk0LjU5YzAgNi45NjQtNS42NDIgMTIuNjEtMTIuNjAxIDEyLjYxYTEyLjU4NiAxMi41ODYgMCAwMS0xMC40Ny01LjU5IDkuOTgyIDkuOTgyIDAgMDEtMS4wMjMtNC40MThjMC01LjUzNiA0LjQ4OC0xMC4wMjMgMTAuMDI0LTEwLjAyM2EyLjI0NCAyLjI0NCAwIDEwMC00LjQ4OGMtMS42NTQgMC0zLjI0NC4yNzctNC43MjQuNzg2Ljc1My0yLjQyIDEuMDM5LTQuNTU5Ljg0Mi02LjQzNGEyLjI0NCAyLjI0NCAwIDAwLTQuNDY0LjQ3Yy4yNTkgMi40NjUtLjg2NCA2LjE4My0zLjQ3IDExLjAzM2EyLjIxIDIuMjEgMCAwMC0uMTk5LjUxNSAxNC40MzMgMTQuNDMzIDAgMDAtMi40ODMgNy41Yy00Ljg2LTEuNzUtOC4zMzQtNi40MDMtOC4zMzQtMTEuODY5IDAtMi4wMzMuNDgtMy45NTQgMS4zMzUtNS42NTVhMTIuNjEgMTIuNjEgMCAwMS03LjMzNS0xMS40NThjMC0uNzQuMDYzLTEuNDY1LjE4NS0yLjE3Ljk5OS00LjY1OCA1LjE0LTguMTUgMTAuMDk1LTguMTUgMy40MDcgMCA2LjUyNCAxLjY2MSA4LjQzOCA0LjM3NC0xLjEyNiAxLjAzOS0xLjk3NCAyLjQ0OC0yLjUwNyA0LjIxOGEyLjI0NCAyLjI0NCAwIDEwNC4yOTcgMS4yOTRjLjgxMS0yLjY5IDIuNTgzLTMuNjI3IDYuMTU0LTMuMDIzYTIuMjQ0IDIuMjQ0IDAgMTAuNzUtNC40MjVjLTEuNjk2LS4yODctMy4yNDQtLjMtNC42MTgtLjA0M2ExNC44IDE0LjggMCAwMC0xMi41MTQtNi44ODRjLTEuMzQ2IDAtMi42NS4xOC0zLjg5LjUxN2ExMi42IDEyLjYgMCAwMS0uMzktMy4xMjJjMC02Ljc1OCA1LjMxMy0xMi4yNzUgMTEuOTg3LTEyLjU5NSAxLjIzMS01LjY3MyA2LjI3Ny05LjkyMyAxMi4zMTQtOS45MjN6bTMxLjUyMiAwYzQuNDQ2IDAgOC4zNTMgMi4zMDQgMTAuNTk3IDUuNzg0YTE0LjM1MyAxNC4zNTMgMCAwMC01LjI1MiAyLjc0NGwtLjMyNC4yNzUtLjEyNi4wMzRjLS4xLjAzMS0uMi4wNy0uMjk3LjExNS0yLjU4MiAxLjIxNi00LjYyNiAxLjM5My02LjI1LjY4YTIuMjQ0IDIuMjQ0IDAgMDAtMS44MDUgNC4xMWMxLjQwNC42MTYgMi45MDUuODcxIDQuNDkuNzhhMTQuNTM4IDE0LjUzOCAwIDAwLS44NzYgMy42ODQgMi4yNDQgMi4yNDQgMCAwMDQuNDY0LjQ2NCA5LjkzNSA5LjkzNSAwIDAxNy42MzYtOC42NzdsLjI2Ni0uMDU5LjA5OC4wMDdjNi41My40NzIgMTEuNjggNS45MjMgMTEuNjggMTIuNTc3IDAgLjUzMy0uMDMzIDEuMDU4LS4wOTcgMS41NzMtNi4yNTEtMS44NjctMTMuMTY5Ljc2Ny0xNi42NTcgNi44MTdhMi4yNCAyLjI0IDAgMDAtLjIzOS41OTdjLTEuNDYyLS4xODctMy4wOTQtLjAzNi00Ljg2NS40NmEyLjI0NCAyLjI0NCAwIDEwMS4yMDkgNC4zMjJjMy4yOS0uOTIgNS4wMi0uMjI0IDYuMDQzIDIuMjE5YTIuMjQ0IDIuMjQ0IDAgMDA0LjE0LTEuNzM0Yy0uNTY3LTEuMzUyLTEuMzEzLTIuNDUtMi4yMTctMy4yOTJsLS4yNS0uMjIzLjA2Ny0uMTA4YzIuNDIxLTQuMTk5IDcuMTQyLTYuMDIgMTEuNDI1LTQuNzczbC0uMDE5LjA0YTEyLjYxMyAxMi42MTMgMCAwMTcuNDYgMTEuNTE2IDEyLjYxIDEyLjYxIDAgMDEtNy4zMzUgMTEuNDU4IDEyLjU2NSAxMi41NjUgMCAwMTEuMzM1IDUuNjU1YzAgNS45MTgtNC4wNzIgMTAuODgzLTkuNTY2IDEyLjI0MmwuMDU3LS4wMTJjLTQuMzQuOTgyLTcuMjI4LS42NzItOS4zMy01LjU0NmEyLjYzOCAyLjYzOCAwIDAwLTMuNDY4LTEuMzY4IDIuNjMyIDIuNjMyIDAgMDAtMS4zODcgMy40NTdjMi4xMjUgNC45MjkgNS4zNDYgNy45MDggOS40NCA4Ljc0NGExMi41NzMgMTIuNTczIDAgMDEtMTAuMDQ3IDUuMDAxYy02Ljg1NyAwLTEyLjQzNC01LjQ4LTEyLjU5Ny0xMi4zMDRsLS4wMDQtLjMwNlY3OC42NDNsNS45NS4wMDFjLjQzMiAxLjY0NSAxLjQyNCAzLjE2NyAyLjk4NSA0LjUxM2EyLjI0NCAyLjI0NCAwIDEwMi45MzEtMy40Yy0yLjEyOC0xLjgzNS0yLjI2My0zLjgzNC0uMjU4LTYuODVhMi4yNDQgMi4yNDQgMCAxMC0zLjczOC0yLjQ4NWMtLjc1IDEuMTI4LTEuMzA3IDIuMjQtMS42NjggMy4zMjhsLS4xMjcuNDA2SDcwLjE2bC4wMDEtMzQuOTU4LjA0MS4wMjRjLjUyMi02LjQ4NSA1Ljk0Ni0xMS41ODUgMTIuNTYtMTEuNTg1em0xMi4zMTQgOS45MjNsLjIxMi4wMTJoLS4wMDNsLS4yMDktLjAxMnoiIGZpbGw9IiNGRkYiLz48L2c+PC9zdmc+" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k"> Theory </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 200px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzkiIGhlaWdodD0iMzkiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxsaW5lYXJHcmFkaWVudCB4MT0iNTAlIiB5MT0iMTcuNzM4JSIgeDI9IjUwJSIgeTI9IjEwMCUiIGlkPSJiIj48c3RvcCBzdG9wLWNvbG9yPSIjRkY3ODc2IiBvZmZzZXQ9IjAlIi8+PHN0b3Agc3RvcC1jb2xvcj0iI0M1MzU0QiIgb2Zmc2V0PSIxMDAlIi8+PC9saW5lYXJHcmFkaWVudD48Y2lyY2xlIGlkPSJhIiBjeD0iMTkuNSIgY3k9IjE5LjUiIHI9IjE5LjUiLz48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48dXNlIGZpbGw9InVybCgjYikiIHhsaW5rOmhyZWY9IiNhIi8+PGcgZmlsbD0iI0ZGRiIgZmlsbC1ydWxlPSJub256ZXJvIj48cGF0aCBkPSJNMjYuMjkyIDd2NS4zMTdzMCAyLjIxNS0xMy4yOTIgMy41NDV2LTUuMzE3UzI2LjI5MiA5LjIxNSAyNi4yOTIgN00xMyAxNy42MzRzMTMuMjkyLTEuMzMgMTMuMjkyLTMuNTQ1djUuMzE3czAgMi4yMTYtMTMuMjkyIDMuNTQ1di01LjMxN3pNMTMgMzAuMDR2LTUuMzE3czEzLjI5Mi0xLjMzIDEzLjI5Mi0zLjU0NXY1LjMxN3MwIDIuMjE2LTEzLjI5MiAzLjU0NSIvPjwvZz48L2c+PC9zdmc+" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k"> Scala </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 200px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img alt="R Logo" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTM0IiBoZWlnaHQ9IjEzNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZGVmcz48bGluZWFyR3JhZGllbnQgeDE9IjUwJSIgeTE9IjAlIiB4Mj0iNTAlIiB5Mj0iOTcuMDQzJSIgaWQ9ImEiPjxzdG9wIHN0b3AtY29sb3I9IiNFNjg5RDUiIG9mZnNldD0iMCUiLz48c3RvcCBzdG9wLWNvbG9yPSIjQjQ0NzlGIiBvZmZzZXQ9IjEwMCUiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxjaXJjbGUgZmlsbD0idXJsKCNhKSIgY3g9IjY3IiBjeT0iNjciIHI9IjY3Ii8+PHBhdGggZD0iTTY4LjMyOSA4OC4xNzl2Ny40NzZoNi43ODh2Mi44ODdoLTYuNzg4djcuNDc3aC0zLjIzNHYtNy40NzdoLTYuNzg3di0yLjg4N2g2Ljc4N1Y4OC4xOGgzLjIzNHpNNDcuNTUzIDczLjA0djEwLjcySDU3LjU2djMuMjY4SDQ3LjU1M3YxMC43MjFoLTMuNzg2Vjg3LjAyOEgzMy43NjR2LTMuMjY5aDEwLjAwM1Y3My4wNGgzLjc4NnptNDEuOTc2IDB2MTAuNzJoMTAuMDAzdjMuMjY4SDg5LjUyOXYxMC43MjFoLTMuNzlWODcuMDI4SDc1LjczNnYtMy4yNjloMTAuMDAzVjczLjA0aDMuNzl6TTY4Ljg0MyA1Mi4yMzh2MTEuOTc0aDExLjE2NnY0LjI1NEg2OC44NDN2MTEuOTcxaC00LjM5di0xMS45N0g1My4yODZ2LTQuMjU1aDExLjE2N1Y1Mi4yMzhoNC4zOXptMzEuNTg4IDUuMTgzdjcuNDc1aDYuNzg3djIuODg2aC02Ljc4N3Y3LjQ3N2gtMy4yMzR2LTcuNDc3aC02Ljc4OHYtMi44ODZoNi43ODhWNTcuNDJoMy4yMzR6bS02NS4zMjMuNTd2Ny4xOTJoNi43NFY2Ny40bC02Ljc0LjA0djcuMjQ3SDMyLjc0di03LjIzM2wtNi43NC4wNHYtMi4zMTJoNi43NFY1Ny45OWgyLjM2OHptNTQuNDItMjMuMzQ5djEwLjc3MWgxMC4wMDR2My4yNjZIODkuNTI5VjU5LjM1aC0zLjc5VjQ4LjY4SDc1LjczNnYtMy4yNjZoMTAuMDAzdi0xMC43N2gzLjc5em0tNDIuMDkyIDB2MTAuNzdoMTAuMTIzdjMuMTdINDcuNDM2VjU5LjM1aC0zLjU1VjQ4LjU4M0gzMy43NjVWNDUuNDFoMTAuMTIzdi0xMC43N2gzLjU0OXpNNjcuOTE5IDI3djcuMzM1aDYuNzE1djIuMjJINjcuOTJ2Ny4zMzVoLTIuNDE1di03LjMzNUg1OC43OXYtMi4yMmg2LjcxNFYyN2gyLjQxNXoiIGZpbGw9IiNGRkYiLz48L2c+PC9zdmc+" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k"> Tableau </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
              </nav>
          </section>

          <section class="css-1pdeqys" style="background-image: linear-gradient(141deg, rgb(35, 136, 176), rgb(51, 170, 204)); padding-top: 96px; padding-right: 16px; padding-bottom: 96px; padding-left: 16px;">
            <h2 class="css-1k1n1og">Browse By Topic</h2>
            <nav class="css-1ewp9ay">
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1lcx9y9">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAMAAADVRocKAAAC/VBMVEVHcEwLCARCExMFAwEAAAAEAgEFBALo0MsDAgXN1sUCAQAEAgGauMnlmVF0TSUlGxBRW2H1smj4wm7A09IuLyy5z97D2eb3x3G81OHG2uetxNGnwtH5x2n6x23E2ebfiT/J3OrE2eXQ4e3L3uuwytnxs2euyNfP4e7kolbA1eNxiI+uxtT603b0q1K0zdy30N+yy9q+0+Dpp1m2zt3Rgj32vm3D2OWhvs7lkznXhzG50N/dijq80+H4zHO70uDbgD3A1uSuxdGow9JuZUL60HGeu8zG2ue2z93dhz74yHHplyf4wG7rpmDbgDubuMqdu8v0r0760HCevMz/1X/upkX1r2bzsGb1q0f0u2z60HXE2ObxyID2r2f61XTxrmbzqzb5xXD0rTLqmzn4yXHbzbu60uKqxNT3tWn5ynL5w2/60nb3t2r3uGv3vGz61Hf5yHGgvc6uyNe1zdz72nn72Hj4xXCwydieu8z6zHP4vW2sxtb723r3uWuow9P4wG/6znT60HWyzNumwdGjv9D703b61nfkkDzmkzr3u2z3v22xy9nolzrijTzxpzX2smj913j93nzlkkHsokbpmDa92en60XLhizflkDPxpi3voTHuojv70WzysF/xrlPrnDX1t2f/vW74w13rnDv/wnDso1XRuq37ymjqmSz4wW33xGXwqVrs0ZvC3vD6xXH/y3S71ubnmEWlxdfli0HlhwP0nj7JzM31pQDT0sn/0nfqlhjqkgH1vGT0ulr90WX/1nn/t2v+2G7wlULeroXI1Nv4pT3vtmbe17/Qw7r00YTl0q/foGbWs5z/8YbconXghSXA1uPL3urM4O7F2efQ4e670uDP4O3C1+TW6vjN3+zG2ujO4/G3z97D2ObA1uTW5fHS4/C+1eO70+HQ5vX3qgC91OLH3+7gyKrf8P7K4fK50uDJ3erI3OnB1+W90+LzrTvtuHK30N/R19XG2+nM2+Lqw4j2qhnquX3wq0bD3ez8sDHjwJX6ukHA2urztk7zrDPaWlcfAAAAZXRSTlMABAEOCA0QAQYDCgz+/hMUFPz7BxIWe/wN/FGZMyLV/j7+/LTJlPrfYpQgbvsN3/ys/oTqOOrlvOdNIpbNjvv9XDNJDKbr7PbLarLLsX5ogkDw2hScvMhwruSlwsrXz8PW3cPM/lQxlHYAAA1YSURBVHhezJS3b9tAFIe5EZwFEWIGQiYHwUPsGEE0GIHgZFDWDJk85/+9xl7Vu9ydWpB7pC2acixCxUA+6t6REPD99A56FP4nRM7zySUplUvS8+ihliqV0rNEgF6qyrrabNZ1+R1/2O1JcX1ZVid2hxCHdGxXh4idno4k123PVRSFUl7djiKL0MTOfn5VtQn3LlCmtlqFb3bkl7GX6DOo4k33S9DEDvwV3aaKSxMvzZpwOztoAgTHakfZo8u4dM8ik/0KDMV2/nd1by9I4NqApjdQphhju3YC/9it/E1iBY+gEzOO4yiKbVuHc9rifOqP/bNgAm4g0uKhrZcFaVN/RfVQMMvgesuastS+iKgdb5bAj1b30MzKgQInzhNpw9pmPUiC7KHQssIwKamfsngJFmtDXRA38VcpQiEK70Cw3FSZv3jCiSCt7y/Xr1oIoRYvANxNY8bgs3Rpdq0iiGsHnI6OWlya0ZuCDcinMKYNZaEkrul/0T/qPQD8bBmQA1pUW/MFLgol9QoCMuYuexozQrK0zkxLgtxvzO/oQWkH//Ka7A7N7b/NzXThCDdaRrsN4oS50TKZCUZQpvX+zgSY27gKs3dfccB+v8G9RgZm5ioYbjfG/YP3WRNFDYA8Y0yZuRqnjajikdNsqIsaGHOMZBmGj+41zpMJ2GTMcYa1anGCKBx/aPvca/jJh4dg0wHSDICkOwTmdhxpxQmSIH879C98/4Ivw78Y+wFYCiEjoHCqYQYOWoe+P+aX4QNG3kOwM+JrCUwuB4Mfg8HnCKa6YIhD7n+Au5Bg4pDLm/ObweUofV5UTK5/dxN+vvkIklUBr37lAnpckULINcHn3dtu92yAHQxu2EZJxpfu1zPgz+33Ty9XTLUolF+P/9JdBqFtW2Ec3y3s0NPYGGtzKARf5oaxnTYojNHLjqGHscMYgzYoOkhEko2xwJahqok2M8WZF0meAzabHS8OCYzYS+IU05I0WXvwRCkz6WGjOHTWptVOWrtJZPY9Pb+43pLfQ+/p6fD7631CfjLHcbsnrfYb8VcPuwf/FG0tLfIbC9UnOPXJfqkKtT98bHcxtn30xfsgOnMBHx67fsIDkDQayL//vNgtdh3NEVVRrRw068hfL20+3rxWIn47KYi8UNm4MnxWmYagQtzLmEiPDvC3NYfhQSHwgnjkbNWrjfres2c21Izcf1KFcFFU71+Gn43T/W++9zTC9fmzQTgstnVH4KG5iHKlWYeLXbtrA5oGei0NcszYJ6d+MsG6PtpVOC4CjUOD8le157e+03SHERiZIFR2rOdFuxuLxbqanU9DRlo8Ifni8wunPIih115541iJIBQAEqyevr4f140E8zI3tsGP9DHNSaiqnE2KPEGU7Rfau5f+mwBVe2t0F6kjCqegjD8aFtCwGvuHq0ZbooFkMkEzNMDcAD1CK/OiIPCiCPXDwEnWSVc+uzqYAJvS+evgJ0DCIzegVi99v2rkUrQkScmF5p7NojOaxQFQGeH/iLzovX8FnANfEiPf+P1Y7kfNz4G91bKs+u21+FybAmtiq1VYyRmUFA7TZR359bwID0SQcQcjdDB1US/gBLINjC55kN/fQ5l8YNVaENBqZjIzZSkcCGe3CpbVjOfKUiAcdnTXz2MZcct9vCpeArn/0ese/yAPUUCtVvhhLZNhA1Qgu9CpQb1yM6kARQXysZihpwXGRZbxQXBnX3uH+y/A0MgS8k/6cYd6xaxhbq9GkZJimx037ssUhWjrugP+s/GqH/cXcH7JMzmAP8hhfeviuZnoOsWy2QM0M3/JRVMwY32Goa8LNIho2u3x4M7xgGpE/G8/9QSDAwHB4N8t5DfRCpbnF31sfufYNAt315aj66zPx67rhp6X6ROweoBbn14iD9ijBAeYDPp/r5mYzp3puM834Utf65iFleXozfgETBdTcxDASDQhseG+IviChAbp1thVsgmgBRA1Ypx7RPxmZ+VmdHMxFErvFDor52anZ2EyPzFlzBkGRUsY0G/vlPbsMsVIWA541ctkF4uMBwk9v9mnc++nr+KhqdD8wd17s7PT3/44MRUKhWABOYf4peT21sXjwp1MzsjTYQkT/nlsuLcNj0TGCSAfJ378p9J8+C8h5hvSxhnH8dO2GaM1ijoy2m4yWEHXjTE2q1U3xrrBqrCVrS82+mbsT9TpkVQQcyUmM4Ek5x2yxBChi8nQOLUu0U6luo6CVXBstIExfNElcEo66oisbFD3whf7/vLcY1Lc6ufI7+Lzk8/3njueO8+SUCjmkGVZujwbUj1XHJLssG9HIuPbNiw46JxY4Ms7vzyzUdO8EokggUETeJw9ZO4zN6mJ3i6486zduu2PSqIoSlI4diXqoG/b45HLPzjJT3w1ldpJrf1xqqJ5ZSQSsDk5uZVcLAj5CTD/xdRD/p2x2RIfQ5Ik0Y69PRAZtjA9LfCp3O/deV04v4JFfx3rHKO2hQ8r+BXozXmp0t6spQrJ7ixXhTrsnXbQCVDtUSxtZxskwGn7cZn8GzWC0LyijAwOowPanDY8OekS1PW5egtwbabYS6tekLB6O+brKCQwDksOuL65FlxLJTeacG/+eA4BAfipYVq4INAZanCZXYX+fkgLoYT5W+HOVk5Ha2BwcBIOgu5QwbVs8t7TTwjCC/UjyiAC9E7bjVdzi7jLVUhvJruH9E765mxJ68BA6wBKOMAknOVgOnkH/grh3IiiKIMBC+/YTO+zMwQtNkZXKruXZFYbqxqGe2C1BJdRGQrbuAUTSKY3mhro9aNx5aEA08In9P84N9R5zBoJ02lW01Rpl8Zp+v3Bgz8/nV9USCK3WXQmr03f/aum8vCZYjpD6A0N857pBt3s3jC7C/zuLgj/k2QwiNfl4Nj3M4riDXRbOKa/7zUdPWgoKzUK1XNeCgi3sW63aYFu1+/2uQHcVN2uTPr/SCZRgquLXi8O0tKdI949OXXqZOnBskpD5SFj/YiXuG6Jx+PUtZiOI+BYH4l1rP1ZTSMbK1ACDUN8r2EKfu9QmAIIU+LCc0WGMoPBYBQa52Cn2emUJz6iZVB3yV3AJqk1wIpu1zjoTt9c9HujcS5ZOCHg8Ml/WvECCm/nPboXVbzXa8WR6xMwa/uRyU1BbtchidFgOGAUXqlXPPB7Zia6eTN+nALeclmtVjd96AylM/sG/Dzr98TyASeKjQaj7gehcHxCbyVaBPAkAsiODdzXMnuAlSr7mtFe+mnI48EzYcJBlCc+OGw8UFRUrSoeIhRDo11vncUEWADHvZXZh/Xg2KwfIr+IJxCQ47VnhEOnm5e8zD+DUTAxIZcnaumPU1wDl7UH7h7azI+Ub1E8TUCFKkp2ov1849ueJVWl0ZA/ikEWnGg5Bz+ouwoz5PS5qK3DQi4Gjnhd/4mN/zqNCZBsRpQJmByjc4pHpUw1NAO/JBOjtWf5e9SxS3DrEf0QcqDM7zIoBAV8p6pqCM9NQDapPKyGMIQSEx1ML47WPrX7svnO1Z5d+hDwKDZf+3Y+J/P7ZInjc0RjfgzFog5JlIAoiqPVQkWxwGg4kg/4beszBtl45QX67D9NR+v9VZiA5BN3wYPUHo2i+vRB39e568soxkVAxBFsCICKAy0KD0R5MXP35ZOlQuNSlapGRV8hkBcO2Edb/m3P/FnbhoIALj1L6uuTrT/UhVJTtNhgHOgrjqGElogklGRIGigk7VC6OR2swfUSAl5Cl0yGQumUQWBPGWriLxD6OUyHjN48evDQexcp4kk19eKtP8gtOf3u3Z0wsvxAIckb6nYem4A4vp0AzYkEuCFsTma8VFMtbWPked7lRTfRdTFgjAt8wAbiCofXeeQ030kKyGzy+fDFlppzikVz75sHIwq6iwgu3J8V0MZAqdL1Kfrz51ggw5T/huFrTHyqWSq04Be6wWJaQb+KBZIKb9/cNfGlOU27p6C/ncHwqeMYgG4R2ILXC1oLgH/066ZFpZ8QlceldhsKnDSnyAT/IAJ8PNuF4RdRnwOY9Wo0GnQ/L6rg9iumbdsmkb/Abr+DJtrTNPxu+IZTRL+u66bOlI29Xz18fhEBYkLLbYXCL3qVmoDP7cP81/z5RNKP+XBefqqZkR78pmkytqPsHBQCEEu4rgshCOvMhvQcZGqpJrZetk/E2zRhxsAns+drBg4fEHr0M22HKo2rQcGDR6VY3+uF/dB1Q7dKLfDrkKdpVJWbgGUPeWccwcdzafj3eoBS5cAf+f4liBEotF//FIbHDeGHXNQTIr9KEMtuDjnvALwzbybDT+lVAvdeY2/kD+4L3FSZZlfrNrNQj37UZ99WzL/DzTqcTXH4kj72kyj3wL+CySNi8patUQP8Jh4fszIQWPZaeX13vVza1jXHSXYr66MKjX03DG+A/r5hA1Z8fCodP92E8qRWs1WtmNktTkc6jfLsfeXo+ONRpSb8IhmPn9VLyxa1defveiLnCiBCsVzkz04nC15Bs7vF6WR/68deiKIZRrLcZSAsvVt1QSIh0QXxdJZFZbqk/ydq1OTyUIbjWbptEqUtD6HxrbkyCMXprBJCVqr/zx/p+AVhlz4KwAAAAABJRU5ErkJggg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-1unwsrq" style="font-size: 0.875rem"> Data Engineering </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC7lBMVEVHcEwAAAAAAAADAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQAAAAAAAACUoKYBAQEDAwQAAAAAAACxxtTC2Obv1Ku+1eP0uWjG2+nhmUvvrl7k9vsjGhG60uHL3uvf8P/2uGvur2G1y9jhkj/ejTnnlDzLmliGZjzh8v/g8f/h8///wHOYpaqWs8awfFL2umvR4/DyuGm0ez6ep63h8v/g8f/g8f/g8v/h8v/Z6vfh8v/0tGlqgY73tV/LnFmYt8jmqV7zt2rimUjpnUflkzu80d3Sp2vikDvekD5ZQCWPrLzh8v/j9P/g8v/j8//B1OBOX2q+0uDwp1XikDuJprXk8f+Xtsf3tml9lqNUQzC3zdzlkjvg8f88PDbvqlj5ynKoczn2vW71x3Xi///g8f/k8f+40d+30N70tGrnrmW20N7c7fj5yXKSsMGmrabmkzvmkzv3u2z4vGyivs+nwtKeu8ymwdGkwNDpmTmfvM2jv9Dnljr4vm35x3H3uWugvc7qmzj4wG6auMnjkDybucqZt8j3t2qdusv4xXD4wW7kkDzmlkLso1Dom0fntGnemkuvure7qo70tWTOrHj0xXTaoFjVpWWytq23sJ6uvsDwxXeuw9G50N6vxNKswc/A1eO80+G4z96xx9W90+HC1+WtwtC+1OKvyNi0zNvf8P6kvMufuMeZssHypzS3zdurxdXZ6/ipwdDpr1TW5/Xwqj3T5fPE2eexytnBzMf50WzzqjS60eCzy9q3zt372HiUrbysws/72nnc7vzK3uusxtbH2+nlkjuqxdT6znTR4/G4zNj71nf5zHPaw5GdtcP60XXj0pfzs0b603a1zdzx04L1vl7KybHes2z5yHKow9Pfvn+tx9bkkTv5ynK8wbiXsL/A1uTLupavwsmrw9Lg8f+40eCVrr3vrFXpmz74w2/4yHC70NfS0bHCq4e0xs/ZrWfC0tb1vVHN4O0Ls5/BAAAAd3RSTlMAEB0SDi8JAQMHJgItCygWBQYqBBk+GyAjH/z0A/Tj9A/+Cx309PTxxP7bbER/SbjqXBdIxVH79NddPpf54sd/9ElzTSBl86bOkff0/HTRsTmmbi7ZNv43+yq8jTnjmGopr+2mMPC8MFPbEtAm848wur8d87JF5z6/BtAAAAkeSURBVHhe3NQ3bxRbFAdwOr7DaguLAmmxJdvvFUhjbLBEh0UBzysXbuBJSIQKxBeanDfnnHNwzjmS8wsd99w7YtdIaMezrjj9/HTOf849l36rst8bGR+9QG/yrzeoJqYviLs//AZXdOaB/QK452NuzFWcNE1TfXNXbl7FnLtuo2lV/bNfb3ScTNt2AKeW+gSnJwgXmqKxp3J9gc//JlxFYYErqRwHoPXwbhCvaiPTljhODVCWF3l0hHBxHF4JOM4h81bByadGeDN4WhU4SeB5zRr4zFhkd8KGNeBYkdc0WbYCPhkzwvvfhv8tcGqAB09AoIUrQLjoHJq2Ex60JwhZytIVgPCcLF488CRZ08DLoqKsXQHyzsi0LKPhaQXwGOqcV+CX4WGPYUTqvFcAwptCGo05zqGR8LKYY/J56txXoOIk7ww8Sea7PTG/vm4WnJ3ohMfkSXosgzjZmPYt5gA0fwWMIzWX4FUcnnMxsvcvAkl3Yh5AF2X6CpAjJbGqlhBweM5IJpOJLTLAkfZcrlzOBDg50jlSgURAlU9YThI0bS8D5Y9swb8wPDPgbNeRohUlwLE0hMenMrhW0+WVzz+4ZLI3OGwcKdYhs5JywsLmwc/YI16hXC5vpBDnAi6p9wbxwFWalpSEpIoS2bzs6xwk6M+ky1CFL7g7PambAGGbQ2jzAglFhc1DnCb5ir5Yxu/3/0e8QmHFAe3p+r4J0O12V9HizSkusnmaa6lYLHoxmMbcfGF+fiOlg1czByqqypVYFB4aV/QUUb3Tt5EXQxwq5M2vrW2BZwasVCrKj5snhN+Bt5RkUhEMAgceqhW2Vgv2Bm8hkDeerSz5gDt+LaKHMbdNQOBwLW+kgk0TYCgU4lkGjkDAi6f1rDNkkU9jaZwf1taWl1sLW7vmwCritKwHT+vNAUc2+SiSJvOCiMBWa/GlaTB3DJxPYkTi4c37tr1J+gMO1+PbPcFoNFqVZcEH4YXRu9W9YegOg7p+urrc8RZQPeoFXo/Go3VZdhnhuTyoTXhoZJH1/aPIp25v4XDgJ2BgaOAsGI/H60JWXPImUXhhGNzb4dDmvfpnEzyDO9z54wxnH7zTeDho7wbb7XYdbijalH0fzlGHaQ2uFmw2v74Hr0W8F90fXx661mg0Dg7uDp0FE9m3aPNyxtbADfCEiRcMNnd3X32IfUQegDs73Rl+L7xeetq6oigAx9eujW3MNdjxQ21p6UOgvAZRQGSa5h0F0lGVVG3TKlHSQZ+qahtjGzAPczEGO+8mTVpVKqgIqgYJNQMYJFEHCTeTlgmqBImrIkWUuFImzLr2Pidcuw5i/4BPa699c0yOn9Dzej6b17TzDWvB31xdXZ2mJ1l8NXP378xMYPFJcJQO3uI3fX19fxIHb+GsUd4hTpfPZnN14Z6DxeAYPhUcg86MZR9PEgyPuAeL0e+eAEys/E5c98dGeR8hHjgte7qHBhEFeO/evR/vzt/hbVHdI7F4TMaLRqMUMNF3Cx7mqPS2tejsadq5wTC4wcG9EnxldnZ2/O78BI77CJ5YfDLGHHkImEj0JRLfL/y70D30cDNze1v1EG2rZc9fCcMDWA/LAPHhRR9jW5RHi/8y9WBKpIvF4giYSCRuwVsYWrgEEOUd0THYNlcHTgR8vxj8SfwATc3xtjehgZPepQSGAiJf9x8ZgFu/Jg2cvhJmD9xg0/E1cHp2egQayhNn/nWKvSgmHot3fguNAnZ3dw8NXRrYvK0lJI+L8nhdBvdsMsDp6ZEZ3FY8DnGxLYGxeDzeeV0GBIiAA3OtrGFQHg00ePXsGeBtBISHM3M61mLstXHArjrKh4BPczqDKC8p1+X+dtoM8MWx6bHbExOPqbxFcVuDa7tOXFfXVQYfXmQOsxJmT/TX8JnTZrMVgWNjP+AVmJyLyfLIg9bZ1tvbe43BC0OUr/CEPZTXE4FHIrzasx4ngbYSkJ4B5oo9gDcoXteTqwyuQOPykpLD1O95tdoN0VkKjuJVWfuQWeN4HR3XCJQBh3ME5laSyUg4Ir++A2csLjtEmdEAEQ4j0sUkB/BGlwyIfQsUUDsXTmLkPQ42qmaLxWqt9lSwKMGlpaX5tdvGY3yLNorXnrnI4AX2kjm8U+dSxrq1RwP+oNdsciGjpwh8mUD2kE5wnK69PXNZBBwSAXGNXITjRQA2HTrlqHIE/D4zIpaD8tNDeexxvMxNChgKXSCu0KRns9ppeBHO9/mOGozD4VdUk4t3NsDxpfGfkQ4juQ7mMgOX2TtfIHC4AQ+VdgXxaJp311RiahAx6LW4rPZSEL/JctvO3mfbDmQG5i6Sl68tgCvUnmwFyPl66rdXikHEgKI+FxTbymU53kD/01AIz8DhAoHDR73exhaN0jUdOFZZ+QIGoOjw/yuPjI/MxDvjcluOB65/LhfSMTLg6z6fEtxaF+lp2EEcg1ShajYhoJs9Cb6B/9jMcHmyPeGlL+v0KB/mfKl9PkVR/IFj21Ge5CqraF/VREcuAye4PJGOvHR/ev8WXQYcTqWayQs4qmrAGfGCXuQTXoWtGLw9MiE8WR7ipd8+dUTPIyB7qX3BoL+KOOnxfX2qiv5cdvaYKwHlLeCB2/WWouTzAJuJSx5U/I5ijq/L8Vwin7MEXF5efoR0BAruvQ8UJai00E9kisBUY8BRA09ytC1xMp7H2FfMJwDvd2TQHiadTu9/14/CfN7dmpbNIV4q2eCAV3LbZ5ydOKfhGWCG1+Xy3lHYU9UzJ7QcLdy0A/vCKy0PzxbilXEMji5PgeuX5UlORYjdW8J4XbZjYXil5cljGO0Z8wX+7poCJ8oLgoMHDSEs5gP1zfv8YmN0V1YeuDLP9tqn86OLAFHeybV4yIAQLovFZMb3jJUxsjx+Uu3VzJV5LG768sNFHOMrWR7HI86KcVnM3mDAIUaWJz8V47ZlonNT4y6UFySPORNx6LzaLkR/AOMnzlS27XrjUQUn48mO3BBNZtWn+MGt86msO3a1OB5+yGzOCrfIKMbE3Prblq9u5Z24Iw83VMEibsPjEuUZt9h4nFbelp43+DabE6LbbqfjYOzP+Xe2MWmneE5eR2T0uJGSpnqdW2xIuivACZBDwgTq9kDbkPsPvILfIStLd0QAAAAASUVORK5CYII=" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Programming </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC8VBMVEVHcEwAAAACAgMAAAAAAAAAAAAAAAAAAAAAAAAhHhsAAAAAAAAAAAAKBgMAAAAAAAAAAAAAAAAAAAAAAAAFBQYAAAAAAAAFBgbt5M8AAACww9K1ydfN3uwAAADxpjbtnze90N6nvMrL3urT5PHp/P+vxNHW6vXN2eXD1+bJ3Org8f/g8f8mLS9MWF7R4u6Wr77W5/W70d/J2+j3wmbzqjXh8//g8f/f8f/g8f/e8P3JsIzf7/7e7fb6z3W3zNy3zNre7/ypwc/g8P6+0+GfuMa+0+H71njA1uPU5PK/1OHI3OnF2OXh8v/8xWT4y3DwpjXtoDfc7fzf8P7zqDjg8f/g8f/e7/6Ee3Cjvs6arbl7kJyQoKpjcXyIoK680NzV5vTi9P/g8v/g8f/5y3Pc7/z6znT6ynf813jsqEL3xnK3ydervsvH2ulqeoJUX2SiuMXK3evh8f+luMa5y9p7i5ObuMnZ6/ri8//uozb0rTP82Xr4y2L5xFj1tkKBRxbpp1Lgx6bfnFHvojbuozf5ynG5y9p6h45ET1LY6PimsLuXpbOpwdG8z97K3ev3uWvS4/G/1eO0yNfa6/q+0eDO4e62ytjE2OfD1uTU5vTY6vi6ztzX6ffQ4vDJ2+rA0+L3t2qyxtbM3+3C1+W60uHG2un2tWnJ2OWxxdTV5fSqxdTG2ei3ytrR4u+4z96cucqlwNDN3er2s2iuw9OvyNihvc7X5/W91OLQ3+24zNrb7Pq1zdzc7vuyy9rF097HvLLuu4Lt06/Kq43WmmClusnprm/OqobDmHLg8f/4wm/OgzbXijf0umX5xXDikznmljjqp1D72XjcjjjrnDj4wG70rDPxqDXvpDfThzb60HX5ynL3u2zhwZz71Xf5x3HxtWTxrEP6zXTsnzfJfzX4vm3pmjn1vmn2wlv3w2rzy4zenVXpqFz4yWz5zW3m49juy5TYomrczrzuoTbYr4Txu3bc3Nri5ubp3MjRk1Ph7fbe7/3vz59fmnPFAAAAjHRSTlMAHhojCQQhAQMCBioNDCYXEQoIDzYuMRIFLNbazBSAfszQ7b8NvyHB+NuI80FU+cfO77H3aig8Y+q3Cdgbv/TmwebM6dr2v/rx10FrmhryzuzvdzNP3eQZ75yFf26rm99GbaiSM996VT3vy4LpXDGq++9Y6Wvw+1me8K/8Zd85kXzhUJzD7k1K+TE2/oWyWVYAAAeMSURBVHhe7NW3TxthGMdxrx7YPZkFMZjxTI8liyFTXBQTsVJDGNmA9PK/vdd7d+/0mj7leXBOkYcI8PsOGfId7n7TR89w0sX+1v/G3qyw5BLpfdtOs/NevbaxF6y8bV1/iWCBFXig63rPcWy7xAh8rEN1x3HSzMCKXukBmGQFVqDTfxz0K6eu69KCpUIxmYCR8X0WYG7PvXLdj7cgRA1u53vuFfThN3gCe5wG3PL9PIqFOCOwAXFNKB3L4D6BSXchKnVQigtswI0gaASc2TSb45mg0QiowVyA1U3TLH7C1YcFINWJEGdCOxH4lgrM5auQ1ul0JBx9GOXUBI34HJiAGwLJ3Gx8dHBhR5blqqaqUhXeXVVVy4SQpRmKE2VMUSR8dRVFARBanxyNS6ztHsoyL2tGjed5uWsYxgC8PHo/yu9qdZGQrzymIMhH4OcLA3r3UG56nmCHSB0jaEmiKJbJ9/OaJkLfspsP4SZSP8htZxYmwYOXPM8rn91YVluDJV6SJ4/uy8Vn50jUuRUlCYIHHNQ2BM+7JmTqaeJe3swS+dOXdlRNEGuDZR0LgnBBoGfLd3OT62Son2EEHg1W2A7DVqsFF2LZ6Tu+5dQvPuqet2kgDgN4BxBiZGPolCBKqFSFSoWWMlRhaGm7FNGyUZD6AehSvgIScRwc45f4nOicpLKNIzmK0pA4fRVg8RU6dChTu4AEobmNi+u0du3zI8vT3z/9fdJzo34Pr3joJnM4yM8fx2dddwAtRrXx3YodyN5hIJnjb73LgfU3pKNcS9kh6f3J9JfDr0GKJ7/9I9Ovw7gni3Z49jL+HP3qBmamJgI9e+4cXviKRU8yJzthQ+jKxfZi2SbnXwWnWCnip3J08JcwNfnI07MZOyq93Yqb2uk+Ini+i21kHQORK9ZqlRr2znY2zRYRnIw/dkF6bgZFiru1fk6/b2/iGOGzaCrGDcAxw1hNRYHdvneww/c902w2QshUQlHuDbp9d9wwjOQKGUTd3a/7265nNpv01f9eHs4qyrjnJp8fbjbHZkbJJGNub154hkH7llyPzxa2ns0P3fBeWQsxyI61bHJE0+OpaoO5IMXZra0HC4H+TSQhhMAii6hheryGxFgO2dJLpXRiLdiUJUlgKYoSUASpXnoi07IQsi1YrVazQHp/tSnTDCNJAk3hNGxyLPXck849BKrtdlVWBUliVrxNmUKMKIqSoNJQpigoRZKXnlBqt9sKoDGIP2c8TVnCoIQ9gQZQxoEMWUQt1xMLnU6nJLOAFrCId17yNmXDWVAAgKVkvazrrBVBWoxlMVy9Xm8rMuWC4kbc35SkA9IOyHG6XgYoiqQ62Ctx+gWYDDZlFYOAhVRZ5/gsh1GBCIKOpmmdHM/pZQqyQFBXQ5sSn8MbnoO5XJ+UxVBOqmKunk+7IKDnRghNiQ0DvKKs83wunc8XFEWhgkdpZbUPmvbxcx/kdRmy7MhTpylebuj6tVsvX72FMJFwzpAv5NJfPuWzmGT9HJK1/43T7WraYBQH8GkS60uiiYmWJg3ooOCgpXQXoEgro1ewvl9AX0dfLmEbmxEMXZRlU4IZZG2oQiohuH7rh93WzvOYmW3ZyJ4P6qcf//M/ntFo5Biap1z3u6jDxU3TvDt4yXFUIKbyHMflyXfP4VIWj1u9Tr/fv9Zt1dLb7fbg66/l3QPXtCwcsN/v9I5LcClL7xkGCclZPg4ewzD0+bNW61WpghPqnmdYxh2QnZ9VfnoYj8ejpmMZnofAbuWwOplI5yzNYJGaRZxLI5DmWfnoaat1XPJn1lTLMe5c1+2hKr+5Y+Q5jqVq04kPa5NJ9SguszyIXBqPHEw9BeOnB196vc0agIquaYZlOappuu6H763RcDhEnGVomq4o15IIl3J2GpuCZAp7QcY5isNgLPGmBJcirYFo26oKovNgmmYTuHHTwflsW1Fel+FSyifZBAYZKjX3J5hKpWlWzsUSRGZ3/fa2IjYUxbNVA4nWxIR8mDPA8xS97Nzf13cKGSIRy8ksmUyFQBCTFD2fg4jZjLBd6XTWRUXXbVsD0zDUx0f4NBBn67pYv7lpbi8XF1DAHJ+mkuD9DUxzSCSyC8W3S4NOR6rpQGpq8DQNuPoGXMrqilDMZAkYmMn/C0QiyeOMCwXhRBoMumIVUno25IQHX56uV8vN5s3GnlAsZFC+eYZEng+GIoLIyHjqQlHYXR8MGqICZwgbR/889KO8NRpt7Qi+l6ORFwqIwUDk0WYQubz9ot2uSSD6TxHrcCn7y5gjEvH5wPPBkAhT5zmGj/vkyhJcinQBlwivf7EBlwLl+ZxMMxzqL/DCIu4xTXI0mhstR7iUXNcUG91ut1EejsdQngDLJYiYzNPAoXh/eOHN4JA0zO1XuWa6a2K3fDUcXgXl8Twc28wLjJAYkEGVZ3Apq3Apv5UXGS88N8MGVQI4Kw+mZTgymgtEWA4mGVqOTcnC5f5esYA4AqZFu/WXgb0oMZgbyNyUxA9zLHD+tCEvmiRxlbAeAh4Mi8uL4KK3w8I14hexi/+tEpPoIS6qvGiSQiTJ8TLPcbg7KoKLnhuZ/gMtctofVGdrkVEKyZYAAAAASUVORK5CYII=" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Importing & Cleaning Data </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC8VBMVEVHcEwAAAAAAAABAQKlwdGevM0AAACkwdGbucqfvM3C2eUAAAAAAAAQBQUAAAAAAACZuMnC1uXE2ebF2efH2+esxta+1eLI3OrN4e4AAADW6fauyNfB1uXR5PIAAACvytnh8f+70uG1zty40N+1zd3K3evR4/G91eTX6ve81OLK3uuwytm60eDc7vy50N7V6PXB1+Xd7/zg8f/d7/3R5PHH2+mrxtbj9f+yy9rK3eq90+GetcXD2Oa50eHG2+jO4e7I3eiYt8iZt8iqxdXZ5uzH2+jd8PDi8//g8f/V6PbP4O3d7/wwNzy1zt2Ws8WXtsfi9P/f7/eBU1Xh8v/V5/TR5fLT5vPZ6/mWs8VshJBTZW2IpLOPrLzB1+fS5fLY6vh4j52ceYO4z96Cnay/1uW81OOvytrg8f+3z97O4u6XtcbL3+3G2ui+zdfO4e+Ztsjh8P+ft8+dusvh8f/N4Ozi8v/I3OrE2earanCRr8CZuMXI3urG3OiauMmbucqZt8iRq7qEmqjOgkV0d3S80uC1zdu2ztyvyNewydiyytmzzNq4z92/1ePG2+jg8f+nwtKtx9erxdWqxdTC1+Wjv9C60eC50N6vxtWKp7isxNLP4u+pw9PF2easxta91OKivs6pwc+it8TD2Oagvc3pmDnG2uefvM3I3OqJprelwdHS5PJ8kp7L3+ykvs2kusiftMHa6/mXssKyiGWIpbWOpbGHmaTB1uSow9Pf8P7W6Padt8fxpzWRrb7mlDvqmzieucn2w2/toDbsnjfvpDZ0jJnkkDuRqLXgij6Am6rzqzfytkzqwGjjvn/yrVHxt2+oeVGPrL2Wq7iFoLBmeYXUxZ5jeIT42HvijT2eu8yTr7+Nq7udusvorlbuzHqxs5vJwq6RfnB5iZPFhFGok4b3umTUi06VcljZhT/solNka3HtpVDtsW2nsauwmX3a0qbQhEdrgY6aoqi5oIN9fXm1noHMtn/SvIO0vbeZtcT1tlDTvIOYJilxAAAAfXRSTlMABhEdv4ABgIC/vwsDCRQXgL+/gIAyv4C/Db+/gL8avoDBf4C/v4B7gIB+gL+Av74x/Xt3fXuAHL+MvjX8+r6ghfRfvwP9CzxUuJD0J73G5C4TG2Kyp93ts0o2h6NTrOVfKL1u/PymbWeX1c7Fj9X7ZyCSyPxHysNPiijLyO0aL5UAAAj/SURBVHherNRHbBRXHAZwTjmMlMPubHZX9lqy3GRj4921AYOR7EiBEDsXSgKngJQLQgEUCQKH5Dh9tvfe3Huv1N57L+m9937K/723610Uj7UGz2lOP/3b+5Y9xff8siX9Gt4u3tqwdNzmV4qLi1esfGP90nDrN9aBV6HR0VuWpPFN9cBVLtfRNL1uCRpv2Io5jYamX3y99Zkbf5kMTwOeptlY0JpufNmWZxoejcrb3VJQULAWN75l437b/k1PM7xdqFsN4ta+BlxR016Q4d9msw0faF5s55u3om6XAwHDA85Yrlbvhf8Cs8023dthjZgWN7x38PB0UJ6u2QicXq1WU3vpHa9CeTMbrJHI4sCN9cBV0mhirS24W+BUquadqNv+jo5IJJVaBNiwa+5U1u4gHEVRqj07p202c6gDvFQq5TYt7p3BPjWZ4WFOvY0Mj5Tnnpw05Tu8OuAqdFCdbrcRPL0aedQ+PLwuxBHPkhe4Hr8zciqt5FSQpzq4HQ+PaMCBB2A+ITXv8A6R4Vlxee7gQwtwlpgp35ACjpwKDA97ZHhWKy7P9zAQiFli8JnyDSm4PPzO9E0U+t7KHZ7b/XcAQPgYAPMJqSeHp6LeI8OzRsipuCePBALdjOzzMowpr5BKn0oR6fbQtmncLVSHypt0T1qOdI+N/3jv8r+y15RvSIFHONU+M+rWmukWrdZyZOzDDy5fOnz6V86Ud0jpyW7x8IbJ8BiGcDHv91dOXLp4+OSpUekFpW7/F1K4vD342YYQl7K0BWYxx/junHj/4uHTJ4+1j4oKYEMdfmeaed+Z1Yo8N1zKLPE+OvEPcKfa24+O+hVAGF8l/URIgZd9Z2i3XACBMfDEK58Dd6z96Nn7nymB9cUrVqFuW7Lv7N30OwMPX4qEQDg9Lxu8dpVw579IKoF1UCB4RsxRZHg4pLK7JSADBQ5euwoceJ/+MqAMVtC0YQd4uDoqPTzgkOZGuxAJ6OWCQ999i8r76eOJoSCvBK6ooA2GlgIjORUyPNws8nAMCAhkvD42OPT71/fPf/XlJxPnBoKCMmgwGIxGPfIOTqPh4fL8D2P48mAZBPTJAPb9dv36N7fHb0KBvNJS0mA5pVJpt2ceRmwkEPBCeSgFGAx6fbLkH5zou3v35zPJwQTvWBgsKipXAWi2TVtxt8cDCLQQkMegLLN84ub4mTN9F/zRKM875gefq6/UpUGtVmueniGrHQHDhzXG68Wgj+MkwZ9MXrhwa4zlwYsqVLiyUldVVQWgFoO9ZLcEJJ7PgUCZY1mJD/oDY2PdPuQtDOqLmsBrNJtDKXwqGGSI54tikJXgE/hH3YGA7ADPuRBYqtc3aRsba4bNIZIqAHbLHAecLMtzoCgKwg3Q5ajT6XQpgRW6qlIA1TXwDQ+HkBeLjZAY/VMCkHMhkBPhE3gegy7wphRBTWlpabmeAq92ZjhEVjuSiVGW49gMKAi8g4BOVzisDNIILKdqamsB7EccI8/FqMiyBJQE5DmiCOTCU1N2uwK4HINN5SrwymZm+oHzytkY5SVWChMQuCxo7+lRAnUGDGpry8re7J3pisV8XG6MipI0hUEer9aFQBY4RXAVgCUlTU3aMvh6e7sgVIScGA0KokhA4KIul+tRGvR4lMGSkhJKjcA1oV4rI4uJbIwOOQVRsCNQhPLAC2MQOE98IbBaRTWCB2CHV/bnxOhgkBd4ALtn/XbQYHYY9CwIVpVUV2tVNeCtORCKyGxujCYcDp7vgaO8c+/yHwnwMmA83qYMVmNwDQL7U7KYjdHxwaADduFJH+UPPXZ7DwbBi3cqgRoMamvBKzzQP8mJOTFqj0YdzvBfmaMcgF0gUIq3tXV2rlMAaQQ2AlhYWLihywJgcqLvNolRtNhw9ijbPB4CAtf5QBEsxWBZIQFZ0Z84h2M0GHY6omH73FGeHT0eT4OdDx48fqwMrl69OgNaAeSdyeQtiFEx6oi6pgZyjvJ4ZzyOQeQtDNY0IvCliJWRRCGaCN6AGGXh8sIDs9mjvPlfb3Xz2kQYhAE8TbJpQpo011JJsBVEFI8hqDk0KZ76gZ7aWqGttUgvYk8qZLdV+5FUSVJTDVQbsVJcQg7FaiFUctHoRS2KuGku1oOfCB5zdJ53N93iulnqwfkDfsy8z+wsgUsAo+Uyz/M64P49VTAcBsiOVKL0kLJMziXjudL2Uj5eKVAWMshH+MjlWuDQoUCY6hiBdPMIxHLMza2JhaK8lFc+PitWJEliYCwSiQht7Xpgyw7wzjy4hAIuxHP54k+2lO+yE6W8RGEQmJ8mTwiZ9MF6/9BhAq1WgIlpejsGLsTFVL64dRVLmV0dF8qozc17UV4Quge8rhpgfRW8f4sdKRmMx+O5wnp26xct5WqlUubJ4zMzGRr3VIfF6zU36ID1AH1WK8BZ+UjJoChmpJW3E1laylKOcXwE0/b3WSzexka322n7C3gAYOe+ZoDDN+l3gVrH09PRyywVxsfTtJQVUSKPcV29xMEzm5ua6mxa8OBOcIpA+mMwEDcqI+UjFVrKvKh0131G4dzENTldEGuCtCv0B2IgcbgCkkBLmUsRKODx4IEzg3PVNTTY9ECH1TH8ZBJcXGSg7BGYTlemeLTX06dOy9qrA6cFOa4zELY6HI6z1yfxPxOrII4KZVG6nUB7o70WOQx4CgdPA3IcFwiEHQyMwUulAEYxrkTLwi+jPTye2p5TbU8D+gn0KeBiDJwCgquuyvn+jj/CYJwxGE2xbBmI9sDJj2cQhgq2MtCngshiY/tIgRsdsOiEoQfafT7KpGcseG0mxaIFGIMn4DuzaMMw6VZLq99uZ+AYxwUXp9HfMsCVqPp4xmGodQ6gp9k3wlEF5xNytBvpXEySvzNwxu2pdfy03+PxjLDB7QTS5hGYiYrguvF42jAMajAUusAGB3h3ucyy5QX1OwOnE4bB4B4Cy8rq4UgBUzXNl2E8eCg4n1SPlBtlVjXNtMaDmyZnk9UjRRIs5OpkGrjdlm3v7Bqmbbt0EpSMQUNzmHbXdWJkjcLoGXS6IMFi2L9x6NDUfrGr6yhjti1gBp6BCkSFFO5/128/fjri5fb9mwAAAABJRU5ErkJggg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Data Manipulation </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC9FBMVEVHcEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD93nzNzZjN3esAAAAAAAAEBAX73Hv/2trX5OXS5PHU5u2+0+LS4vLM3uzK3+zV5fK/1uSiuMbP4e7V1NXI3Oi80+KZsb7L3+ulwNDA1+WnwM/svnm/1uRCS1LM3+12hI/P4e260uHG2+jN3+2tx9erxNLJ3Oq2z97C2OamwdHM3+zB1+bE2efDjWj83HrxqWCqwtGju8rC2uesxtTI3OrP4e6ux9e91OO61OHF2ui40d/T4/DB09zD2Oa2z938y3Xuo1y91OLJ3epWYGaUqLaLmqbR4e6NnqrU4/CpvsxfaW6vyNb3tGqxubX2smj1tmv2u270rmT2tWn93Xz3vUz1s0D83HvjjkekwNCmwtKmwdGeu8yhvs6gvc2nwtLjjzz3tmr4vW33umvnlTr4wW+jv8+cusvkkTv2s2inw9Plkzq20N6auMrehkCivs6wy9v4wG7nmUznl0HPwZ7Rwq/upFjhi0G5vLDOzsjfl1TikUPVtZDJ1drYrIDbnmfgqGevwcalvMXSuqLYpHWZt8jD2Oa4z97Q4e7C1+XO4O3M3uzA1uS0zNvL3uu70+G90+HI3Oq2zdyrxdXF2efT4/C1zdvS4u+sxtb6z3S50N/G2ueuyNe3z93H2+mzy9rqnDjB1+XrnTj713i80uC2zt3G2+i+1OLL3ev5x3Hsnzf6zHOvydi60uDpmjnyqDTJ3erwpTaxytm/1ePU5PH603bwrkzuy4LAy8T20Hv61Xetx9apxNTUzbrT2trcwZH72nnomDn5ynHqoD+3yc7jz5jtoDfvojbwqT/XxqTS3uTrrljw14rspk3zqzTxsVnU1c7J0tHYz626z9uwydjqoEiqxdT2ukr60XXGxK74w3C40d/Ky7/esnu4w733wVjd0qTzxXeow9Phu4Pzs2D1umW91OLjwoj4yGbA0dbnljrK2uLjo1XjsGrltm7my4zV/5crAAAAZXRSTlMAGxUPAwQGAQoMEBkWRgRGFBIe/gEMHEYnE2oYglSH5Ag3r2yy7S62/WsmvkP68F7X986R5ebdPEbQEPB+xKF+76R4+/ec8IXGyvv5Jbf09zJKVupa6Kcz4Ej72dMe6/SJ5/NZ7pkTd8cAAAeWSURBVHhezNRHaxx3GMdxvQGtD7tykRSEgzAxQlhgjGJ0MPJB+BDQIeSe8o7+U3vZ3nuv6r0X917Te88lzzOz69g5eHecOeS7D7u3D8PA/vr+D5086aznujY+POCg5xn2Gb6RC86Bp3zYuMuxF/ghenTWKdA15qNpHx3SnQKHaUzRnQJPmF6eEOIMODRCszTLEadAzxgLHksI4wzoucpiIQZzApwcR0/hzRwAz4ygV+edAj2jHAsfvmX138GrHMa0cv8Cz7zlUpwdRy8EXs7M1df/gQlcnPto0PM2L1AET5HlnJyDk2VXv9v9MTzbad+cMTdhfyFHRU4U8y1ZTstpOFnun3K73VOf9Y2CN8eesruBg3kxL4qJtCSlpTR2b2pry+1OfTJpAGgodsGzswiSCpSupOHuubewJ59eMyA2aw/0DEyIYl2MSsFKEA7QX25a3u33DCwKoK2m84pSV6Rgp5fe7Q3D8Bn5UOiELW8QNKWee81LbaWe1Go/GbjeIV23BZ6bRZCJBINUkKIi1IObKeyrWu3oug9SCCF2wIEJBdIjVASOoqgH+x2vsY8eS2yC09GoEo0GM5EMXCSi7q+kVkyvtkAjaK6ZDfBCFEEpo2ZUs69XsJ8btUbjPnpcgknYAc/NApjlQXrdg27QGMNjvYKewZksRFSvV/WqcPfnV+bnwcPW0avzdsDJy1kzVRAEr+D1xtbnMcv7k/YB2J7HnsCh6RBgoSyhBCgmxGKfW97RYaNx2LhLQ9lcyxy0XsChmawOoF6JWaH3FNo5OsS+Q4/N5XDQegOv6JgsNDve0vIr3u4GzdI0kdv1ALouAUfU5suWlgNPA4HS0YHZJnhs3lxGSZakHsB3dEj6x9tbDkDgFYsHxYNb18Gj+Upn0HoAxwiAQtnsYfnh3mIAWziAQDxmIaVSwUHDuoKe00QnhA+Xy+FyGG5vsQSPV1oomiVvsJiMVC8gdpFAmXCnv0rYQtKsmFznWJYLvbABDsyAx4RXLc6/hFwAPKs/OIitUBTsmTVqXcHzDPznKf+qH2511b+P4B204sl4PHmXhSdkcMysUesBvMwQhoT9VtpeqbRTKsQ7fcFB+UhGtQYNh60bOJmAKppf88Np2vEOdMfEqvHq7gYncmJOfbX3u4DvJhJ8ovkY0x5r2jfgFXarVtubIqSodsChFoA/WtiatvasAH1fbffoNw6SVFVQrQ2CL6ELeIWHhLVOi+DtPNo2q24fAydmhZgZjBDUDXRdAq+lfdvuy8LzQuEH1LDf63lR5CJtDESzN4Pn+RbPZzre2mYBuvVru3URYhBpxppw1u8bwb97sb+Xtu4wjuOmVU+jMmqwww4MXZ3ViXTUObGipdCVYruObYy2+33Tk3NOorFJGhcTjb/q7/5UBuskzbkYi8tdz7wwEJJLU/oHjCFCFGbcoGfOwXq7z/M1J4smR2ov+vaQk6sXz/HxHA5WbcXRj7Z0d9ZRv+bdXoguRKMPYzvaE6xoYV7KIlhY6iCBv95IN0oDxmPJWBJHTDvvBba8v4V+UgHiEAThGTzHQNr7YwETBmLJ+eQ8jmQmffDEh8zrERD/5++D/zxzONYdy99uNzAZRZFFxJ5DLHxZ1AfPkxeBxwtP1x1aq6T5b088wUvOggxod7rgsZPk2ZxOp+9OGrNvLv+G2YZWgEUD0Wjg5/mlTPQ8wkfT6/obntuaW3RSg/1oc3XgJmsIVoAKf689g+jMOmc2lBq5/Cs+PocsTh8mrCMQHGuKYd/NPYoxgj0y8MFqrzeUlpaVF+cDj0bgPZaoW3Z7v32zk7rZOfacvF+COa0FrzUYDKVlZYfLS4yFee6RCApKVslqfWpHfnBofJQ8OZhIrCUyjwz2/VS1icY7UF5ectB4qDjnujsIlKzoLnl/A6Pu0RtiOJnY3bkr29xhcPAOFRdxO8lK8vrIEwcJHJ6eIu9+gMBZFSXUBA46w2u8aiAPXAnjiosKd414gUCLSNV12+3d40Ph0enOsUkFxVWbasOhap39VBtP44pyNt0Ez8O8W91oefw5pKGNMJKDPbYe2/+dMpu0ZRzMPx56M4WWXKLoEv8i0D+lhBVldi0eVsJJITtbbX1mGXocOk+g5EJsQPfIhoKCgvBImd3BNTeA2zkex+W77eA9JO9uHYHDY+TFcVfzQQsv0Jl1Fn8qbDy9ZWhVeVKelNqLJrrQ5sgEgX1O3slnZTtjylqGUVtGvo7D84jwPu/qBuj3rsCTBQZCZfGNH+ksI7djlzweT1/vg97aL7vQsHc6FFJCEV8m3OE1n5j0lpFbxTtNnsf8g+YTbeS5R7z3ZDkkL/l8kk/CgTNvNukvIzeOO3LhUk11wVffEOj3eidDIXlG2s6Kn1oTPJ1l6NWKf29cdqNhr/c+e8e2ajU36C5j7z4gDxd8vUNGKijRKoo11VhF5penM55On33hdvu9bUcv4p04LrKkM8DYZpm2Lw691vb15daCN2YAxpjXXn8AF5qtgYP34lWAPT2DfLhtaj5mlobtl9PII+/Bi7hcVjMosjRN4/YvvntxJijWtmoULMKgwXu5Kt9qrNQkhkF7eQ5xhRwMisuu4BX3H+8SVQGuC2OeAAAAAElFTkSuQmCC" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Data Visualization </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>

                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAJdklEQVR4Ae2cBZDjxhZFvZ+ZmZmZmRkLPzOGmTn5zPyXwrDMzDhg2gwz2uthJg9PR9c716vErSdrUj3aqlVXnXq37JmuV0dqqaXJJhSMYAQjGMEIRjCC4X2o/uXPVB0rPqPal/0AIOOzwEye8uY6V35PdSz9iR18lofEYOBsg7Dp6E1/S285pz+99dxBZHyG7wJDLgNLFrIgb2zTrxSARHyG7wJDeQgcS955wdDWi2YoEDnVtP4PbgIDeUotmeq446ux8v3JVHjpMZyFoCfyj02R8oNDjfU7Lw4sCWNvVfJ5qwqrqytqdm3mzYPgDFxfVN6xurD244EpzdgUqX3OA4XVpasKa37vtI2BvAcKqrtWFdV8PjDmIM/tZ+0Sg42qKM9d4uqimq+c1RtVWZ481hTVvpcSfd2oAqMbVQPyeMemRGuOb/q6UUXmRtV/ed6GLNGwQPMbVQPyZInf9mUJY6MaK9+XOlJSUGXy7mZCHseqcP1bKdGXm4jqXPH9dcWVP8NG1mqiiCL9kqd6VrxSdSzbDJDzllhYnbJ6/54v2xhenHEUZZHm5KVnep+gOpddYx3UcQsFkPEZvjMj0cCwi7SWRgFFmpSH52BLVgPFaWjAz7heEwvrXw+JWFFnxAM+jqYo0oM8eblSlAyXdb4ShRV3ZojUNVjVXPjKvOTZl6sHuKzdJOIFREP91hXSg4OvIndHo9/gTYmMJe84P1ZxoHFDcek/3Oabab0lMdf+37hXgXPJv8XHd38u4TY/eouUH+jh6zHhwcEfkYdLChOQha0QG6wL39NZWbNzh9ggBTZdNwdmk38qVu3/S7mJm237b2qi8PvRsU3vVSAkjyUQONRyz+XY6/r14OC6MccSsTc4svWCdF4NUiChSP2SHp+puvrI6JYPjlPe6AYKdH9wwJ8FzAv04dFwuv5alaHumjnmmeabHrasuVwhbGTd+xQq8fOPVP6/nKBAjUSAsxHLlbIoD5X49vape1y9snVIbQbIPr0eo0A9tddAksiivv/kTj8xrK63xI1bKICMz+SdvpkGIUlieM37RUKLOdrG1JcsWQ0Up6EBP2O8EQ8Ch1b7IFBYripPuKzNC6y+WkkMrno/JDphXmDrsLqWy9Uj4/jdM0ggMisxL7C6YzjRMjgb8yoQv3OitTdhur+piqsUma5kPZ0haeD+DwBkVmbzAitODirQ0J2OWlJSeYhLlZ0cjBY3dCqwKAIFIEnAvMDy5AAEniI1ON7UN3XEYUlnvrOkjVNeYX1HIBACCcGybu6fjVMecqS5OwFhx+vaIY6YF1h6ZVYWMivpvycrC5mVmBdYmujPikNmBTWdI1EsV0gDx2raWE9R274oAgVssogPAiUgSsLck41G4INXsIIcgX13fZCV2VN/XWm1GXjaopW09iuJI9UpEdNPNpSlAwIhSiCv/ixp11mMd6XnFEDGZ+xPHCda+kSBh6pOigJNP9lMxi/PCmNGJQsVyP6606oB0nTgO7E/CpQ4VJkQMf1kQ1GUx8rcc8eHROTlSllusL8FCDxYkRAx/GRDgXoo8PYMzKJA+3L1Ape1VmCsuScDMyrzfkvSvvLWDMyozKafbCajl2VlMaMyQ5iAtr+O0dm4V4H4He2TFyRJ7CtrFdFvzL0/2RTV6zfmlKUlcpnqXvFhCbG/zvRsyk0cfgbbOaf+QpGm7tPCmk5X5t0lTWpvaYsjzhtzNDqQ15ONtDGfDFuiSISVWRbYuYwC5f70SzqP/iiQwphRCQRK5LOvdHqy4V5S2phDkgAkSeTdn31ZI+fVHwU6EW7sUjtPNJKMMFZm3b6SjTGjAj7ZsCFsg1hJjsCiS5WAN4Fyf1jWnvsLQZKEXeCOeAMrcBToAG5M0r4S33kXuPQjlMWMSrz2hz489RfimxUdEAhR2+P1GZhRifdtkeOeEt9pBU4UXJKBGZW5/X8fIRDGymy8vxDuLhTGjEq2RusgTQu+09zVpQa5BdKjaRCSBCBJwmx/FCgASQIagbyT62jyvi2aOHZxVhYzKvAg0Fh/Id6edUDglkit2hw+BTMq8bItAntsWyBmVGadQIm2f3+UsphRiYH+BIF8WYrKvClcQ4HMmUo83tWFLRHv6hqBRy4CEMbKDEkSxvsLURaFsQLsezYWV4ss4K4uIAjU4S7QeH8hbhB14Lv1hVVqQ5EefCfclLTY7uLMqMy5AilLEJj6x8cAMyox3J8gkKwtqIQoRx7FXZ1ZvClNHLowK4sZlUCYgPH+Qtxt68B3ECghXFPRECszbjwSuQIPWqLIIVZmV4Fm+6NAidXHKySkBnEGswLelAiuoazMOfONH7hAOQGByb99TMJsfxR4eB5m+6MLJK06WgaYUZlzJhQuB7hB8fqpRXdToijAjEoSf/04ZTGjMhvvLwRhEOUE5YEHjpRmK7N0TWVGJbZrKjMq0Qp0ZO/5ECVhvL/QgYqkKPC+wyUUpkPboITXayokSbT++RMEwliZDfQnCGRGJRAoIVwSCPFwTfUkUMJsfxQocc+hB9XdBzMwozLnTIgDwIaYUZm9XhLGd5+XlcWMSlr+mJXFjEoM9CcIxNsGVmbKA3ceiLMCR4FOHKg8mWnk/sMZmNmg9oxO7zyXsnRAkoTx/kLCX90ARIks4IyW0J6BkEiRrMwuAo33F4IwiTv2xSiLOVOJ5oBIDeKsldCegVl2nK7Mzb/7pITB/iiwLFHgKLAsI9CRlXsix60pnvjwA5IosP/dmJWIZ/T+eM5849vPKaAwHZK8pt9+Mm60P/6btn0VyV9YE/fxvZedFXsiOazcF+v9y/07LnrMYx7zMmuKl1o80+Ixj5zP6YwmJDPfqt2O843vPP8XmX+ks+2cjDRU0vibrDBm1KHoNR+92mx/mv8v1Z7S1rvcBC7dWXDv17761bfNT/YSixdYPN/iOfNHZwnnsxrCfHyb+6jmU9sue54l7C6LHIF2am79xGL1px97y5Kf2F3aUoXJ8BKREy3bHam6+u8rvmmb6EWPaO6Z8zzZ4jG6+ewNLl/gfOkd531ibOs5Vfg3d3aBWK7rf/3exexP/m/mrEmv3VXakl66szj99/X7b33py17yapy+monY3NPAfIM40o/j0TYxX3rrr6+1lnWay9WH/tzH0daBV1/x5/++y+2oahpjc0bn69h7+VMv/uIbX+VLfx7HEzGJ0Ji9ucecHfN5H0vwi5xIOKpny3wLHo/BJEQ+qsF88sSA4yyb7yEQgW+Uqhq96wAAAABJRU5ErkJggg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Probability & Statistics </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
              </nav>
  
              <br>
  
              <nav class="css-1ewp9ay">
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1lcx9y9">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC9FBMVEVHcEwCAgICAwMAAAAAAAAAAAAAAAAODg4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC6taQEBATl5dnuojuivtEAAAC5zdu6z96QqrmYs8XxrEHL4O+ww9LA5f+JoK/jjz3yqTjzrj3B1uLwoj/L5fbP5vXM3uyTrb2nxdrF5fO5zNrA1ePpmDqhvtLT6vuPpbS11u7f8P/G2OaRqrWguMjTnnLb7Pnf8P0iJyqwxtRKVlypvsxreILZ6vfR4/Dijz/jkD3Z7v/1qTnhjD6uzN+VrbvvozmiuMfT5PHdhj+5s6jFxcS2zdmUp7HZ6fearr7g8f+etcPI2+l/k5/P4O3C1uXK3Oqfu8zfhz/h8v/zqznLj1Xrnjnbhkf2tkvcs4bB0tuwxtTbonTzrTn1tkm1zt+2oXi+4/3ZrnetzePzsEbzsEGdt8fvs1HO1NeyytzP1eDCwLixydhUX2RwgIiRq7vg8f+PlKHtoTeol33nljrwqjyilouInq65p3THpWrf8P6ztJ+5tJfMuKnNtInd7vt+eYPkzKm3y9qzx9bQ4vG6z97K3evP4e7G2ejI2+m1ydjB1uS+0eDD1+XA0+K90N+wxNO91OLE2Oa6zdy50uGar76rv86kuMeftMPS5POtxtacusuTqbevwtGyytqivs6nvMunwdGqw9S0zd2No7G+2e6fp7Gcn6Dbkkrso0jomTzijT3ro07ytFfyvnXwrlTqnDrnmEDpnkfllT/g8f/vqEbnmUPmlDvmlD3sokHlkTvkkj7kjzzsp1P1vF3c7vzhij3sqlj0wnnwtWjtoTnsnzvzumTehj/vqk3xr03xtWLgiD7zuF3IycPyum7qnUDurlvvsWL1xX7olznvpz7yrkbztE/1wXL1v2yjnJP1vmTg1sbY6vmil4zQwKb1uVba6/rbmV7dgz/h5OXCkWjXspDUpn7jv4zf8P7fl1TP0svhsGnV5/XOjFWzk3fX6Pfiu3zM3+3Xh0mtuL6qlIBz3+wVAAAAiXRSTlMAGxcnBR8KAQIJKyMMExAuMQg1BE2zD+Pws7TKHtazsTs9nrITLD3VQrMSzvefm09vsvK6G1j+ZMxA4VLPa8fl12J7JMV/hYbV86D92mubqDLrqPCE+ujI8ISNbuWt0OXYlL/mea62zpkesuHY4vX5pMH+9DFRx+LL5MPm5MRSRs3aw8fId7qs/ubc0w0AAAp6SURBVHhe1NXHbhtXFAZg7eQtQRIBSYGUBEugCIgiDQnK0uqAAjl2drJkyQmcYtoIAm0Cx2lOgx5n+rD33ntR78Xd6WWTcziaiBCooRba5F9ffDjn3Dtn2v5n6erWaLq7Lo2TdR+EmOxqol92SWD39sZGinMm+f5L6nd9o1YrVQh+ZaXzcgpkSjE2HrSRbvfllKjhcqV4lLa6fYHxiwJXpzQP7yjOA4lgNGhz2QMUdVFwbDFU4JyJqeZgf9kGnD9wtP9i+mKe7MF2jM1lyuRI80sh7Xa/n4rsF4vvyy8EKhi2xlbpsvv6OSX6oNtI/nnxLe2orBEYU8zduzenGDsLTnGll39EbR5foHkBsmng0sfHP2i12k8Mp1/P4jpkDfLlma9oKrr/5g3tClCR5mC7/qv5+fHpzrYBFDuErjQHmPX1LICrq6uaxiv9calYLL6mqDy03DSTOt3XCF/p1WrNlh54FR8ehDCFAwZEBBOJO1dPOPmMZWlp6QWVT6dHmnuDKpUKZodij9ls6dN3LYZC25BUqsCg6EQw8ahTOG1U9n3wzlEknT6ePafAIbV6sq1dwDs+61N+XwBuE7MNIpfNIAjhcZL699RK5cT92fEFmNGV5ndiVKpvChywBuN3DHobkM1NrBFBbyKZTPJkp8ykUqnVQ0I7wDWNvk9pbD/1738a2t4ELxaLbWyWUjmGyzi9XvR4/vENnUp1Y7BNOhOWvqEGH+4D6ovVIDG2AeQx8zqdqdXWNJgtFsOpp1gvoCeC8VyFyxAA8vwK5gs9jEUyV2fM5okGX3NQB2siWBXA8gl4vdXa0MAru9vbuG2ZQkqskK2DQZpAj0TP7ZbewbKHqRLj5RvWULcAsiDGhAKjAJbLJEki5/b1t1obbMVG+gd6Ok5G/SALYKnEsjEW60OQJmzokeBBHkt57XNcvBp0+aiPLPB0Jk1D+m/WsgyTQxE9oUAErSRZ53y+wJiUOELQtMtP5RFUqtUq3cdrWY7J5eIlTFzwaPCs1joHXkAhecVWO2zW9MLMhBFAFYDOTJarVHNxjOjZXFarR/QCI9JDxE24gHtEPnhTbzJ963RmOK5SqWIq/3l2j+eEo6jZFs9QMdLZLvKG28tOFJEELRoM0jbwXHa7xy9wkLsd0iJqQuTDtxzLXqeTyGSCGKiu7lnRE7kItXxt9GJ/nt63HQ7HjtfrJQiCxggcen5/QPQi+R2H48lAa67jXQfmadKLpI0gAAMO20UPOfQAfIrnfuqR5gyj1xz1vAomyxBRsyIHnsjl8/nXr4STtw0S3vDndQzzkufLGNSs2C1E5BBMP8dTiN4alksMDzkQw+Ed2AIkCRZqAhcQuWe//ZpO74TDAomjPHd46CEXPjz8GbyznNDtX3t7W8+Odg/DItlslHIYnuiBtrv7yw4JcXs8HvTEu0Uvn/5nb2vr7z9/F0msI3xmlANPHKL3L9llE+M2EcVxDtVCjwgkkOgNSo5IUJU97VaIy2pBgKolJyQq7q3opWculezxeOyxx/FkN1lbSTbOxy5QvikgFVD4RuVbgEABF6xVtlRdd91Z9cKbcTZO4H+J5cMv//fm/T160l6WpgHaVV8VOcggSVP2gPf1/t5wsx3RNIsLIrSymOSj5y+OpXhxlgamsM68M4nt1FnkGtXbUaeLgJgMAJhrfvJ9fvGZi9P1ZgEyMWHe2XcvSd6FjyY4xdve3t4/W42iTtiyoGxlMfdYPri20NKCeqd4YDBQPFtfLoImcQVvuWIbLQB2aTqxeO38SX4AXEJocf6gg0kcIGFR4NXayy9fUJq4AxzwttbrNdszWq3uaj8Y5MD5kuvSMfD2I0+ZprlwbgzMUiQIq+rNWn19cyRxs7zR5toaEDXG/dXVfh8l1/787PXTu3/R+49MzXTJNFcWHs6BAcJcAtv19bW14Q1JK3A3ho2GAlY0inEfJN74+zu5Lt33wMyt8uiiECuJOmPl0NMqNUVsDEf7k+aNhr1GowG8dlPXPGIGot+/ebAuPT578x8tYYzNGHqYBsLiTlVXRInsbV25tbd368pWr9fL/bVrFbvKOEZZer1Yl+6eTUqWCssiZqKaaEHRtt6cIHM1FA7qbVZsj3FLmIjcnKxLl56bTkoSZwDChBAED3JwqAdI5RKYjQbQZLHSnbTnMWIJwbut3WJdOlEkBQqNZUAQ5oQTJJ+ERVxWzZntuuymrBVwTYWjFjZJV2+1dot16USRlIEyCBhTEO5SEuRIi1PH0PRKswZQoNbBnK4BzgV7lhaGoUH+Ga9LcG2+MJWU3GAKQMxhQqmVSqSJMafMq2q2DgKzOuAcxomFsdEJw5bP8fXxugQx+n4mKQBMU4QEJpxK4SzLmbJ0xwM5jDpVh3KgCRZ2OqFBOcFiB+Ku9pur/0nKYl4yJhLAfEpRIv8EqXdKWJimZcHZcr0TdXSDuerdjsJtX/1fUspLgXIIXXQM32fUTRPJDBBAQfATpHIUWlEUhVC7yyUQoTPPHz++88SD8mqfTcpiObfjuo5naJ7v+0QOuqxdCR7iJPYiybMNz6GuMl1+1vfvffKumZwcOjx37NSKwKWSiWUTGRyurQGRYXXBxEpJMkhItLEB5dqa4TDZQly6xzCMU8fm5g4VxDsOz0k9/QjGuHwSQ0qYOtqKBt8TBw0KiXADeJWKDbPjMMjKv4XVvW7aUBQH8ALGtR1/yVJtq5LVsYM9dMmAMWMjpqhKUMScPVKlJFXeoAKZBggfwaiVbIFFkPIafZWMGfIC/d9rA6JOdM/E9NP/nHuOOXPBnX+TCSAK23w8Ldk8/Nzv3bqnM7wKQCxgNwI5fMm5l26SJOnvKXaRgiPPtddr/1A1AaK43QS5DFTLX0/7vTOXLF8YDQZdkOF8Ht4+4VP5dActxaFgIwdRiGNxrfX68qikEHDb8q5rGaBSuvp+2+9bFtk8KiLOHPXruZeSeIsp8sHDVvrOcrk8uapSUBYF6u0yIiQFqwfXLlbOtclDd4i4WCwhPoJDPJqv0wkndhAv4+C6cpCBnPD+f1AQRIBliNKXs9nMczxk7JCI08V0HQIkHPWiSRRcxrHf0iR4ZUUVBaEIQuRkClYk49gbzWxnEkU0JJTF4x9YKMSLIseP49VxTZcqBDRFDt6rIMfnol7/MBqNfCvMybwGlPObDw8PH+tG7ski9xYIUTQzUTMa7nA4duxJBBMoqkO0yG7iP77ZMAyNegr1MrAoAhRlBeJBRdONlj0cou9JGGKHUCH5FbRXq3bLMHStAq9symIhYA5uRX4TUq+dePf3lo/HyWviXKSr9FNtG8/kcy8DiyIleVktVTOyfo57dqzx+A41tpo4vB8Y3oaTecIVvIIIMutb0o2Gj0txPHxxvWaSJhcYng4O3argit6rL4OQu76NI3s+t51h0E6SdquWxQNn8oi3/x6MkGa5uhkl7vkGl4Lh7XVbiMciVYTMRhmEN8l2eFVwjG73xeIoNa3xs6FrWmF4ex5TBIm+KVmRUDlHV+8Nj02ib5AwSVWz4TE49ihBwoRGOfbw2KPk1TIpReZZw2OTtG+YqqJCo8NjcOy+YQKlGDRmt/8AHiFBF8JyeKcAAAAASUVORK5CYII=" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-1unwsrq" style="font-size: 0.875rem"> Machine Learning </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q"  style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC9FBMVEVHcEwAAAAAAAAFBwcAAAAAAAAAAAAAAAAAAAALBwP/7sLK3uqmw9TnmEvd7vjzsmG/1uS40uC71OLu3KX6uV+50uHX5vL/u13D2uewy9vD2uj0rDPQ4vDdhT/a6/jI3evg8P7D2+r3tmrH3uzvojzN4OzjsnP5w2/Y7PjH2+nO4Oy30uLpmDvolT7gjUHY6/nf8P76yHPg8f/S5vPg8f/7zXP8z3X4wG6UssTtnzn5xW6xzd3h8v/v///yqTfg8v/3vGzM4O7N4O3dhEDI2+j60XXvpULR4u7723n0uVaauMrwpDb5xG7rnjjvpl3i8v620N/mlDvyq2Llkjv603b1rjNWUUqYtcXV5/X81Hb0rTPxpzTuoTfWgjz1rjS0zt7oljv60XP813f5wnD6ynTjkDzsmjzkjzzg8f/g8f/W5/Pg8f/h8f93kZ35xnHh8v/4u13b7frH3OrmlDvC2Ob2wGnwpFPnlUTYf0DYgT6Yt8nU5fP3v2mFnq75zHGNqrlabne40eDf8f/snjr6yXG30eDi8PyZWi382HP5yXLvolrpmDm50OL4xVj0qjT1tUDQ4fHpmDrV5/HY7Pj4wG74vm2nw9L3u2z3uWudu8y60uHkkDu+1eTlkjv3umz3t2qkwNDjjz2jv8+ivs++1eP4vW3gij3mlDrijT33tmqauMndgz/kkTymwdGgvc32s2mfvM3rnlX3uWz1uGbnl0GYt8jb7fugvM3E2efrnTjS4/D72HjG2uj6z3TN3+35yXLsnzf603bD2Obqmzj60XXP4O3L3uzH2+n1rjLwpjXB1+X5x3HI3Or6znTg8f/4x2i80+LxsFHG2uf71nf1vVv5xXD5y3PA1uT72nn71XfK3eu3z97uojftoDa70uD0rDO2zdzyqTTxrEX3w1rU5fK40d+2zt7Q4u/pmDmwydjO4u31tkXvozbnljq91OK+1OLup1SrxtaxytrC2ObtoTquyNf4xG+zzNvX6ff4wm+qxNTroEXphoV6AAAAlHRSTlMAFBEbCxgOAQcEAfTM/Uj+8/PzBxnN5QvhzMzZ4vPDzMWS/EmF4hTaMPPzkkQnOOH0M9MY/Uti9rtXvo2SDzft68zX3O/sEPP7/PLkombJPN+T6vd0oS6o06ty9qh48nqu+LkpVUwg4+jwy25fUnyhc5/h6vP+RtXGpNjz0WuLhjmt37vOt6tJxvqgySy/ptvSt+HAps1NVAAAB5JJREFUeNrt1Xlck3UcwPFHRcZmMHUUpWWRhAdHWoKhHXZoeVVmh1qZdmj3pd13dtsdyBoKAkNGwYQQHWPjZoMxxs0UFE8gKxZHgqt/+v6O5/F59jxtWvzZx98LfB7h7e/3bHxh/u//RirFyHLB9yxdPoKkas3a6urqydePlBc3uRo6XH34nuCReRyffHWYVL12DSPRHVddNv2cwNtum7eWkpMZUVc/53A45lx73bmA+/cv/OKoNKh46gkH7vGnFecAQut/xaDotA6usz33hi+LwCvaf/woNFl8WtrZnjsk7P6jzUVFRfuLJMCPZjkE3fCCL07x/OLjLb8CCEmAF1xwGX+DU2650YenWl1E4sD7BW/ub43GVe+z3lU3may+wNXNuKJmAtbUwFojBI09U2bh005pgnyAMc38jteQwnhgDwrODactN0A+wIcPNDcfaD5Aq6fg0TAeaKxC5JM3lZMu9f4Cl9EOlGHRfEywR7pDJN5qo3kDFYrZIAmqJOJvNXEsWEW61UmbxHgrFgxP8Tcceq2lQa+z526RV2Ym4sGli1iwuKq4uHjm77RJXmYPgJVllbAEZlvNQdwDBCyGCNjwe0ODq2GSt9kDIITA+pb6SlpZvQC0WIotxcWWmZ2dDZ0NsCZ5nT2xFKk/dOjgMY5sOXQQrslDvNiCA5BUPsvr7ImhxOJDUOkyVmyEqy2MAByHOdcJH7Mn2ExaXIp6IHZ9pbkSViNcRFKwnTSuq6ujqwM8H7NnobmuzlxnbgEBITHr68zmupI/Sv/YylDQbm+3t7fbx3WgThhP+5g9q+tw9YWFpYWlpUuDH0VXjfD3OA4kjevuPtJ95ITR+Ib32QMA7lghbmtIbB1ssBB+5bOg1Wq3IvAIyuhz9qgWYq9tqLCwAlbplpC36xZXhC1i2K6wkmYMo4y+Z8/bbaQWAGFVbAl5c0sUAwlBEwV9z57whVRsrMCByJAo2NRkajKZTDMGUHiHVd5nz7NtJW0lsDjxG5UANJkMJoMBgScHTvacxexRfVACAciJ6xR80GBoMjQ1GWachHyAtNmflpBqG3/GVaxgzvRiebkBHn/5jFOoX8SzR6LY2hJa38+k5VJgPwLFs0ecgllWW1sCKHygYm/kGXAQZ9vcj/pFNHsYqVTzammtf/XiNi3igbZBeDk3/4mq4mYPDUCpQj5gRTcV13Gg02lz2mxOAlq42UPqkAYjVeHTWltba1tBfKk3sxdW5mMcSNq8DWURzB7ofAku6pveFUwwiCRlb2YmiK/Q986FQpCbPbhuCXDRikwoDsQhVOtQa18mLo4DtU6tVjs+J2dnzk6LYPZAIvDlTZmoldEgulvd7iH30Et70J0dWymoJY3fiWrnzx4pUDEtcAduUxQSSX3kVhQLurQ9e9vHp6Wlp6XbudlD8wRj3HP37EBlbtzAPDKXgPH4VlIYAV0u+14Yf6f70yEKmig4IALnud358M3ZO7KzN0Yxj7JbzIbgzBgcPE3nvSFNr7dys4fmAc52x8e7ldlJ2UkgbIoGPx7dyUdg0ko5PBLVGw6uvd16K3/2wLA45QEuw4AyCZWatHJ5DFy6Ye1JRXc2MEzQ+w5+PRb+7AHv5EUCL2RuPCpBmUrSrYMbqD58Gc3cAZ6g0wZ2VNCEYDgFE5TZOl2qDsTLCahM1UGRTNDM7+bwOWcHB/aLQejZ+ATg4EP+HgAAzU/A5SM89TEmyG6/+ErOM8Gb2cDOHpoHqJiXkZABKyEhMQ+LiQTMwBcAWqGvZ5Hn1z08fGTYwM4emicYPi2DBMcO1Ony6BUBIxFospqaVs1x7B0cGB6AZWNnD24bgB49siCRlpGozs/IgE+wELhLF80EmUhXrAINZ2NHBU0EMrMXAIAhXhmpu6BwJshAMoWybxMPMAdACVGcDryNDIC00FM0G3/2wDpPDMrg1MmCEpPVu6AVAJbTQvtpTv7sARFAccFvJierk9WwWFKJwCgAB2kcuB1mj8vlIrMHlhSoUIQsU/+k/gmWmn7uA+9BBkAbzmkL/ZPmIo1PJwEolUz2vFpQ4K5LwAPQSQvdRnO54Jdxg2aingagdJenECpFnZKiVv74WjDDB7UA5mzLgaXRNGgQmJurz9XD8gLyeiWaIQVt367droUi8KsKdXZqOjUazcTvaf8MZhErKyVLGaZgaFNdLngRYEWk0TS4Thb8wQuI6gvMywu8mYEoiB8ZrIj0dH26HlYXBrsmYg3+3Cf3Cub9CPFBDc6licjN3Ze7b59+30MTulATfsAteWvMWD+ZNLg7K2u3CJzeqSFF0CMuCb/6vTPgffeOGTt21GjJTd61G+UJyqefLwBBGCWfemdHx4SCgoLXn0HcqNGj/QMkNqn4TEnBSx4M4N8nZ5yPNgQCIvxU87snFCx5By5Ggefv7xcAorjg2zH42r3wLfx/hzNiEAljyI7kn394zbvs9vzAk0u/NC/flffqx/BM4L+U88mpd3bNLwCBEP6IkAVQzh9z4g3SoseKPUg1/6G3xgh3JJP5URu+WNKjP9QBIg4nH817YPQL5H7UZrwmo89DTPK2x97D9r9NFiB+YDJ82v9A/vOO/gZRIno794zC0QAAAABJRU5ErkJggg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Applied Finance </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q" style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC91BMVEVHcEylwdGvydmbucvD2ebH2+nK3erF2efQ4e/K3uvG2unF2ebB1+XJ3OnP4e7N3+2+1eK80+HD2ee50eDD2OXN3+3B1uXL3urA1eTS4/C91eO+1uUAAAC60uEAAAATDg4AAACxytq0rKX82XkAAAC8qY8AAACrxdW1zdy50N+mw9MAAACrxtb6zXSxytmkwND5xXC50d8AAACcu8y0zdz803ba3NWrxta1zdy6pY8KCwq/1eO70eDF3OjG2ee2mn/G2ujE2ObL3+zK3enQ3+vQ4O24pI/CsJC+1eL9x3TJ3OnK3ev/w2jH2+iXtsaYtsc3Oj3wslzxqz/lunL4xHD1s2jC1uXC2OXE2Of3vW3yumvI3OrM3ezzuGvI3On2uWxzf4G+1OS2n4XUzrrrsVDfwYv6wW/0tWnurUfxtWzus2f4vm34vm2Vs8OLpLHvvnPvrmXtqmWmw9TR3N3ArI/qt17axpz0rzWHmKKVYj7F2ubnrWTrrmX83XyducvM1dXczazL2+D73HnuqUD60ml/dF/1sDPY2s/suF7FwLH2rzPMlF7d0q9INivAcTmKWzrU1cyyknn4wW/3umz4w2/3vGz4wm/62ab726f4wG73vm33v2772qb3uWv3tWn3tmr616X3t2r3wW74vW361KT4xX3jjjzolznlkzv2s2jfhz750JPhiz3cgz/5zYv616Hqmjj5yYL60pv4w3X5zH35yHT3vnX5xHD5yHf5y4b50If5zHPwpjf5znT60XX6z3X60nb71nf746r1rTP736nyqjT5yXL5ynL75av614jtnzf74qn72Hjvozb735z724/73pT61Hb5yHH5yHL713j4xXD5x3H6znT736b61I7++vL846T72nn6znv////50oT85az73aj61YD0tUj979PrnTj72YP3vlL6zHP62p30sj36z274xmH4yGr3wlv0tlH1ulr3xGr5xXD61pn73aT97Mz84rb61ZX726P86ML+9uj+/fr9893dx6UeAAAAknRSTlMAgL+Av4C/gICAv7+Av7+Av4CAgIC/v7+AgIC/E78NBQm6AmUe3Bu9gL+9GIBlf3ZmuiJ2dmUJdr/cKblzFia7bk2KMoR13Nx7Gq+dEKh+dy1b/uX0/ENXYc5+t5SnkTlApMjN7NxK9fQrmOb4b2O44tGuw9zt1vlLkH13x1qQxNHB5frvGIyLz9Dj05pGm3LIs30FAD8AAAfvSURBVHherNNHaxx3GAZwH6bszuxsLyqIgHRRIAkIEiUgS6hiRYoOsUAOSJgYyaBbDk5sMIQ4cTo5+PtM3d6rau/FvaWXQ57/fza6zqyzz+5lLz+e553ZC1bp7LzQzPQOsYMDTfRGxpwsy37U3zRwrNXhgXh7qLcZGq7XijhIybGRzqZczwHPJPGjGdebGAYH08PyPH+l//9fj4tMjbY6SJwQb8+84im/+fi1+vU4jpMmTZGUDA6+yik/+PZW/McPzeu5IyBn5xz1kn6/v/FX6P2vvn4Yj8cXfiKi2x3iJEmaGCYVQfqDQf+VBnd/vr1deEzI779rdbiRiIRMjgL0eDxs0OUan2lod9c2Ej2DeOuTUXi0pCDNznlonLzL5RocaBBEHh3Fk/HFO26TlARBmLhaJ4MtLS04ZSNgYbtQwO5kPPnZPXfIHQq5OQGZGiWe08m3IEO9tsECyfLycvVpEpm+ARAhJZlJeAgtOT7SaR9cRqLR6qMziHevR0I0AiMys3OmyLpAXhywC55uVeBFqyCPQC7eMUVOYJjwxDA8utvn89n5N3ahXCWzHysTr1o1cMokTgkvwkUEBpkaNUsG23y+wEyvDTBa2c/sZ7ZOiWcY2llSSSrTN0IRhBNR0jtZ301KWu7uwthsBmAmUzo0aMhu5e7NCEciQQz3XWWdLL48So73W4BRpAQOZqxMPM3QnjxUFGVhnqMRIIanhlmatkBgyBLE1NMtUjGWWc8amqYjTy+DnL5HO0piGLnmJCAfCFy0AKtmynmAsVi+pJuRnylKUbn+JUBOEph2uhvxBd62BFHRQCrgkKyuq4iuPjlCycWbkiQJeMvD7e1ezObbbDU0aA5LeZgAEXVNVeXHl4vF4sI8OCTs9V4jDbstGr5z7mmall0/bwhRVtPPFZDTX1DR6+1jed4aNM45HY+jXDKItbYmq2vyfVmuPSsWc3fnCdjeEAiORjXXop1sJv0CuxdFsrmjj7cHajF45TzhVHxpORn1oKXTqVRNyeV+IJMpGLDTcLOiH25uqvV64OhaeHI6BfF5LndJEEVvR489UNP+PNb/2f1NhUY+hKpz0NDwBQEF0ft6D+/3W4M43uHfx7vHqr6zhyeLmPWgoV5taWl1FaAoEtBvE1R/3f1rR1cf5B/syCo0yhFwiYBLKxRk6uBbViC5XHYzi3LriUTid5U0NDlaD3l5cnJJZETb4Pmz+CWBrG+gnekt1eAhKycnn4oMbRgMdluBb8JT6bsibyRo/thL1dvViLa68hIgY4J+G2D9jyGv3Zf30PHg4CDx806qBtCst/If2PFej8sV7H7jXSsQ11Ph0RdlYz3xb6f18ttGFUYB3Akss4ixhCLnWctQ2kAoQYI2G3ZIkSpopLJIpSjAolVpgVKxAIH4BxAgJuOxZyZ2HpNJYjt26kdiNxHbdh9rjGypriKxIFsbJ07SBef7rk266sxw7PVP596jK3ufUtkqiXZUUH7woA2+0tPjAKSAE9MWG/uc1k4brFuWAF+lhk5AvIsDYE8PdsUU+QNBNvLwENcgsGrl7ycnVXBijZ1/CIzsHxWtet2SLQUgfgMuOATxLo6bu3/VjskrlWiNrWQkAjESTqOeXFDW1r592TmILZ6eHtee7T7eOXqyVy7xFvlGBF4kYsqyrCwD5IbnnIEQm7Vq/tFei9btbJvZDCMPCZSeA/scNXxcOa3guC1eV3iYNw0woyB6G+w+h79MY7Ygb1vEOyuLeQ/yeBv4yHIuuZlTlGVJZbCfwZ6xLlvwUeel5VuYFmNgXXCIokiKJOnq2jqBl0XDrrdtQMHxGFuVCBIOz6cBKvAUCf10dX39tX4G+/r8TsASv1ueotgAiGxmhCepiEFgP4N+Bw33mOtMwevOg2yYy6iH8+r6Geh3BpabJau8fywebqGAdTnJnAoNDUME9grQ/6YdeAn1TlpW5bRqcTBFLinEzYfQNFWjhr29vU5B3N78Sb5aa9ShFeAhZpbAxXBWUjXNPWiVqye1Q0s+CqcLiphWx1UuIqZmaEZo1RVIYxzWmqjX4nWxBI0rpQnMANRCq6sEdhPotQfpXRSbRVwev96sqfO0qpZLUkN4cwK8fP6812sP3rQodHey2GIR61I0wzDThqGFOmC3AF9698Xg1et1aJgC3//WRT86KzTEAHjFKYjckDn8LrAub5E1mQM4F5qbizI46BDELNehSbytqrbXxbnb7ZDoxuqVwUEB+uzBgGdyBiCFttCxLiet4/Y4SxsbAJ03RGanJYkwTMHrUlZWMsxF0VCAFy96fd4ugE4ydQfD6jQtxjCzQsxus4iGH3ZAnzMQee/GXQ31kBDeRmYFHrIdjUaXzkCfCzAQmJyhXXlarJFm0IySmEi4B5mcnTZCWmfb7SSDKAjw83fOwI8BOCYv3TF4Wx7DzOLIABcSiQ+8n11wDXKufnFXeHxW4cUA+nz++wL86EuPu9z+mj3OAnkMEvXTfQY/HRh2Sd6cZm2Jrm8BiacA8mG/G/N9dS0YHB1xKQZ+uMUge7FYPJFqgxMT18aDr4+ODgyhpMurpHZL0ODFUgD59ibGg8IbGnEnYu/bP6Ic94vHU6nfxe2xN0DeyHDA7bE9U/fAwYsB/J5vTxyX+7ks2HmNt+IcgHx75NnUs8nkTBv8bWL8+eN6/m+GPbPfxOOfpH6FZ3NcxwlM3fv5lwmbNVxmKPiC4/4L5/EtgVWG57wAAAAASUVORK5CYII=" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Reporting </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q" style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC9FBMVEVHcEwAAADC1+UAAACvaDXH2+ityNi81uUAAABfWUqivdK11OP723rKzMvwqDwSCgcAAADvpDjxpDbbgkC70uC+1OGtxteyydb/yFT0rDXhjkikwNHTfT6+0uC1zdzbg0HtozuwytjxqDfrmz3zqzW70eCgvc6kwdG90+Dbw5m2ztyXt8jZg0L2vE7yqDTyqTPchkX6z3H5y2vyrkakwNCyy9qivc+hvc6hvc2+1eLtoDe81OHAwbvTg0T0rDPNeTzzqjT71nW60t/F2efuoje60d/D2OXC1+TB1+Sbucr83nu0zNv82nb6zWOZt8m4z9/4y2SJUSjNeTzDdDmpqaTElnCtv8nbhULgiETxpzbzqzf4x2HkwoX5y2n2vlrfqmTvozj3xWzsnjmxytmtx9aow9OnwtKrxdWsxtawydikwNCzy9q2zt2qxNSgvc2hvs60zNuvyNivyNemwdG3z921zduqxdSevMydusubucuiv8+jv9Cux9epxNOlwNHPq4DcgkC6saDjjzyuxdSov87gij7srGS9y9TZnVuswc+yxtS4yNC2rZ6zytnehT+auMneol6wqZ7nmk7mlTy1xc3Ij1SwwMmuxNGyw8z2tGnxsWbnp2KYtsfIqH6VssLolzn2t2vSl1jPk1bYi0nAtaHhm0vfkkzVpXG1ydamvs3LsYm1uK6yvr/YlFTsuHvMh0m5rpPB1uTP4O3C1+XzqjTwpjXB1uPQ4e6/1ePK3erI3OnS4+/snzfC1+TuojfL3uvD2ObN3+zF2ee70uC+1OL72nnG2uj60na+0+Hys0+60d/61Hb6zHLwq0TvqDzT5PG80uD73HqlxNb0rDT71nf3xmi80+L6z3T60XXMx7T713iqx9nXuYa50N+vytrmr1n1vV7ivHfO0cW4z97qnDnA1+XBztS50N61zdzlxYqgvtDF2uijwdPTxam+xcHmp1G2y9n2v1Tqo0LermnftHDI1NWzzNu8z93ouGO5ztvMu5njpmCnVc/mAAAAY3RSTlMAE1YNVlYTEwkDEgiHSMkYENQ3pksfKjkJd8HTt5F88RjdUCPdbJdf6/n97vrB76xS8N7B7PFNuXTP9Vb24vKXwPX29eTg27r7822mvM3Ht50ydnh00dJBzpcuyt7Yx+4y5YpczIUFAAAIIElEQVR4XrzTV0tcQRyHYW9EFt2FEzS6FxtEDWhARb1yAxKjKaRgbtPLNzy99+29N3s3vfeb/GfOCYRgdBMPeZnrh9/ATMd/yzfoKTcQnCsHTnno3SjeHCmXJz1bea1YjEbHJ5i54IA3YE8Ris6PM8zIkDdgNAoTo63pAMNcuOMFiDgktu6OsOztwZODLgfNj0+wE0GfZ2Clkp6aYVli6MSgywFYSd8nWG1y+IRgy92XhpOOP1jUwo/+7YdMBgcQ6HoVx4vHpwiNAMAXWrjc+xde/9hmuQxPr8flQMMeNK1pcOn+PGXRV0636/UtxGJbX+Dp3frlwnEMNhqEBn/bn8/LOn011NUe2BmLSTGpmWSYtw3gHA9zAAYwKMt5WZa40bO+9kCcOpYD0rmxC0IuiKMMYba3fdAwPm6ybLaahjCHqgcUDLrpZGSpry3QsgzLMOitdZZNZhDoer+BMi9FCqHu40ELQIim1ZdZIOvgxX+CQxikBErmZZ6XKSsx2nkc6HoqTZJkYiWn5TacfQDOKMRwh5+XjdLyrgwBqpO12f7jQDyPVklS4IS9D5qWreJ99cz0ohJ+4jd5o1QqrT3nwQNVitjX+44AgXP2caQgCJFIormuackqeCBOzYR3HpqU+akEHei8k2VfPNN9NIj3ceABmCh8ewdkBouZx4Hvom6au8uIrJk8T/EUZZKpS+ePAIGj0T7Xq9l7K4qS2wAOdc+SRF3Xa0hc3kUgHLGwvdR1OOjs4yAAkVco2Hbq1XtFyVYx+JRTjdWYbkoHiFwTKSdje+lPIK2qHEkKEeQVAEylUvv7q83Xyk6yisDPiTfPXqyKomisgWhTbl/PdR8K0jSAJPej1Lr7abMMwwDOdJmrHGyJ+0g2HS5Gp9kOdrC5JW4x8cyQaPz2vKW0lJby9uNtaQtVmKxlrGwFuukGcwPZlx+VhACJGYymwcaDZgc9URKTtrZr6yQ7IGGeeN3P87Sk7n1l0usP+OW67/vtA9MA+bzol1jKpdOrjx5mMj8DvL4syXKhaXpmZj716730j/M8f7QqzvwUOzC/byxF46IfuHRra2tfVybz8Pf7keD1NrfbUjDOoeVCERYH88rgT3RffuBkKpWkeku5nN0O0JF/MJDJ/PUgEAlGXG654MnHcZ8y2OtQBcV9waEfQOKQfN5qtY6NZzK/DUciwZ6Qq1DQL8xicAGuKINsXqyP9pdkXDpnT/OCVtOK9frN8Oj5Rz2BYPeNkGwpGKIoSZnvNamDtD+cF/dgF+b9rA6rdcVkMmkHJ0ZHB24FAsGIjLlLK/GZecx9p1erDIITBZOcw8B2jJt3cE6rNRpPDIyOTtzGKn1yoVC6N3sHJWdUQXDwUqLekp0OnG9FPSs84oyS0Ts2Hg7fHA50L5sLpRKujfQaVcBY5ffB+oED6HBgf+RBbG+XJF3kZjg8fisQXM0ZSsX/BLG+GO6bTCwsLSVoXH5fB2km1k+SvF6dznD7dHh8pGc5MTUXn50lUNqi0hDfS/n3wUFHPm9aWSkP3C7pvF6DwdA8PB7+YcS1PHUXIMhenTK4iOAcC1gf/T5of3m+PrFAryQZyHM2Xw5fHrkkG6bwh3dmZrbPoAKK/WFeeGv7o3m17e2YWId5yTOPhbtGLrkLTXfj6KgGvoHvmZ+XPmfqZ4UnPhfM65UY12x2mpvGPu8K3HAVSlFwAJ3KIO+XQ/g9HADRj4ESBhb9CPwMYLDfWZqMU1RB8f6l4YHLi37C0wFEP6cTHgdXF3FmZLavSRmsvH8CxHVJNBKI/bH7YmIfAy9bKmCxz6cCivev8vulfvDoHFSP+pmZpwc44jImp+aK8WIx3qdXBqvePxoYYfPi+0Pg4b7E6QnElQ2Lk3cpamD1+8c9XLed9kf9EHOTrwmijYOFEgcHbcpg9fsn7iH2x/rRuD4UJLCrpw2fTYy84mCnMvj4+0ee2B9ANi88m42B3VpfKUrg3KBHEdyh8P7BQwASxz1EgMuJxegcogbuVH7/pHI9p8/n07OCLZ1nqsEOgAppVH7/Kj84MTCBrGE/wEn8s6EK1jeqvH8Gp7OZa+DgtXSg4Y3u/tzi9GSWwDZlEGvcr/z+NVM9EhnX4mkpg9HsXDabHbQAVI7m5aOK7x9+IOAQ1q+j48wp0TBLuSADVMszL/QrvH++tf0B9HDQmEvGopQL8rMAVLPr3cffP5/wwFHBNt4QYDaazUYvuAhUj2bH/n+9f9DW+nEQVzbaE4vTlHVApH7f0er3D56NCno8xLUx0GxMJ1McdK8HYpWNVe8fPwcCER7AiUC31p5IxWICXD+791bePz5uZyfmBddhsVjOnDp//0H/ahkMEbh+DuyvvH82Aj3wSOTglSuv9SYWUiBjw08IYpV68f7pWT+A4BCZgRev/Jnk4KUnBDV1Oxt9oiDnaIECvEj57kQKpCKovkrBeXAP7smyfO7U+S9Z9ky8mkoNH6yve/JoDhwBSBz3EAZ+wbLn7OF9b328q+5/ZcsrKFipB87lOne2DPoP120gO4/Bg8jqcfCbr1g2BiKv7xXnYJ4b4N8se/yf1G0smuNHABJHATjA8qn/pQ2CtEoZIoFuN4GnKQ0NdTVk1zHu0cjXmPet/7laQFol9XOHAH5NuebfvLUmUFN//G0BXqX4T27btklTE4lVhkIEDl0dGjrk3wxwU40lNbvfCR30HxoaGmrwvwlu0/ant6JkTTlw8CP/ye8P+Z+Htx3e1prF+g8b/H70I455AGvMi+9/8B7qrXG1R7Odpq32/gH0ZoEdythNTgAAAABJRU5ErkJggg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Case Studies </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
  
                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q" style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAMAAAC5zwKfAAAC61BMVEVHcEzm0bBtkaz3uF3l7/X3uV+30uUAAADb6fP4xo/Y6fPD2evS5PD4yW75z3D2sUu20eXzrT33vGv1sWb4w236s0/khjzwpFyywMq0z+TwqDkAAAANBwXPezvzqzRpka3tfkFojqr4wm7U5fD5xG/m8PbzrGHH3Ovo8vf1sUb1tGfumlW20eXpe0Hn8fb61nHzqzQAAABnjqjQbDbRgDn0tWbo8vdnjajo8/fG3OrP4+7i7/Xo8vb60HW40ubtdznzsGT4vWz71XT3tmff6u/3tlGaqLO40+b0rDR4e3630ubxqDXyqTXnlDzwkVfxpzXM2eOEmq2asL73vWvt6tXzqTXsejv3u0aDhom3v8LWmV+5w8W6tqbwpTdnjqiFl6aZr8A0OUPDaz7qfkX1s2d+obq4rZH4wm/j7vTd6vLS5O/M3+3V5e/5ynL5xXD4vW3n8fb4wG76znTX5/DZ6PFnjajK3uy/1+j6zHP3umv737X72nnh7fSxwMr61Hb968/5yH76zYedwNrD0tz5zX772qH86MPe6/P2tGijt8b726zk7/WftcX72HjO4e33t2r6z3Tg7PT603bQ4u7I3eutyNu2w8y50+b847f1r2bb6fKFpLu7ztr3vGz5yHHV5vD4vm360XXm8Pb60HX5x3H61p28x8761nezzeDl8PXC2eqQssv72prF2+r+9uj85Lr847L////713j979jBy9D4yGz4x1/wnlrppEKdu9H74aqBnK9khp/50GmqvMfysEn3tmr73Kb2tD+musj61orztlutxNTupVn5zY+81ejEzdL3uWuascL+/PfXp2r60ph5nLb1vVf0uE7xrlvpmjn44qHehj/614SVrsH989/0pmT44ZD4xod0mLKJrMX614/N2eDq7ebyl1PU3uLv6sr0o17Pr3uXssatt7nM3umsvsrZpG1dfJN7obvr0L/kkDzNsYXHsYz++vDap23xsIPo6+zaqF/Tqmuytaw018JaAAAAZ3RSTlMADP43WCT7JdUFIxpM7vJQ9P679+kXLMRiePgFHrbOJPlqz+OI+43cml9yr+a49/euEPlKtuSMn2bxQ8nvwc+Lo/v10utEmJy/TYymmHT+48gcsPLXgNG1V7rlwHCOpYrxNXfi79fEH4vQnAAABzpJREFUeF7tl2VwHEcahseSJVm2E6PkRIkdO4k5TmI7jkN3xuCFGQ9hlpmZecXMzMzMMjMzBRmOf+brnl2tFHkj79o/clV6qnbUq1I99XXP+3W3iP9bpphi5so06crpt0w369nwtLS9e6UrZ94a3QtzQSeVVtbUJM++ed20O+fi6iora5KTmcybqvG1ZZuX3fkA0uHqJMzYJMZ9wevu2bxBLBYfCceTRdWBj5Unmn5zOrG4A09WIomNZeTliUT0GcHpfg+2kEvlaUfEl9DaSWCyjDxhZqZVpZoWhG/ZBtBVlUNQOi6mwNoxk2CyQhHdmkBz8h4Lwod1KCl4ssxYrMu0WlXOCh5vVeDz3SA+Uu4LCkyWqk7l5CGeDDjHfxOHlHt0EpgsgwWvgo6rw8wIuC1CxFVIV4OCgqvLRNUVgitw4TToshSxOMVXHV47Gg1mG4SQ6rJdYrHUo8vz6GCyQQgjcZdJpdKsDtwVkGP8ZpEuGOEdoPMFBcWY0sHaBSOc/dvRHQrrWKjLErAuGOETozocY9D5qQ4LA60OT5ZO6YISPkajMx736JKo6rwxnlw4Z8k7E44fHoL+OKoO506EetbJ80fhGGHoc4M9D02YsOfv7sM6arJYN2mFYQ8PtiqVE4TTeV4j2j8n0QEVHmHUi/Na3Uq/QmC5EFdXwZuEV3FTvRVzVOMGlNv9CIGdeP+chKSvXoemeumVYwWNjWWNZWWNt/sXFkJQ/EKnoadwl8v1OjHnlb6eMi9+hcBTfnXOMxerhDxrtsvlsv99SV9LIqazU6NJDE6YlWUwpBntLrv9f/+Z15pYgH1NDQcLCn5R+KA/n1FsuJR1xm63f/Hf9qPxRRTVDUhYdDuxerU/4XJ/wl0hHd3iM/YvPmsfkcVTCIrqQQiD24ieY0vmTBRWFBY6/Qu7s7q7XL2H+lpkAplAAI/hejMI/yGTyW4jlMqewSVPjxFWOBE0Fc16fZ2K2fFNVlZWb++hEbYHU3pdJxKy2UgItA4+F+oV0mgqVUKCFcgc76EySYuV1lR2fdN1QK32Cavr0jvTsZC9kFCifLtb5z0cTe3URha46HR6pkg41jcUUsWCbDLwFp7d1aWmhLnsXKAkPX0AhLUwXEg0ut2Nboh567zfhSFhaamRIRKJYJsBwShnjhgM5Twh3nKZsUa1OjU19dBIMcI0oEfCOhDCNxBCv5ShvtEcjXkxirgjFTDG5uWxWIwxHdZtqLpUnkzt4EkMI+hKSw+NcACTIr3EI7TA14VEogYxfFKTmFhwNOajD5FQrc7+kpEU6xNmiw0Xv01BOy4+X4yppcA/j5MkhwQZEprrGs7qSJIEISL9YMNlNIg/FvOZmiI7+ctxUf62Kg3Z8IFwpbS0ubkZhMCAQnG4BD46B4fEwiKIt7bhoLapqKgAxuxj7T+ovwJ6s5NHfbTyboPB4Dle4J50pbl53759Xx/n80m+gxLq+BT3EvGAWXs6vrY6nqKlr/3HXiC7xusTJid/Dhe6PBHo0Onn/Bh0SJifz89HwsMOHR+N+W+siSQEFPXVZ72xl5F97UNDQ9mVlE4kkTBYLNfnXhtsuT4hFwl1XC4M8rnb7kfXWRlQLZPVms7WXzbJKFpOQOeXS5HOypTAJUTotSGdV/j+8RzAoSjh5iDkW6MIBBuwVLNrTZcPQtbPVrOBlhOw06XshfZIAh1lS1BRNq/wvecf2S2Xy/fv2SPHzA8jKHIh7hZzbq0pV2+ut2gtSMjBwnAnQ5IkpK6XcM779m8QfvB8RMQju/cDcXHouSOU8FIMWMzFtaZiH+SJAwcO7Apngg69VLjAFVZ8cvL8+ZOfeA6mdyMikDDOwya8X/mEHIuZU6vTWzgleo6e4xWmhAszYabUa7h6WulWAuevwq36ibvupoT9mAVriLFwgDosbOI4FBytnsMh+d9jIY4Inuq/TRrcoI3u02/OJoD1IFy82wYsWBFJjIME9HoSCUmHgtTuIUmv0HdXvQCnBXSnBo617dTiQ5GL22y2jLWQlPHwQQCfplEhxD2HEvK8fMeGpFrq607DcaT5jUewfnFbxiJsH0/YfBRxPoevb4KQ5l9X+C+IkqxTIBgQCOBM8r7Q6LV4NJHQ+fko6qSCEsJYPl6oOgcByAXp8DAbmul+YlJCd3BzUNzNihztHvgZN174cRsJb664pM5sgh2aDXOenKc3odTn6ORIKP+ZcN/L+SSimOToIFX3EjfEnE37EY4L8LCdGivc+cy6DC4sM99RotOCdz5xY0Su/iuEfj90UtxY4YOPvhbx55flsBBc7gXtYfDCGt6ocs2Cfozt1B/+CEKsm4Uy95dPccfKkXcbROXGlSsW2ICMUxF/egaEy1fNpFK8cWlbPwJpVxABMW1rBhbefdesZ0HnZePSa/B7W7/NtogIlKhFGRmn1hM/I/rtpdc+PXeubW0kEThhi14iJrJxy7p1W6KJXyVTTDHFT+se69NUSdq9AAAAAElFTkSuQmCC" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Management </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>

                &nbsp;&nbsp;&nbsp;&nbsp;
  
                <div class="card" style="width: 174px">
                  <div class="card-body text-center">
                    <a href="/courses/tech:r" class="css-lafe5q" style="text-decoration:none">
                      <div class="css-1ctspcv">
                        <img width="80" alt="R Logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAGwUlEQVR4AezQMQEAAAQAMO08wkgnnxBOO1ZgkV0HSBAoUKBABAoUKBCBAgUKRKBAgQIRKFCgQAQKFCgQgQIFvg2cZc8cgJzJ1jB8bdu2Ubi2b+Gaa+9Yv73eLfy2bY/C8cS2nbEVzXzb76R6Nhmlk54pblU91Qi/J+/p/s6JyvMWdWDwGypf/3+NoeHHZK6eP4uNwY+v5pe61mn7ULM19GuNf/ABfC4+/4bC/fZV+8zf73ptrL3iY0nr+t+l3OsfS5rX/juuq/76SEvRWwoWiC+t9w++ZO0aV1siY2POvji5B5NkjoySKTTcY+ueqEWBK1UYfiilt+9vhtDIVbmnNyJ395I+PEbq4DDJPX1jCk+fCd8H32ulxJGy6B1Jx7r7Uv5NdUnXxp6Ud+M0RbcThbbQtHfTSMq90ZA0r3seMjkLhBBjcHSrOTo2CGHW7kmCNENoeA4cQygeg2CkhU8hdVr395XefqnKP0gQBnmdzm7qYMCWST0xckkbGiHI7HT1PM/3h4vLKn457d/UCVkEfJtpxr0xC5yDUMhM2DdsRSKXFSjQBd+LBEAOJOkDgzlJS5wYQRoLKQSp63T3DkJemz1KrbYIs10aSIXkVntXI4Z6IZ+J1EEKBKXs63MCmZCccm28Srpt71lUIIYQKw9i8gGyLV0Tk5CRTyFILuRBSpMlTM1W7kA4JOabxKSi6q/Tro2T5NhACes6Asy1D9usY5bMcxDOyL+EJC4QqPENVCBNWt9AQaQTOxRt9YQ/w/VGITYHXa3OHpKYAiQxB/Omw9NPEmt4M2d5HcWfTVnXhwjyzGvzZtq0Fmkk5iZTkSUQRaN4XN/U3r6Cmf0BAkN7uBQjMgd3tLn7SGT0F4zEEiKxJTRYr/Z9hZNA05o9BHmGNQUzY1kHgeHJzvJPzAlE+pAglaePF5rAwGwKc7U5uNYifQzUqPPyopkZ/vgxcslDwQltTTdSlNQxMhgyt5kkMvcXeQ45Nsyl8DXU2vpahbtXgDsc7n58wfugd8t17RMY/dP1WjfxRcQM/0aDT36+re2Ny30mejsyraOEuvoVNDUsmeeXfA77PLxPXF0thLvXIC3ou3BRRrvAF/RuaDOWK6ZW46wUMcOvVuNawF21E2CfC5CIbX+u1KOfIxSurOINaSGzOoTm+zUd9uiPUDj6rfm0O7qwXZaO7GMkEO3ImeWKuat2vSQwBem20sGbtHDXeJ3a9c3lPjMmKz+FwuOyyhUhoaiaScjKfzgrkJVVAJCVeYwejZPAWp2PbshsK0I+AmOdFStCQs5IhEBEv8UejSCFkJEmgoaWPc7cn/8ckPkcCMw5hC+26Wsg8GqHhS8QiG3OIRxvL3+eVNUUayvnS1pee3kwhiGMiTQaUplvILtZZYSw2/ng/FL76M3E5tCyN5FLLfrfX5XZZi63mYgv1xUOutJhkeW6icSExf+jzkqKtZTxBu8TbyoTEG4ieHOJJVwJgZgNgMVmBTgnydjHdj4tji5Cmrm0MRdajV4k6EKLgRc3lC4612LcyqWNibeUdzNQrKl0caSZx0s/BwJj4uKKuT6QHcYQgOY0E0jLPrf048yPwGwj+7g0tSga6TnbrC8YCGR+iIHTrfovcVpAEBXvRfFT4pKCmWlmhrCkNLuRZlOImQGmSGKOoAdj99n05TOVO9ukc5xvs9ApiTaL01JdzmOAywCbPq5TORQPCTFBcUHgtUjfoosJuHviGgYxeZAe2o7uSSy25jOxPyHQ/OqUVD90ptlIJ0Qa7og1BPHM9m4hiwkpSelkUsQkqqGIM6y8pLDkMhYTllzOYobgTUzw0egys4WcYCrVZI+ONOh8DxeytHRcqPkbK/GYQJUTCIS8kxKtqNDlrKnaJx5OCUtGSFxKU3VP5iTeWEx4LuTRzfLFl7Myk4gVDkzSIQfpYmU16n3YEppgqS2axhKVoZfku6AKIUjWSakBouioQElHGhUzAPt4DJIhm9l/ju+CaqLhiZ+mGovlJCghkKgronhtWhi2AOcJ4hqKBhOCki05F1QzwQoHJulCY0AvMPjHIBJCMYdl5p49SCraFb6FALRSaEOQRgzLQ/XyCMTNymQSd7hBMX5EoDBC3P562bdWckkf7U2qvqg2dvfxHlYaiNc+MZaofVIfr3/y2dH6x7/G608ldPmQhWGKhQC2TUHhq/WnEq6PB2s7Hz9c1/m/w3Xqb+J7rPafSljmx/CG1Lig7Bt5/Kn0KpBYSCBebocOSAAAABgG9W/9AG8wBCyguCdQoECBCBQoUCACBQoUiECBAgUiUKBAgQgUKFAgAgU2AhmncDIhgDZaQQAAAABJRU5ErkJggg==" width="110">
                      </div>
                      <hr>
                      <table style="width: 100%">
                        <tr>
                          <td align="left">
                            <h5 class="css-okns0k" style="font-size: 0.875rem"> Other </h5>
                          </td>
                          <td align="right">
                            <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-icon--flex" style="float: right">
                              <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                                <path d="M5.69 17.92a1.7 1.7 0 0 1-1.197-2.896l6.09-6.115-6.09-6.115A1.7 1.7 0 0 1 6.888.4l7.3 7.3a1.7 1.7 0 0 1 0 2.394l-7.3 7.299c-.312.33-.743.52-1.197.528z" fill-rule="nonzero">
                                </path>
                              </svg>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </a>
                  </div>
                </div>
              </nav>
          </section>

          <section class="css-u8h2gg" style="padding-top: 64px; padding-right: 0px; padding-bottom: 64px; padding-left: 0px;">
            <h2 class="css-1k1n1og">Explore our Course Library</h2>
            <div class="css-14c6pk3">
              <nav class="css-1ewp9ay">
                <a href="/courses/intro-to-python-for-data-science" class="css-179cao1" style="text-decoration:none">
                  <span class="css-11khr5q"><img alt="Python Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjMuMjQ5JSIgeTI9Ijk2Ljc3NCUiPjxzdG9wIG9mZnNldD0iMS41MzklIiBzdG9wLWNvbG9yPSIjODJEQTlFIi8+PHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjNDhBRTc2Ii8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48bWFzayBpZD0iYyIgZmlsbD0iI2ZmZiI+PHVzZSB4bGluazpocmVmPSIjYSIvPjwvbWFzaz48dXNlIGZpbGw9IiNEOEQ4RDgiIHhsaW5rOmhyZWY9IiNhIi8+PGcgZmlsbD0idXJsKCNiKSIgbWFzaz0idXJsKCNjKSI+PHBhdGggZD0iTTAgMGgzMDV2MzA1SDB6Ii8+PC9nPjxnIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PHBhdGggZD0iTTE1MC4wODIgNzAuMDAxYy02LjY4NC4wMzEtMTMuMDY3LjU5Ni0xOC42ODMgMS41OC0xNi41NDUgMi44OTUtMTkuNTQ5IDguOTU0LTE5LjU0OSAyMC4xMjd2MTQuNzU4aDM5LjA5OHY0LjkxOUg5Ny4xNzdjLTExLjM2MyAwLTIxLjMxMiA2Ljc2NC0yNC40MjUgMTkuNjMxLTMuNTkgMTQuNzQ5LTMuNzQ5IDIzLjk1MyAwIDM5LjM1M0M3NS41MzIgMTgxLjgzMiA4Mi4xNyAxOTAgOTMuNTMyIDE5MGgxMy40NDJ2LTE3LjY5YzAtMTIuNzgxIDExLjE2Ni0yNC4wNTUgMjQuNDI1LTI0LjA1NWgzOS4wNTJjMTAuODcgMCAxOS41NDktOC44NjQgMTkuNTQ5LTE5LjY3NlY5MS43MWMwLTEwLjQ5NC04LjkzOS0xOC4zNzctMTkuNTQ5LTIwLjEyOC02LjcxNi0xLjEwNy0xMy42ODUtMS42MS0yMC4zNjktMS41OHptLTIxLjE0NCAxMS44N2M0LjAzOSAwIDcuMzM3IDMuMzE5IDcuMzM3IDcuNCAwIDQuMDY4LTMuMjk4IDcuMzU3LTcuMzM3IDcuMzU3LTQuMDUzIDAtNy4zMzYtMy4yOS03LjMzNi03LjM1NiAwLTQuMDgyIDMuMjgzLTcuNDAyIDcuMzM2LTcuNDAyeiIvPjxwYXRoIGQ9Ik0xOTIuODU3IDExMHYxNy4wNjNjMCAxMy4yMy0xMS4zODggMjQuMzY0LTI0LjM3NSAyNC4zNjRoLTM4Ljk3M2MtMTAuNjc1IDAtMTkuNTA5IDguOTk4LTE5LjUwOSAxOS41Mjd2MzYuNTljMCAxMC40MTMgOS4xOTUgMTYuNTM5IDE5LjUxIDE5LjUyNiAxMi4zNSAzLjU3NyAyNC4xOTQgNC4yMjMgMzguOTcyIDAgOS44MjMtMi44IDE5LjUxLTguNDM4IDE5LjUxLTE5LjUyNnYtMTQuNjQ1aC0zOC45NzR2LTQuODgyaDU4LjQ4M2MxMS4zNCAwIDE1LjU2NS03Ljc5IDE5LjUwOC0xOS40ODIgNC4wNzQtMTIuMDM3IDMuOS0yMy42MTIgMC0zOS4wNTNDMjI0LjIwNyAxMTguMzY0IDIxOC44NTQgMTEwIDIwNy41IDExMGgtMTQuNjQzem0tMjEuOTIgOTIuNjYyYzQuMDQ1IDAgNy4zMjIgMy4yNjQgNy4zMjIgNy4zIDAgNC4wNS0zLjI3NyA3LjM0NS03LjMyMSA3LjM0NS00LjAzIDAtNy4zMjItMy4yOTQtNy4zMjItNy4zNDUgMC00LjAzNiAzLjI5MS03LjMgNy4zMjItNy4zeiIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                  <div class="css-1hi4crg">
                    <h4 class="css-1a7hu85">Introduction to Python</h4>
                    <p class="css-z6sjuf">Master the basics of data analysis in Python. Expand your skillset by learning scientific computing with numpy.</p>
                    <div class="css-4acvv2">
                      <span class="css-4acvv2">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        4 hours
                      </span>
                    </div>
                  </div>
                  <footer class="css-3guzbk">
                    <div class="css-3xxt4t gatsby-image-wrapper" style="position: relative; overflow: hidden; display: inline-block; width: 50px; height: 50px;">
                      <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEAMQAAABsxk7os81F7hP/8QAHBAAAgICAwAAAAAAAAAAAAAAAQISIQMiMTJC/9oACAEBAAEFAgNpyZ6bhhRYyPtqXH1//8QAFhEAAwAAAAAAAAAAAAAAAAAAABAR/9oACAEDAQE/ASP/xAAWEQADAAAAAAAAAAAAAAAAAAAAEBH/2gAIAQIBAT8BK//EABgQAAMBAQAAAAAAAAAAAAAAAAABERAx/9oACAEBAAY/AkdxxlKMef/EABwQAQEBAAIDAQAAAAAAAAAAAAERACExQXGBof/aAAgBAQABPyG3R8yp9LAuRKfuVEmHFl84fQyRM1RuWf/aAAwDAQACAAMAAAAQGCAD/8QAGREAAwADAAAAAAAAAAAAAAAAAAERECEx/9oACAEDAQE/EFJsQfMf/8QAGREBAAIDAAAAAAAAAAAAAAAAAQAhEBFB/9oACAECAQE/EEd1KQ7j/8QAHBABAQADAQADAAAAAAAAAAAAAREAITFBcYGR/9oACAEBAAE/EJZNNaT3Nv4Z4Au9TuMJuj0YIhY0fqZb1nU69wkdUUix/MIi1U384pkeZ2r7n//Z" alt="" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                      <picture>
                        <source srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x">
                        <img srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" src="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg" alt="Photo of Hugo Bowne-Anderson" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                      </picture>
                      <noscript>
                        <picture>
                          <source srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" />
                          <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" src="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg" alt="Photo of Hugo Bowne-Anderson" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                        </picture>
                      </noscript>
                    </div>
                    <div>
                      <h5 class="css-1ogvh3l">Hugo Bowne-Anderson</h5>
                      <p class="css-f85llx">Data Scientist at DataCamp</p>
                    </div>
                  </footer>
                </a>
                <a href="/courses/free-introduction-to-r" class="css-179cao1" style="text-decoration:none">
                  <span class="css-11khr5q"><img alt="R Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzdERDRFNyIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzcwOTlENyIvPjwvbGluZWFyR3JhZGllbnQ+PHBhdGggaWQ9ImQiIGQ9Ik0xNjUgNTUuMDIzYy0uMDUtNC4xMTktLjg2Ny04LjIxNS0yLjM1LTEyLjA3My0xLjQ2Ni0zLjg3NS0zLjc1OC03LjQxNS02LjM4My0xMC42NDktNS4zMzUtNi40NDktMTIuMzc3LTExLjQ0NS0xOS45ODMtMTUuMTA4LTcuNjI2LTMuNjc4LTE1Ljg2LTYuMDU3LTI0LjE5NC03LjQxOC04LjM0OC0xLjM2Ny0xNi44MjgtMS41ODQtMjUuMTYtMS4wNjctOC4zMzUuNjEtMTYuNTM5IDIuMDItMjQuMzMgNC4zNjQtNy43OTMgMi4zMTItMTUuMjA4IDUuNDU4LTIxLjgzNiA5LjQ1QzM0LjE1OSAyNi41MjEgMjguMzQ5IDMxLjQxIDI0LjIgMzYuOTljLTQuMTggNS41Ny02LjQ4IDExLjc3OS02LjQ4IDE4LjAzNC0uMDA1IDYuMjUyIDIuMjc4IDEyLjQ3NSA2LjQ2MiAxOC4wNDYgNC4xMzYgNS41OTMgOS45NTMgMTAuNDkyIDE2LjU2IDE0LjQ5MSA2LjYyNSA0LjAwNCAxNC4wNDggNy4xNTIgMjEuODQ1IDkuNDYgNy43OTQgMi4zNTUgMTYuMDA1IDMuNzU2IDI0LjM0MiA0LjM2IDguMzM2LjUxNCAxNi44Mi4yOTYgMjUuMTY1LTEuMDgxIDguMzM2LTEuMzY1IDE2LjU3Mi0zLjc0OCAyNC4xOTYtNy40MzUgNy42MDctMy42NjUgMTQuNjQ0LTguNjY2IDE5Ljk3OC0xNS4xMTcgMi42MjQtMy4yMzQgNC45MTktNi43NzUgNi4zOC0xMC42NDkgMS40ODYtMy44NTggMi4zMDItNy45NTYgMi4zNS0xMi4wNzVtMCAwYy4wMzcgOC4yNjctMy4xMjYgMTYuMzgxLTguMjMgMjMuMDc5LTUuMDggNi43Ni0xMS44NDYgMTIuMzAxLTE5LjI4NSAxNi43NTYtNy40NyA0LjQ0Ni0xNS42NSA3Ljg0NS0yNC4xNjUgMTAuMzE4LTguNTE0IDIuNTA3LTE3LjM5NiAzLjk3OC0yNi4zODkgNC41ODUtMTcuOTQgMS4wNzctMzYuNzMxLTEuMzcyLTUzLjcyNy05LjYtOC40MzktNC4xMi0xNi40My05Ljc2NS0yMi43MDUtMTcuMzgxLTMuMDg5LTMuODItNS44MTItOC4wODctNy42MjQtMTIuODI4QzEuMDYgNjUuMjI4LjA0NyA2MC4xMzIgMCA1NS4wMjNjLjAzLTUuMTA4IDEuMDU0LTEwLjIwNiAyLjg2Ni0xNC45MzIgMS43ODItNC43NTEgNC41My05LjAxNCA3LjYxNC0xMi44MzYgMy4wOTQtMy44MzYgNi43Mi03LjEyNSAxMC41NC0xMC4wMTYgMy44Mi0yLjkwNSA3LjkzNy01LjMxNiAxMi4xNjEtNy4zOTJDNTAuMTgyIDEuNjA5IDY4Ljk4My0uODQ0IDg2LjkzMS4yNDNjOC45OTUuNjEgMTcuODgxIDIuMDgzIDI2LjM5NSA0LjU5OSA4LjUxNiAyLjQ4IDE2LjY5NyA1Ljg4MyAyNC4xNjYgMTAuMzM0IDcuNDM4IDQuNDU5IDE0LjIwMyAxMC4wMDYgMTkuMjggMTYuNzY3IDUuMTA1IDYuNyA4LjI2MiAxNC44MTUgOC4yMjggMjMuMDgiLz48cGF0aCBpZD0iZSIgZD0iTTk5LjA3MiA5My4yMzJjMTAuNzI2IDAgMTcuMjM3LTUuNzUxIDE3LjIzNy0xNi44NzkgMC0xMi4wNTUtNi41MTEtMTUuNTgtMTcuMjM3LTE1LjU4SDg2LjgxNXYzMi40NmgxMi4yNTd6bS0yLjg3MyAyMC4wM2gtOS4xOTVWMTYwSDYwVjQwaDQ0LjI0M2MyMS40NSAwIDQwLjk4OCA3LjIzNCA0MC45ODggMzQuMzE0IDAgMTYuMTM0LTkuMDAxIDI3LjgyLTIyLjQxIDMzLjk0TDE1NSAxNjBoLTMwLjA3bC0yOC43MzEtNDYuNzM4eiIvPjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxtYXNrIGlkPSJjIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjx1c2UgZmlsbD0iI0Q4RDhEOCIgeGxpbms6aHJlZj0iI2EiLz48ZyBmaWxsPSJ1cmwoI2IpIiBtYXNrPSJ1cmwoI2MpIj48cGF0aCBkPSJNMCAwaDMwMHYzMDBIMHoiLz48L2c+PGcgZmlsbD0iI0ZGRiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAgNjUpIj48dXNlIHhsaW5rOmhyZWY9IiNkIi8+PHVzZSB4bGluazpocmVmPSIjZSIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                  <div class="css-1hi4crg">
                    <h4 class="css-1a7hu85">Introduction to R</h4>
                    <p class="css-z6sjuf">Master the basics of data analysis by manipulating common data structures such as vectors, matrices, and data frames.</p>
                    <div class="css-4acvv2">
                      <span class="css-4acvv2">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        4 hours
                      </span>
                    </div>
                  </div>
                  <footer class="css-3guzbk">
                    <div class="css-3xxt4t gatsby-image-wrapper" style="position: relative; overflow: hidden; display: inline-block; width: 50px; height: 50px;">
                      <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAUDBAH/xAAVAQEBAAAAAAAAAAAAAAAAAAACAP/aAAwDAQACEAMQAAABp8/JjVJgYi+AdEB//8QAHBAAAQQDAQAAAAAAAAAAAAAAAgABAxESEyFB/9oACAEBAAEFArZxGTasEUotGEgAqtYtsfi8/8QAGBEAAgMAAAAAAAAAAAAAAAAAABAREiH/2gAIAQMBAT8BjCr/AP/EABgRAAIDAAAAAAAAAAAAAAAAAAAQERIh/9oACAECAQE/AZ0s/wD/xAAcEAACAgIDAAAAAAAAAAAAAAABEQACAxASIXH/2gAIAQEABj8CdolxWjUpzskeRjLFv//EABsQAAMAAwEBAAAAAAAAAAAAAAABESExUUFh/9oACAEBAAE/IeTJUphtkfC+GaoW7kfNOn6FOmPtH8EVaL0TcTGD/9oADAMBAAIAAwAAABDv137/xAAXEQEBAQEAAAAAAAAAAAAAAAABEQAQ/9oACAEDAQE/EAKujkN//8QAGBEAAgMAAAAAAAAAAAAAAAAAABEBECH/2gAIAQIBAT8QaMD0z//EAB0QAQADAQACAwAAAAAAAAAAAAEAESExQWGBofD/2gAIAQEAAT8QVQXeP3YZtTT2uxbdRIzQpVN5yAV408e7hEFoUt9MY6N97Dioo/MFa1ajJ//Z" alt="" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                      <picture>
                        <source srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x">
                        <img srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                          https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x" src="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg" alt="Photo of Jonathan Cornelissen" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                      </picture>
                      <noscript>
                        <picture>
                          <source srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                            https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                            https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x" />
                          <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg 1x,
                            https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/25b50/jonathan.jpg 1.5x,
                            https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/86e11/jonathan.jpg 2x" src="https://static.datacamp.com/static/6c0f223866067a82e5ec7eae01279d8e/2c7f8/jonathan.jpg" alt="Photo of Jonathan Cornelissen" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                        </picture>
                      </noscript>
                    </div>
                    <div>
                      <h5 class="css-1ogvh3l">Jonathan Cornelissen</h5>
                      <p class="css-f85llx">Co-founder of DataCamp</p>
                    </div>
                  </footer>
                </a>
                <a href="/courses/introduction-to-sql" class="css-179cao1" style="text-decoration:none">
                  <span class="css-11khr5q"><img alt="SQL Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI0I4QkNGOSIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzk4OERFNiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PG1hc2sgaWQ9ImMiIGZpbGw9IiNmZmYiPjx1c2UgeGxpbms6aHJlZj0iI2EiLz48L21hc2s+PHVzZSBmaWxsPSIjRDhEOEQ4IiB4bGluazpocmVmPSIjYSIvPjxnIGZpbGw9InVybCgjYikiIG1hc2s9InVybCgjYykiPjxwYXRoIGQ9Ik0wIDBoMzAwdjMwMEgweiIvPjwvZz48cGF0aCBmaWxsPSIjRkZGIiBkPSJNMTQ4Ljk3IDE4OC44OThhMjg3LjU4NCAyODcuNTg0IDAgMCAwIDU0LjY5LTQuNjE3IDYzLjkxOSA2My45MTkgMCAwIDAgMTkuMjczLTYuMzU5djI1LjgyM2MwIDguNjE1LTMzLjExNCAxNS41OTEtNzMuOTYyIDE1LjU5MS00MC44NDkgMC03My45NjEtNi45NzYtNzMuOTYxLTE1LjU5di0yNS44MjRhNjMuOTA0IDYzLjkwNCAwIDAgMCAxOS4yNzMgNi4zNTkgMjg3LjU2NyAyODcuNTY3IDAgMCAwIDU0LjY4OCA0LjYxN3ptMC00MC43MDFhMjg3LjQyOSAyODcuNDI5IDAgMCAwIDU0LjYzOC00LjU2NiA2My44OTQgNjMuODk0IDAgMCAwIDE5LjI3Mi02LjM1OXYyNC44NDhoLS40MTRjMCA0LjM2LTI1LjgxNiAxMi44NzQtNzMuNDk1IDEyLjg3NC00Ny42NzggMC03My41NDctOC40OS03My41NDctMTIuODIzaC0uNDE0di0yNC44OTlhNjMuOTEgNjMuOTEgMCAwIDAgMTkuMjczIDYuMzA4IDI4Ny41NjcgMjg3LjU2NyAwIDAgMCA1NC42ODggNC42MTd6bTczLjkzNi01Ny42MDZhMy4yOSAzLjI5IDAgMCAxLS4wMjYuNDYxYy4wMTMuMTcyLjAxMy4zNDEgMCAuNTEzdjI4LjIwN2gtLjQxNGMwIDQuMzU4LTI1LjgxNiAxMi44NzItNzMuNDk1IDEyLjg3Mi00Ny42NzggMC03My41NDctOC40ODctNzMuNTQ3LTEyLjgyaC0uNDE0VjkxLjYxNWEzLjI3NSAzLjI3NSAwIDAgMSAwLS41MTEgMy4zMTcgMy4zMTcgMCAwIDEgMC0uNTE0YzAtOC42MTUgMzMuMTM5LTE1LjU5IDczLjk2LTE1LjU5IDQwLjgyNCAwIDczLjkzNiA2Ljk3NSA3My45MzYgMTUuNTl6IiBtYXNrPSJ1cmwoI2MpIi8+PC9nPjwvc3ZnPg==" width="40"></span>
                  <div class="css-1hi4crg">
                    <h4 class="css-1a7hu85">Introduction to SQL</h4>
                    <p class="css-z6sjuf">Master the basics of querying tables in relational databases such as MySQL, SQL Server, and PostgreSQL.</p>
                    <div class="css-4acvv2">
                      <span class="css-4acvv2">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        4 hours
                      </span>
                    </div>
                  </div>
                  <footer class="css-3guzbk">
                    <div class="css-3xxt4t gatsby-image-wrapper" style="position: relative; overflow: hidden; display: inline-block; width: 50px; height: 50px;">
                      <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGQABAQADAQAAAAAAAAAAAAAAAAQBAgMF/8QAFwEBAQEBAAAAAAAAAAAAAAAAAgABA//aAAwDAQACEAMQAAABr1mxtc4lzn5E/QAX/8QAHRAAAgIBBQAAAAAAAAAAAAAAAQIAAzEEEBESE//aAAgBAQABBQLmJ22tYrZ6NEsJXUZXKABf/8QAFxEBAAMAAAAAAAAAAAAAAAAAEAERIf/aAAgBAwEBPwHKJP/EABYRAQEBAAAAAAAAAAAAAAAAABABIf/aAAgBAgEBPwHSH//EABkQAAMBAQEAAAAAAAAAAAAAAAABMREQIf/aAAgBAQAGPwKnrZDEUhvFiP/EABsQAAMAAwEBAAAAAAAAAAAAAAABESExYVHw/9oACAEBAAE/IVDaWSPYXdPmR4ZKKaiKc2IjVXgimM+madElBH//2gAMAwEAAgADAAAAEHswP//EABcRAQEBAQAAAAAAAAAAAAAAAAEAETH/2gAIAQMBAT8QAqcuCVG//8QAGBEAAwEBAAAAAAAAAAAAAAAAAAEhEUH/2gAIAQIBAT8Qti07Eof/xAAdEAEAAgIDAQEAAAAAAAAAAAABABEhMUFRYXHx/9oACAEBAAE/EKpDgSWC6WEbvjGoqrRfYHhTd1aSusZ42ewMgS3+pjRWjtUxbpA/IEAiqF7n/9k=" alt="" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                      <picture>
                        <source srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x">
                        <img srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                          https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x" src="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg" alt="Photo of Nick Carchedi" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                      </picture>
                      <noscript>
                        <picture>
                          <source srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                            https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                            https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x" />
                          <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg 1x,
                            https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/25b50/NickAboutPic.jpg 1.5x,
                            https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/86e11/NickAboutPic.jpg 2x" src="https://static.datacamp.com/static/f860549a40799b9665904af88142fec3/2c7f8/NickAboutPic.jpg" alt="Photo of Nick Carchedi" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                        </picture>
                      </noscript>
                    </div>
                    <div>
                      <h5 class="css-1ogvh3l">Nick Carchedi</h5>
                      <p class="css-f85llx">Product Manager at DataCamp</p>
                    </div>
                  </footer>
                </a>
                <a href="/courses/intermediate-python" class="css-179cao1" style="text-decoration:none">
                  <span class="css-11khr5q"><img alt="Python Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjMuMjQ5JSIgeTI9Ijk2Ljc3NCUiPjxzdG9wIG9mZnNldD0iMS41MzklIiBzdG9wLWNvbG9yPSIjODJEQTlFIi8+PHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjNDhBRTc2Ii8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48bWFzayBpZD0iYyIgZmlsbD0iI2ZmZiI+PHVzZSB4bGluazpocmVmPSIjYSIvPjwvbWFzaz48dXNlIGZpbGw9IiNEOEQ4RDgiIHhsaW5rOmhyZWY9IiNhIi8+PGcgZmlsbD0idXJsKCNiKSIgbWFzaz0idXJsKCNjKSI+PHBhdGggZD0iTTAgMGgzMDV2MzA1SDB6Ii8+PC9nPjxnIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PHBhdGggZD0iTTE1MC4wODIgNzAuMDAxYy02LjY4NC4wMzEtMTMuMDY3LjU5Ni0xOC42ODMgMS41OC0xNi41NDUgMi44OTUtMTkuNTQ5IDguOTU0LTE5LjU0OSAyMC4xMjd2MTQuNzU4aDM5LjA5OHY0LjkxOUg5Ny4xNzdjLTExLjM2MyAwLTIxLjMxMiA2Ljc2NC0yNC40MjUgMTkuNjMxLTMuNTkgMTQuNzQ5LTMuNzQ5IDIzLjk1MyAwIDM5LjM1M0M3NS41MzIgMTgxLjgzMiA4Mi4xNyAxOTAgOTMuNTMyIDE5MGgxMy40NDJ2LTE3LjY5YzAtMTIuNzgxIDExLjE2Ni0yNC4wNTUgMjQuNDI1LTI0LjA1NWgzOS4wNTJjMTAuODcgMCAxOS41NDktOC44NjQgMTkuNTQ5LTE5LjY3NlY5MS43MWMwLTEwLjQ5NC04LjkzOS0xOC4zNzctMTkuNTQ5LTIwLjEyOC02LjcxNi0xLjEwNy0xMy42ODUtMS42MS0yMC4zNjktMS41OHptLTIxLjE0NCAxMS44N2M0LjAzOSAwIDcuMzM3IDMuMzE5IDcuMzM3IDcuNCAwIDQuMDY4LTMuMjk4IDcuMzU3LTcuMzM3IDcuMzU3LTQuMDUzIDAtNy4zMzYtMy4yOS03LjMzNi03LjM1NiAwLTQuMDgyIDMuMjgzLTcuNDAyIDcuMzM2LTcuNDAyeiIvPjxwYXRoIGQ9Ik0xOTIuODU3IDExMHYxNy4wNjNjMCAxMy4yMy0xMS4zODggMjQuMzY0LTI0LjM3NSAyNC4zNjRoLTM4Ljk3M2MtMTAuNjc1IDAtMTkuNTA5IDguOTk4LTE5LjUwOSAxOS41Mjd2MzYuNTljMCAxMC40MTMgOS4xOTUgMTYuNTM5IDE5LjUxIDE5LjUyNiAxMi4zNSAzLjU3NyAyNC4xOTQgNC4yMjMgMzguOTcyIDAgOS44MjMtMi44IDE5LjUxLTguNDM4IDE5LjUxLTE5LjUyNnYtMTQuNjQ1aC0zOC45NzR2LTQuODgyaDU4LjQ4M2MxMS4zNCAwIDE1LjU2NS03Ljc5IDE5LjUwOC0xOS40ODIgNC4wNzQtMTIuMDM3IDMuOS0yMy42MTIgMC0zOS4wNTNDMjI0LjIwNyAxMTguMzY0IDIxOC44NTQgMTEwIDIwNy41IDExMGgtMTQuNjQzem0tMjEuOTIgOTIuNjYyYzQuMDQ1IDAgNy4zMjIgMy4yNjQgNy4zMjIgNy4zIDAgNC4wNS0zLjI3NyA3LjM0NS03LjMyMSA3LjM0NS00LjAzIDAtNy4zMjItMy4yOTQtNy4zMjItNy4zNDUgMC00LjAzNiAzLjI5MS03LjMgNy4zMjItNy4zeiIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                  <div class="css-1hi4crg">
                    <h4 class="css-1a7hu85">Intermediate Python</h4>
                    <p class="css-z6sjuf">Level up your data science skills by creating visualizations using Matplotlib and manipulating DataFrames with pandas.…</p>
                    <div class="css-4acvv2">
                      <span class="css-4acvv2">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        4 hours
                      </span>
                    </div>
                  </div>
                  <footer class="css-3guzbk">
                    <div class="css-3xxt4t gatsby-image-wrapper" style="position: relative; overflow: hidden; display: inline-block; width: 50px; height: 50px;">
                      <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGQABAAIDAAAAAAAAAAAAAAAAAAMEAQIF/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/9oADAMBAAIQAxAAAAG5DrBVtgleAa6Qzf/EABwQAAICAgMAAAAAAAAAAAAAAAABAhEDIQQSIv/aAAgBAQABBQJ6IpjM+Sn3lV2chEdiXn//xAAXEQADAQAAAAAAAAAAAAAAAAAAAREQ/9oACAEDAQE/AVIQef/EABgRAAIDAAAAAAAAAAAAAAAAAAABEBEh/9oACAECAQE/AdLYo//EABsQAAIDAAMAAAAAAAAAAAAAAAABEBEhEkFx/9oACAEBAAY/AjW44o8hvuxCrD//xAAbEAADAQEBAQEAAAAAAAAAAAAAAREhMUFRof/aAAgBAQABPyGGrutnY9u6YfEyRg9rEpJL+hTTorj4U2R+nyCcR//aAAwDAQACAAMAAAAQVAeD/8QAGhEAAgIDAAAAAAAAAAAAAAAAAAERITFBgf/aAAgBAwEBPxC5JA04PLP/xAAZEQEAAgMAAAAAAAAAAAAAAAABABAhMUH/2gAIAQIBAT8QROKO4an/xAAdEAEBAAICAwEAAAAAAAAAAAABEQAhMUFRYXGR/9oACAEBAAE/EDU9gfcAjBQGi9ScYCH2Vxt+K7CrJ+YJogtDfHn3rGmth16ucEQfhHAbEQODtMHhM//Z" alt="" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                      <picture>
                        <source srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x">
                        <img srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x" src="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg" alt="Photo of Filip Schouwenaars" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                      </picture>
                      <noscript>
                        <picture>
                          <source srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x" />
                          <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x" src="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg" alt="Photo of Filip Schouwenaars" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                        </picture>
                      </noscript>
                    </div>
                    <div>
                      <h5 class="css-1ogvh3l">Filip Schouwenaars</h5>
                      <p class="css-f85llx">Data Science Instructor at DataCamp</p>
                    </div>
                  </footer>
                </a>
                <a href="/courses/intermediate-r" class="css-179cao1" style="text-decoration:none">
                  <span class="css-11khr5q"><img alt="R Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjAlIiB5Mj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzdERDRFNyIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzcwOTlENyIvPjwvbGluZWFyR3JhZGllbnQ+PHBhdGggaWQ9ImQiIGQ9Ik0xNjUgNTUuMDIzYy0uMDUtNC4xMTktLjg2Ny04LjIxNS0yLjM1LTEyLjA3My0xLjQ2Ni0zLjg3NS0zLjc1OC03LjQxNS02LjM4My0xMC42NDktNS4zMzUtNi40NDktMTIuMzc3LTExLjQ0NS0xOS45ODMtMTUuMTA4LTcuNjI2LTMuNjc4LTE1Ljg2LTYuMDU3LTI0LjE5NC03LjQxOC04LjM0OC0xLjM2Ny0xNi44MjgtMS41ODQtMjUuMTYtMS4wNjctOC4zMzUuNjEtMTYuNTM5IDIuMDItMjQuMzMgNC4zNjQtNy43OTMgMi4zMTItMTUuMjA4IDUuNDU4LTIxLjgzNiA5LjQ1QzM0LjE1OSAyNi41MjEgMjguMzQ5IDMxLjQxIDI0LjIgMzYuOTljLTQuMTggNS41Ny02LjQ4IDExLjc3OS02LjQ4IDE4LjAzNC0uMDA1IDYuMjUyIDIuMjc4IDEyLjQ3NSA2LjQ2MiAxOC4wNDYgNC4xMzYgNS41OTMgOS45NTMgMTAuNDkyIDE2LjU2IDE0LjQ5MSA2LjYyNSA0LjAwNCAxNC4wNDggNy4xNTIgMjEuODQ1IDkuNDYgNy43OTQgMi4zNTUgMTYuMDA1IDMuNzU2IDI0LjM0MiA0LjM2IDguMzM2LjUxNCAxNi44Mi4yOTYgMjUuMTY1LTEuMDgxIDguMzM2LTEuMzY1IDE2LjU3Mi0zLjc0OCAyNC4xOTYtNy40MzUgNy42MDctMy42NjUgMTQuNjQ0LTguNjY2IDE5Ljk3OC0xNS4xMTcgMi42MjQtMy4yMzQgNC45MTktNi43NzUgNi4zOC0xMC42NDkgMS40ODYtMy44NTggMi4zMDItNy45NTYgMi4zNS0xMi4wNzVtMCAwYy4wMzcgOC4yNjctMy4xMjYgMTYuMzgxLTguMjMgMjMuMDc5LTUuMDggNi43Ni0xMS44NDYgMTIuMzAxLTE5LjI4NSAxNi43NTYtNy40NyA0LjQ0Ni0xNS42NSA3Ljg0NS0yNC4xNjUgMTAuMzE4LTguNTE0IDIuNTA3LTE3LjM5NiAzLjk3OC0yNi4zODkgNC41ODUtMTcuOTQgMS4wNzctMzYuNzMxLTEuMzcyLTUzLjcyNy05LjYtOC40MzktNC4xMi0xNi40My05Ljc2NS0yMi43MDUtMTcuMzgxLTMuMDg5LTMuODItNS44MTItOC4wODctNy42MjQtMTIuODI4QzEuMDYgNjUuMjI4LjA0NyA2MC4xMzIgMCA1NS4wMjNjLjAzLTUuMTA4IDEuMDU0LTEwLjIwNiAyLjg2Ni0xNC45MzIgMS43ODItNC43NTEgNC41My05LjAxNCA3LjYxNC0xMi44MzYgMy4wOTQtMy44MzYgNi43Mi03LjEyNSAxMC41NC0xMC4wMTYgMy44Mi0yLjkwNSA3LjkzNy01LjMxNiAxMi4xNjEtNy4zOTJDNTAuMTgyIDEuNjA5IDY4Ljk4My0uODQ0IDg2LjkzMS4yNDNjOC45OTUuNjEgMTcuODgxIDIuMDgzIDI2LjM5NSA0LjU5OSA4LjUxNiAyLjQ4IDE2LjY5NyA1Ljg4MyAyNC4xNjYgMTAuMzM0IDcuNDM4IDQuNDU5IDE0LjIwMyAxMC4wMDYgMTkuMjggMTYuNzY3IDUuMTA1IDYuNyA4LjI2MiAxNC44MTUgOC4yMjggMjMuMDgiLz48cGF0aCBpZD0iZSIgZD0iTTk5LjA3MiA5My4yMzJjMTAuNzI2IDAgMTcuMjM3LTUuNzUxIDE3LjIzNy0xNi44NzkgMC0xMi4wNTUtNi41MTEtMTUuNTgtMTcuMjM3LTE1LjU4SDg2LjgxNXYzMi40NmgxMi4yNTd6bS0yLjg3MyAyMC4wM2gtOS4xOTVWMTYwSDYwVjQwaDQ0LjI0M2MyMS40NSAwIDQwLjk4OCA3LjIzNCA0MC45ODggMzQuMzE0IDAgMTYuMTM0LTkuMDAxIDI3LjgyLTIyLjQxIDMzLjk0TDE1NSAxNjBoLTMwLjA3bC0yOC43MzEtNDYuNzM4eiIvPjwvZGVmcz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxtYXNrIGlkPSJjIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjx1c2UgZmlsbD0iI0Q4RDhEOCIgeGxpbms6aHJlZj0iI2EiLz48ZyBmaWxsPSJ1cmwoI2IpIiBtYXNrPSJ1cmwoI2MpIj48cGF0aCBkPSJNMCAwaDMwMHYzMDBIMHoiLz48L2c+PGcgZmlsbD0iI0ZGRiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAgNjUpIj48dXNlIHhsaW5rOmhyZWY9IiNkIi8+PHVzZSB4bGluazpocmVmPSIjZSIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                  <div class="css-1hi4crg">
                    <h4 class="css-1a7hu85">Intermediate R</h4>
                    <p class="css-z6sjuf">Continue your journey to becoming an R ninja by learning about conditional statements, loops, and vector functions.</p>
                    <div class="css-4acvv2">
                      <span class="css-4acvv2">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        6 hours
                      </span>
                    </div>
                  </div>
                  <footer class="css-3guzbk">
                    <div class="css-3xxt4t gatsby-image-wrapper" style="position: relative; overflow: hidden; display: inline-block; width: 50px; height: 50px;">
                      <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGQABAAIDAAAAAAAAAAAAAAAAAAMEAQIF/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/9oADAMBAAIQAxAAAAG5DrBVtgleAa6Qzf/EABwQAAICAgMAAAAAAAAAAAAAAAABAhEDIQQSIv/aAAgBAQABBQJ6IpjM+Sn3lV2chEdiXn//xAAXEQADAQAAAAAAAAAAAAAAAAAAAREQ/9oACAEDAQE/AVIQef/EABgRAAIDAAAAAAAAAAAAAAAAAAABEBEh/9oACAECAQE/AdLYo//EABsQAAIDAAMAAAAAAAAAAAAAAAABEBEhEkFx/9oACAEBAAY/AjW44o8hvuxCrD//xAAbEAADAQEBAQEAAAAAAAAAAAAAAREhMUFRof/aAAgBAQABPyGGrutnY9u6YfEyRg9rEpJL+hTTorj4U2R+nyCcR//aAAwDAQACAAMAAAAQVAeD/8QAGhEAAgIDAAAAAAAAAAAAAAAAAAERITFBgf/aAAgBAwEBPxC5JA04PLP/xAAZEQEAAgMAAAAAAAAAAAAAAAABABAhMUH/2gAIAQIBAT8QROKO4an/xAAdEAEBAAICAwEAAAAAAAAAAAABEQAhMUFRYXGR/9oACAEBAAE/EDU9gfcAjBQGi9ScYCH2Vxt+K7CrJ+YJogtDfHn3rGmth16ucEQfhHAbEQODtMHhM//Z" alt="" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                      <picture>
                        <source srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x">
                        <img srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x" src="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg" alt="Photo of Filip Schouwenaars" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                      </picture>
                      <noscript>
                        <picture>
                          <source srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x" />
                          <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/25b50/Filipaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/86e11/Filipaboutpic.jpg 2x" src="https://static.datacamp.com/static/e57673425b18194f71e29408e7b31e13/2c7f8/Filipaboutpic.jpg" alt="Photo of Filip Schouwenaars" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                        </picture>
                      </noscript>
                    </div>
                    <div>
                      <h5 class="css-1ogvh3l">Filip Schouwenaars</h5>
                      <p class="css-f85llx">Data Science Instructor at DataCamp</p>
                    </div>
                  </footer>
                </a>
                <a href="/courses/python-data-science-toolbox-part-1" class="css-179cao1" style="text-decoration:none">
                  <span class="css-11khr5q"><img alt="Python Icon" height="40" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIgdmlld0JveD0iMCAwIDMwMCAzMDAiPjxkZWZzPjxjaXJjbGUgaWQ9ImEiIGN4PSIxNTAiIGN5PSIxNTAiIHI9IjE1MCIvPjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjUwJSIgeDI9IjUwJSIgeTE9IjMuMjQ5JSIgeTI9Ijk2Ljc3NCUiPjxzdG9wIG9mZnNldD0iMS41MzklIiBzdG9wLWNvbG9yPSIjODJEQTlFIi8+PHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjNDhBRTc2Ii8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48bWFzayBpZD0iYyIgZmlsbD0iI2ZmZiI+PHVzZSB4bGluazpocmVmPSIjYSIvPjwvbWFzaz48dXNlIGZpbGw9IiNEOEQ4RDgiIHhsaW5rOmhyZWY9IiNhIi8+PGcgZmlsbD0idXJsKCNiKSIgbWFzaz0idXJsKCNjKSI+PHBhdGggZD0iTTAgMGgzMDV2MzA1SDB6Ii8+PC9nPjxnIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PHBhdGggZD0iTTE1MC4wODIgNzAuMDAxYy02LjY4NC4wMzEtMTMuMDY3LjU5Ni0xOC42ODMgMS41OC0xNi41NDUgMi44OTUtMTkuNTQ5IDguOTU0LTE5LjU0OSAyMC4xMjd2MTQuNzU4aDM5LjA5OHY0LjkxOUg5Ny4xNzdjLTExLjM2MyAwLTIxLjMxMiA2Ljc2NC0yNC40MjUgMTkuNjMxLTMuNTkgMTQuNzQ5LTMuNzQ5IDIzLjk1MyAwIDM5LjM1M0M3NS41MzIgMTgxLjgzMiA4Mi4xNyAxOTAgOTMuNTMyIDE5MGgxMy40NDJ2LTE3LjY5YzAtMTIuNzgxIDExLjE2Ni0yNC4wNTUgMjQuNDI1LTI0LjA1NWgzOS4wNTJjMTAuODcgMCAxOS41NDktOC44NjQgMTkuNTQ5LTE5LjY3NlY5MS43MWMwLTEwLjQ5NC04LjkzOS0xOC4zNzctMTkuNTQ5LTIwLjEyOC02LjcxNi0xLjEwNy0xMy42ODUtMS42MS0yMC4zNjktMS41OHptLTIxLjE0NCAxMS44N2M0LjAzOSAwIDcuMzM3IDMuMzE5IDcuMzM3IDcuNCAwIDQuMDY4LTMuMjk4IDcuMzU3LTcuMzM3IDcuMzU3LTQuMDUzIDAtNy4zMzYtMy4yOS03LjMzNi03LjM1NiAwLTQuMDgyIDMuMjgzLTcuNDAyIDcuMzM2LTcuNDAyeiIvPjxwYXRoIGQ9Ik0xOTIuODU3IDExMHYxNy4wNjNjMCAxMy4yMy0xMS4zODggMjQuMzY0LTI0LjM3NSAyNC4zNjRoLTM4Ljk3M2MtMTAuNjc1IDAtMTkuNTA5IDguOTk4LTE5LjUwOSAxOS41Mjd2MzYuNTljMCAxMC40MTMgOS4xOTUgMTYuNTM5IDE5LjUxIDE5LjUyNiAxMi4zNSAzLjU3NyAyNC4xOTQgNC4yMjMgMzguOTcyIDAgOS44MjMtMi44IDE5LjUxLTguNDM4IDE5LjUxLTE5LjUyNnYtMTQuNjQ1aC0zOC45NzR2LTQuODgyaDU4LjQ4M2MxMS4zNCAwIDE1LjU2NS03Ljc5IDE5LjUwOC0xOS40ODIgNC4wNzQtMTIuMDM3IDMuOS0yMy42MTIgMC0zOS4wNTNDMjI0LjIwNyAxMTguMzY0IDIxOC44NTQgMTEwIDIwNy41IDExMGgtMTQuNjQzem0tMjEuOTIgOTIuNjYyYzQuMDQ1IDAgNy4zMjIgMy4yNjQgNy4zMjIgNy4zIDAgNC4wNS0zLjI3NyA3LjM0NS03LjMyMSA3LjM0NS00LjAzIDAtNy4zMjItMy4yOTQtNy4zMjItNy4zNDUgMC00LjAzNiAzLjI5MS03LjMgNy4zMjItNy4zeiIvPjwvZz48L2c+PC9zdmc+" width="40"></span>
                  <div class="css-1hi4crg">
                    <h4 class="css-1a7hu85">Python Data Science Toolbox (Part 1)</h4>
                    <p class="css-z6sjuf">Learn the art of writing your own functions in Python, as well as key concepts like scoping and error handling.</p>
                    <div class="css-4acvv2">
                      <span class="css-4acvv2">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-mr-8 dc-icon--flex">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M8.35 9.689a.943.943 0 0 1-.278-.67V5.614a.946.946 0 1 1 1.893 0v2.46h2.082a.946.946 0 0 1 0 1.893H9.02a.943.943 0 0 1-.67-.277zM9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-1.837A7.163 7.163 0 1 0 9 1.837a7.163 7.163 0 0 0 0 14.326z" fill-rule="evenodd"></path>
                          </svg>
                        </span>
                        3 hours
                      </span>
                      <button class="dc-btn dc-btn--unstyled dc-u-p-none" type="button">
                        <span class="dc-icon dc-icon--primary dc-icon--size-12 dc-u-ml-24 dc-u-mr-8 dc-u-top-none">
                          <svg class="dc-icon__svg" width="18" height="18" viewBox="0 0 18 18">
                            <path d="M2.32 18A1.32 1.32 0 0 1 1 16.68V1.32A1.32 1.32 0 0 1 2.98.177l13.3 7.68a1.32 1.32 0 0 1 0 2.288l-13.3 7.679a1.32 1.32 0 0 1-.66.176zM3.64 3.61V14.39l9.339-5.39L3.64 3.608z" fill-rule="nonzero"></path>
                          </svg>
                        </span>
                        Play preview
                      </button>
                    </div>
                  </div>
                  <footer class="css-3guzbk">
                    <div class="css-3xxt4t gatsby-image-wrapper" style="position: relative; overflow: hidden; display: inline-block; width: 50px; height: 50px;">
                      <img aria-hidden="true" src="data:image/jpeg;base64,/9j/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEAMQAAABsxk7os81F7hP/8QAHBAAAgICAwAAAAAAAAAAAAAAAQISIQMiMTJC/9oACAEBAAEFAgNpyZ6bhhRYyPtqXH1//8QAFhEAAwAAAAAAAAAAAAAAAAAAABAR/9oACAEDAQE/ASP/xAAWEQADAAAAAAAAAAAAAAAAAAAAEBH/2gAIAQIBAT8BK//EABgQAAMBAQAAAAAAAAAAAAAAAAABERAx/9oACAEBAAY/AkdxxlKMef/EABwQAQEBAAIDAQAAAAAAAAAAAAERACExQXGBof/aAAgBAQABPyG3R8yp9LAuRKfuVEmHFl84fQyRM1RuWf/aAAwDAQACAAMAAAAQGCAD/8QAGREAAwADAAAAAAAAAAAAAAAAAAERECEx/9oACAEDAQE/EFJsQfMf/8QAGREBAAIDAAAAAAAAAAAAAAAAAQAhEBFB/9oACAECAQE/EEd1KQ7j/8QAHBABAQADAQADAAAAAAAAAAAAAREAITFBcYGR/9oACAEBAAE/EJZNNaT3Nv4Z4Au9TuMJuj0YIhY0fqZb1nU69wkdUUix/MIi1U384pkeZ2r7n//Z" alt="" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 0; transition-delay: 500ms;">
                      <picture>
                        <source srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x">
                        <img srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                          https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" src="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg" alt="Photo of Hugo Bowne-Anderson" width="50" height="50" loading="lazy" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; object-fit: cover; object-position: center center; opacity: 1; transition: opacity 500ms ease 0s;">
                      </picture>
                      <noscript>
                        <picture>
                          <source srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" />
                          <img loading="lazy" width="50" height="50" srcset="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg 1x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/25b50/hugoaboutpic.jpg 1.5x,
                            https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/86e11/hugoaboutpic.jpg 2x" src="https://static.datacamp.com/static/e40713a69926168fbf944b66c1695f17/2c7f8/hugoaboutpic.jpg" alt="Photo of Hugo Bowne-Anderson" style="position:absolute;top:0;left:0;opacity:1;width:100%;height:100%;object-fit:cover;object-position:center"/>
                        </picture>
                      </noscript>
                    </div>
                    <div>
                      <h5 class="css-1ogvh3l">Hugo Bowne-Anderson</h5>
                      <p class="css-f85llx">Data Scientist at DataCamp</p>
                    </div>
                  </footer>
                </a>
              </nav>
            </div>
            <br><br>
            <div class="css-1k1n1og">
                <a class="dc-btn dc-btn--tertiary" href="/courses/all">
                    See all 327 courses
                </a>
            </div>
          </section>
    
          @include('footer')
        </div>
        
      </div>
    </div>
     </body>
</html>