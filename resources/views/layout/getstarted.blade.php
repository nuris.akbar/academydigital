<section class="get-started get-started--videodownloader">
    <div class="platforms get-started__platforms">
        <h3 class="platforms__title h2">4K Video Downloader is cross-platform. Get it for your Windows, macOS or Linux.</h3>
        <div class="platform platform--macos"><img data-src="https://static.4kdownload.com/main/img/redesign/macos-logo.9aa85a939f46.svg" class="platform__logo lazy" alt="macOS logo" width="80" height="76"><span class="platform__title">macOS</span></div>
        <div class="platform platform--windows">
            <svg class="platform__logo platform__logo--windows" width="79px" height="79px">
                <use xlink:href="#windows-logo"></use>
            </svg>
            <span class="platform__title">Windows</span></div>
        <div class="platform platform--ubuntu">
            <svg class="platform__logo platform__logo--ubuntu" width="80px" height="80px">
                <use xlink:href="#ubuntu-logo"></use>
            </svg>
            <span class="platform__title">Ubuntu</span></div>
    </div>

    <div class="get-started__wrap">
        <p class="get-started__title  h1">Get started now - it's free!</p>
        <div class="get-started__actions">

            <a class="promo__btn btn--videodownloader  btn btn--download  btn--large download-button-js" href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3_x64.msi?source=website" onclick="ga('send', 'event', '/downloads', 'Download', 'videodownloader_4.11.3.3420_windows_x64');setAppDownloadedStatus('videodownloader'); setTimeout(function() {$('[data-remodal-id=thanks-modal]').remodal() .open();}, 2000); return true;">
                <i class="icon btn__icon"><img src="https://static.4kdownload.com/main/img/redesign/ic-videodownloader.f7d4fd5cb85b.svg" alt="4K Video Downloader" width="30" height="20"/>
</i>Get 4K Video Downloader</a>
            <select class="select js-select-OS">

                <option value="windows_x64" data-type="installer" data-icon="windows-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3_x64.msi?source=website" selected="selected">
                    Microsoft Windows 64-bit</option>

                <option value="windows_x64" data-type="portable" data-icon="windows-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3_x64.zip?source=website">
                    Microsoft Windows 64-bit Portable</option>

                <option value="windows_x86" data-type="installer" data-icon="windows-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3.msi?source=website">
                    Microsoft Windows</option>

                <option value="windows_x86" data-type="portable" data-icon="windows-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3.zip?source=website">
                    Microsoft Windows Portable</option>

                <option value="osx_x64" data-type="installer" data-icon="apple-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3.dmg?source=website">
                    macOS 10.13 and later</option>

                <option value="ubuntu_x64" data-type="installer" data-icon="ubuntu-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3-1_amd64.deb?source=website">Ubuntu 64-bit</option>

                <option value="ubuntu_x64" data-type="portable" data-icon="ubuntu-logo" data-href="https://dl.4kdownload.com/app/4kvideodownloader_4.11.3_amd64.tar.bz2?source=website">Ubuntu 64-bit portable</option>

            </select>
        </div>
    </div>

</section>