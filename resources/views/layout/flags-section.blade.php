<section class="section flags-section">
    <h3 class="flags-section__title h1">4K Video Downloader speaks your language</h3>
    <div class="flags">
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/en.de4cf710e398.svg" class="flag__icon lazy" alt="USA flag icon"><span class="flag__title">English</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/de.5dcef5c8aad9.svg" class="flag__icon lazy" alt="Germany flag icon"><span class="flag__title">Deutsch</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/fr.e883f7470f84.svg" class="flag__icon lazy" alt="France flag icon"><span class="flag__title">Français</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/it.c8be4cbcbcd5.svg" class="flag__icon lazy" alt="Italy flag icon"><span class="flag__title">Italiano</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/es.71fb3e43f90b.svg" class="flag__icon lazy" alt="Spain flag icon"><span class="flag__title">Español</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/nl.2d9bd43ff3a3.svg" class="flag__icon lazy" alt="Netherlands flag icon"><span class="flag__title">Nederlands</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/ru.a3a6bc8a1039.svg" class="flag__icon lazy" alt="Russia flag icon"><span class="flag__title">Русский</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/pl.cec4b6924457.svg" class="flag__icon lazy" alt="Poland flag icon"><span class="flag__title">Polski</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/ja.c3d98e9adf93.svg" class="flag__icon lazy" alt="Japan flag icon"><span class="flag__title">日本語</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/zh-cn.75cf4faa19a8.svg" class="flag__icon lazy" alt="China flag icon"><span class="flag__title">繁體中文</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/ko.506979d0f849.svg" class="flag__icon lazy" alt="South korean flag icon"><span class="flag__title">한국어</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/pt-br.2be292903a88.svg" class="flag__icon lazy" alt="Portugal flag icon"><span class="flag__title">Português do Brasil</span>
        </div>
        <div class="flag">
            <img data-src="https://static.4kdownload.com/main/img/redesign/zh-cn.75cf4faa19a8.svg" class="flag__icon lazy" alt="China flag icon"><span class="flag__title">简体中文</span>
        </div>
    </div>
</section>