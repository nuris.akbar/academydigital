<div id="comments" class="comments-section">
    <div class="comments-section__summary">

        <div class="comments-section__heading">
            <p class="comments-section__title h1">Summary</p>
            <div class="comments-section__heading-socials">

                <div class="social social-line">

                    <div class="social__item">
                        <div data-id="fb-social-script" class="fb-like fb_iframe_widget lazy-script" data-href="http://www.facebook.com/4kdownload" data-action="like" data-size="large" data-width="120" data-show-faces="false" data-layout="button_count"></div>
                    </div>

                    <div data-id="twitter-social-script" class="social__item  lazy-script">
                        <a href="http://twitter.com/share" class="twitter-share-button" data-url="https://fkd.page.link/Anr5" data-via="4kdownload" data-count="none" data-size="large">Tweet</a>
                    </div>

                    <a href="//www.reddit.com/submit?url=http://www.4kdownload.com/products/product-videodownloader" onclick="window.open('http://www.reddit.com/submit?url=' + encodeURIComponent('http://www.4kdownload.com/products/product-videodownloader')); return false" class="social__item social__item--reddit"><img src="https://static.4kdownload.com/main/img/redesign/ic-reddit-square.5413227fb00d.svg" alt="Submit to reddit"></a>

                </div>

            </div>
        </div>
        <div data-id="raty-script" class="summary lazy-script">
            <div class="summary__head">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/videodownloader.b9996289c0e2.svg" class="summary__logo" width="100" height="100" alt="product logo">
                <div class="summary__title">4K Video Downloader</div>
            </div>
            <div class="summary__info">
                <p class="summary__row" itemprop="name">Application Name: <span class="application-name">4K Video Downloader</span></p>

                <meta itemprop="image" content="https://static.4kdownload.com/main/img/redesign/videodownloader.b9996289c0e2.svg">
                <meta itemprop="url" content="https://www.4kdownload.com/products/product-videodownloader">
                <p class="summary__row">Rating:<span class="rating-stars" id="rating"></span>
                    <span class="rating-info" id="ratingtext" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
  <meta itemprop="worstRating" content="1">
  <span itemprop="ratingValue">4.4</span> (
                    <span itemprop="ratingCount">2621</span> rates)
                    <meta itemprop="bestRating" content="5">
                    </span>
                </p>
                <p class="summary__row">Price: Free</p>
                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
  <meta itemprop="url" content="https://www.4kdownload.com/products/product-videodownloader">
  <meta itemprop="price" content="0">
  <meta itemprop="priceCurrency" content="USD">
</span>
                <p class="summary__row">Current Version:
                    <span itemprop="softwareVersion">4.11</span></p>
                <p class="summary__row">Last Updated:
                    <span itemprop="dateModified" content="2020-02-04">Feb. 4, 2020</span>
                </p>

                <div>
                    <p class="summary__row">Software Category: Multimedia</p>
                    <span itemprop="applicationCategory" content="MultimediaApplication"></span>
                </div>

                <p class="summary__row">Operating System:
                    <span itemprop="operatingSystem">Windows 10</span>,
                    <span itemprop="operatingSystem">Windows 8</span>,
                    <span itemprop="operatingSystem">Windows 7</span>,
                    <span itemprop="operatingSystem">macOS 10.14</span>
                    <span itemprop="operatingSystem">macOS 10.13</span>,
                    <span itemprop="operatingSystem">Ubuntu Linux</span>
                </p>
                <div itemprop="author" itemscope itemtype="http://schema.org/Organization" style="display: none;">
                    Author:
                    <a itemprop="url" href="https://www.4kdownload.com"><span itemprop="name">Open Media LLC</span></a>
                </div>
            </div>
        </div>
        <div class="socials comments-section__socials">

            <div class="social social-line">

                <div class="social__item">
                    <div data-id="fb-social-script" class="fb-like fb_iframe_widget lazy-script" data-href="http://www.facebook.com/4kdownload" data-action="like" data-size="large" data-width="120" data-show-faces="false" data-layout="button_count"></div>
                </div>

                <div data-id="twitter-social-script" class="social__item  lazy-script">
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url="https://fkd.page.link/Anr5" data-via="4kdownload" data-count="none" data-size="large">Tweet</a>
                </div>

                <a href="//www.reddit.com/submit?url=http://www.4kdownload.com/products/product-videodownloader" onclick="window.open('http://www.reddit.com/submit?url=' + encodeURIComponent('http://www.4kdownload.com/products/product-videodownloader')); return false" class="social__item social__item--reddit"><img src="https://static.4kdownload.com/main/img/redesign/ic-reddit-square.5413227fb00d.svg" alt="Submit to reddit"></a>

            </div>

        </div>
    </div>

    <div class="comments-section__comments" data-entry="videodownloader">
        <h3 class="comments-section__heading h1">Users comments</h3>
        <div class="comments">
            <div class="comments-list__wrap js-comments-list">

                <div class="comments-thanks">
                    <a href="#" class="comments-thanks__close js-comments-thanks-close">
                        <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/ic-cross.acbcd4668eab.svg" class="icon__ic-cross" width="16" height="16" alt="close form icon">
                    </a>
                    <img class="lazy" width="134" height="156" data-src="https://static.4kdownload.com/main/img/redesign/ic-feedback.3e76c2a2e6a6.svg" class="comments-thanks__image">
                    <div class="comments-thanks__text-wrapper">
                        <h4 class="comments-thanks__title h2">Thanks for feedback</h4>
                        <p class="comments-thanks__text text text--large">Your comments will appear here shortly. Please spread the word about us in social networks.</p>

                        <div class="social social-line">

                            <div class="social__item">
                                <div data-id="fb-social-script" class="fb-like fb_iframe_widget lazy-script" data-href="http://www.facebook.com/4kdownload" data-action="like" data-size="large" data-width="120" data-show-faces="false" data-layout="button_count"></div>
                            </div>

                            <div data-id="twitter-social-script" class="social__item  lazy-script">
                                <a href="http://twitter.com/share" class="twitter-share-button" data-url="https://fkd.page.link/Anr5" data-via="4kdownload" data-count="none" data-size="large">Tweet</a>
                            </div>

                            <a href="//www.reddit.com/submit?url=http://www.4kdownload.com/products/product-videodownloader" onclick="window.open('http://www.reddit.com/submit?url=' + encodeURIComponent('http://www.4kdownload.com/products/product-videodownloader')); return false" class="social__item social__item--reddit"><img src="https://static.4kdownload.com/main/img/redesign/ic-reddit-square.5413227fb00d.svg" alt="Submit to reddit"></a>

                        </div>

                    </div>
                </div>
                <form class="comments-form js-comments-form lazy-script" data-id="comment-form-script" data-url="https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit&hl=en" role="form" action="/comments/post/" method="post">

                    <a href="#" class="comments-form__close js-comments-form-close">
                        <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/ic-cross.acbcd4668eab.svg" class="icon__ic-cross" width="16" height="16" alt="close form icon"></a>

                    <div style="display:none;">
                        <input type="hidden" name="content_type" value="main.product" id="id_content_type" />
                    </div>

                    <div style="display:none;">
                        <input type="hidden" name="object_pk" value="1" id="id_object_pk" />
                    </div>

                    <div style="display:none;">
                        <input type="hidden" name="timestamp" value="1583135572" id="id_timestamp" />
                    </div>

                    <div style="display:none;">
                        <input type="hidden" name="security_hash" value="509253b6078ae000ea4b8c0dc98764cf70350157" id="id_security_hash" />
                    </div>

                    <div class="comments-form__input comments-form__input--name form-control-wrap">
                        <input name="name" placeholder="Your name" class="comments__form-control form-control left-input" tabindex="2">
                    </div>

                    <div class="comments-form__input comments-form__input--email form-control-wrap">
                        <input id="comment-email" name="email" placeholder="Your email address" class="comments__form-control form-control right-input" tabindex="3">
                    </div>

                    <div style="display:none;">
                        <input type="url" name="url" id="id_url" />
                    </div>

                    <div class="form-control-wrap comments-form__tarea js-comments-form-tarea">
                        <textarea name="comment" placeholder="Your comment" class="comments__form-control form-control" tabindex="1"></textarea>
                    </div>

                    <div style="display:none;">
                        <input type="text" name="honeypot" id="id_honeypot" />
                    </div>

                    <div style="display:none;">
                        <input type="number" name="score" max="5" required id="id_score" min="0" />
                    </div>

                    <div class="comments-form__input comments-form__input--rating form-control-wrap">
                        <div class="comments-form__rating-text text">Your rating:</div><span id="js-comments-rating" class="comments-form__rating rating js-rating--comments rating--large"></span>
                    </div>
                    <div class="comments-form__footer">
                        <div class="comments-form__captcha">
                            <div id="g-recaptcha" class="g-recaptcha" data-sitekey="6LdYl1sUAAAAADRufi0gdLV-DI5V-tCUmRlifKSx" data-badge="bottomleft" data-callback="onSubmit" data-size="invisible"></div>
                        </div>
                        <label class="comments-form__checkbox agreement-check">
                            <input class="agreement-check__input" type="checkbox" id="comments-agreement" data-action="/api/newsletter/subscribe/">
                            <span class="agreement-check__checkmark"></span>
                            <p class="agreement-check__text comments-form__checkbox-text comments-form__checkbox-text--updates">I want to receive 4K Download news, special offers and updates.</p>
                        </label>

                        <p class="agreement-check__text comments-form__checkbox-text comments-form__checkbox-text--privacy">By clicking the <b>Send</b> button, you agree to our <a href="/privacy">Privacy Policy.</a></p>
                        <button value="Send" class="comments-form__submit btn btn--primary">
                            <span class="comments-form__load-icon"></span>
                            <span class="comments-form__submit-text">Send</span>
                        </button>
                    </div>
                </form>

                <div data-id="comment-load-script" id="comment-list" class="comments-list lazy-script">

                    <div data-comment-id=52461 class="comment" itemprop="review" itemscope itemtype="http://schema.org/Review">

                        <div class="comment__text" itemprop="reviewBody">Definitivamente es un una aplicación demasiado útil, ademas muy versátil, cómoda, ágil y rápida. Me gusta tanto que constantemente la utilizo. no omito decirles que la he recomendado a varios amigos que ya la utilizan. Muchas gracias para ponerla a disposición de nosotros, los que buscamos aplicaciones como esta.</div>
                        <div class="comment__meta">
                            <div class="comment__author" itemprop="author" itemscope itemtype="http://schema.org/Person">Walter Marin Garcia</div>

                            <span class="rating-info" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
            <meta itemprop="worstRating" content="1">
            <span class="rating-info js-rating" data-read-only="true" itemprop="ratingValue" content="5" data-score='5'></span>
                            <meta itemprop="bestRating" content="5">
                            </span>

                            <div class="comment__date">
                                <img src="https://static.4kdownload.com/main/img/redesign/ic-time.66cb7046d374.svg" class="comment__date-icon" width="11" height="11" alt="clock icon">
                                <span class="comment__date-count" data-comment-datetime="2020-02-23T17:26:20+00:00" itemprop="datePublished"></span>
                            </div>
                        </div>
                    </div>

                    <div data-comment-id=52447 class="comment" itemprop="review" itemscope itemtype="http://schema.org/Review">

                        <div class="comment__text" itemprop="reviewBody">do very much recommend this product. videos downloaded are very high quality and downloading time is very fast.</div>
                        <div class="comment__meta">
                            <div class="comment__author" itemprop="author" itemscope itemtype="http://schema.org/Person">J</div>

                            <span class="rating-info" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
            <meta itemprop="worstRating" content="1">
            <span class="rating-info js-rating" data-read-only="true" itemprop="ratingValue" content="5" data-score='5'></span>
                            <meta itemprop="bestRating" content="5">
                            </span>

                            <div class="comment__date">
                                <img src="https://static.4kdownload.com/main/img/redesign/ic-time.66cb7046d374.svg" class="comment__date-icon" width="11" height="11" alt="clock icon">
                                <span class="comment__date-count" data-comment-datetime="2020-02-22T04:53:58+00:00" itemprop="datePublished"></span>
                            </div>
                        </div>
                    </div>

                    <div data-comment-id=52439 class="comment" itemprop="review" itemscope itemtype="http://schema.org/Review">

                        <div class="comment__text" itemprop="reviewBody">Dette er et program du kan få mye utbytte fra. Det er gratis og du kan bare laste ned videoer så mye du vil. Det er enkelt og det er ikke ubehaglige annonser som kommer opp i programmet. TippTopp :D</div>
                        <div class="comment__meta">
                            <div class="comment__author" itemprop="author" itemscope itemtype="http://schema.org/Person">Adam</div>

                            <span class="rating-info" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
            <meta itemprop="worstRating" content="1">
            <span class="rating-info js-rating" data-read-only="true" itemprop="ratingValue" content="5" data-score='5'></span>
                            <meta itemprop="bestRating" content="5">
                            </span>

                            <div class="comment__date">
                                <img src="https://static.4kdownload.com/main/img/redesign/ic-time.66cb7046d374.svg" class="comment__date-icon" width="11" height="11" alt="clock icon">
                                <span class="comment__date-count" data-comment-datetime="2020-02-21T20:09:25+00:00" itemprop="datePublished"></span>
                            </div>
                        </div>
                    </div>

                    <div data-comment-id=52355 class="comment" itemprop="review" itemscope itemtype="http://schema.org/Review">

                        <div class="comment__text" itemprop="reviewBody">Excellent product, working beautifully. Keep it up :)</div>
                        <div class="comment__meta">
                            <div class="comment__author" itemprop="author" itemscope itemtype="http://schema.org/Person">Andrei</div>

                            <span class="rating-info" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
            <meta itemprop="worstRating" content="1">
            <span class="rating-info js-rating" data-read-only="true" itemprop="ratingValue" content="5" data-score='5'></span>
                            <meta itemprop="bestRating" content="5">
                            </span>

                            <div class="comment__date">
                                <img src="https://static.4kdownload.com/main/img/redesign/ic-time.66cb7046d374.svg" class="comment__date-icon" width="11" height="11" alt="clock icon">
                                <span class="comment__date-count" data-comment-datetime="2020-02-14T18:55:36+00:00" itemprop="datePublished"></span>
                            </div>
                        </div>
                    </div>

                    <div data-comment-id=52318 class="comment" itemprop="review" itemscope itemtype="http://schema.org/Review">

                        <div class="comment__text" itemprop="reviewBody">Excelente aplicación para descargas en buena calidad,felicito a los desarrolladores, gracias</div>
                        <div class="comment__meta">
                            <div class="comment__author" itemprop="author" itemscope itemtype="http://schema.org/Person">Yony Quedo Huaman</div>

                            <span class="rating-info" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
            <meta itemprop="worstRating" content="1">
            <span class="rating-info js-rating" data-read-only="true" itemprop="ratingValue" content="5" data-score='5'></span>
                            <meta itemprop="bestRating" content="5">
                            </span>

                            <div class="comment__date">
                                <img src="https://static.4kdownload.com/main/img/redesign/ic-time.66cb7046d374.svg" class="comment__date-icon" width="11" height="11" alt="clock icon">
                                <span class="comment__date-count" data-comment-datetime="2020-02-11T13:27:03+00:00" itemprop="datePublished"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <button data-id="comment-load-script" class="comments-list__more js-comments-list-more lazy-script">Show More Comments</button>
                <span data-id="raty-script" class="lazy-script"></span>

            </div>

        </div>
    </div>
</div>