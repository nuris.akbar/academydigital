<section class="section features-list">
    <h2 class="features-list__heading h1">Still not convinced? See more amazing features!</h2>
    <ul class="features-list__wrap">

        <li class="features-list__item">
            <div class="features-list__icon">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/features-icons/itunes.049b2abf1836.svg" alt="itunes">
            </div>
            <div class="features-list__content">
                <h2 class="features-list__title h2">

<a href="/howto/howto-download-youtube-to-itunes" target="_blank">Direct transfer to iTunes</a> 
</h2>
                <p class="features-list__text text">

                    <a href="/howto/howto-download-youtube-to-itunes" target="_blank">Get videos and songs downloaded directly to your iTunes library</a> and enjoy them on your iPhone, iPod or iPad anywhere you go. </p>
            </div>
        </li>

        <li class="features-list__item">
            <div class="features-list__icon">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/features-icons/popular.b19b45270919.svg" alt="popular sites">
            </div>
            <div class="features-list__content">
                <h2 class="features-list__title h2"> Support all popular video sites</h2>
                <p class="features-list__text text">

                    <a href="/howto/howto-download-vimeo-video" target="_blank">Download video and audio from Vimeo</a>, <a href="/howto/howto-download-soundcloud-music" target="_blank">SoundCloud</a>, <a href="/howto/howto-download-flickr-video" target="_blank">Flickr</a>, <a href="/howto/howto-download-facebook-video" target="_blank">Facebook</a> and <a href="/howto/howto-download-dailymotion-video-or-audio" target="_blank">DailyMotion</a>, <a href="/howto/howto-download-tumblr-video" target="_blank">Tumblr</a>, <a href="/howto/howto-download-streams-from-youtube-gaming" target="_blank">YouTube Gaming</a>. </p>
            </div>
        </li>

        <li class="features-list__item">
            <div class="features-list__icon">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/features-icons/3d.d9273b6619e3.svg" alt="3d video">
            </div>
            <div class="features-list__content">
                <h2 class="features-list__title h2">
<a href="/howto/howto-download-and-playback-3d-video" target="_blank">3D video download</a>  </h2>
                <p class="features-list__text text">
                    <a href="/howto/howto-download-and-playback-3d-video" target="_blank">Download video in 3D format</a>, you will find a small special icon among available formats after video parsing. Everyone should try watching live shows and cartoons in 3D, it’s a one of a kind experience. </p>
            </div>
        </li>

        <li class="features-list__item">
            <div class="features-list__icon">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/features-icons/360video.75ddc3848f3b.svg" alt="360 degrees video">
            </div>
            <div class="features-list__content">
                <h2 class="features-list__title h2">
<a href="/howto/howto-download-360-degrees-youtube-videos" target="_blank">360° videos download</a> </h2>
                <p class="features-list__text text">
                    Videos created with a camera that simultaneously records all 360 degrees of a scene, change the viewing angle by dragging video with the mouse and enjoy this mind blowing novelty. </p>
            </div>
        </li>

        <li class="features-list__item">
            <div class="features-list__icon">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/features-icons/smart.30aa56c9ec53.svg" alt="smart mode">
            </div>
            <div class="features-list__content">
                <h2 class="features-list__title h2">Smart Mode feature</h2>
                <p class="features-list__text text">Activate “Smart Mode”, apply preferable settings to all further downloads and get videos and songs downloaded easier and faster.</p>
            </div>
        </li>

        <li class="features-list__item">
            <div class="features-list__icon">
                <img class="lazy" data-src="https://static.4kdownload.com/main/img/redesign/features-icons/proxy.8cc761d36b27.svg" alt="proxy">
            </div>
            <div class="features-list__content">
                <h2 class="features-list__title h2">In-app proxy setup</h2>
                <p class="features-list__text text">Setup proxy servers settings right from the application to download videos that are blocked in your region.</p>
            </div>
        </li>

    </ul>
</section>