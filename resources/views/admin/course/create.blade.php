@extends('admin.layout')
@section('title','Create Course')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Horizontal Form</h3>
            </div>
            {{ Form::open(['url'=>route('adminzone.course.store'),'class'=>'form-horizontal','files'=>true])}}
            @include('validation')
            @include('admin.course.form')
            </form>
        </div>
    </div>
</section>
@endsection