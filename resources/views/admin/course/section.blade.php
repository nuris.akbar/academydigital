@extends('admin.layout')
@section('title','Manage Course')
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
          <div class="card-header">
              <a href="{{ route('adminzone.section.show',['id'=>$course->id]) }}" class="btn btn-danger"><i class="fas fa-edit"></i> Add New Section</a>
              <a href="{{ route('adminzone.lesson.show',['id'=>$course->id]) }}" class="btn btn-danger"><i class="fas fa-edit"></i> Add New Lesson</a>
          </div>
          <div class="card-body">

            <table class="table table-bordered table-striped">
                <tr>
                    <td>Title</td>
                    <td>{{ $course->title}}</td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td>{{ $course->category->name}}</td>
                </tr>
                <tr>
                    <td>Instructor</td>
                    <td>{{ $course->user->name}}</td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>{{ $course->price}}</td>
                </tr>
            </table>
            <hr>

              @include('alert')

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th width="10">No</th>
                          <th>Section Name</th>
                          <th>Lesson</th>
                          <th width="10">#</th>
                          <th width="10">#</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($sections as $section)
                      <tr>
                          <td>{{ $loop->iteration}}</td>
                          <td>{{ $section->name}}</td>
                          <td>
                             
                            @foreach($section->lessons as $lesson)
                            {{ Form::open(['url'=>route('adminzone.lesson.destroy',['id'=>$lesson->id]),'method'=>'delete'])}}
                            
                            <button type="submit" class="btn btn-info btn-sm"><i class="fas fa-trash"></i></button> 
                            <a href="{{ route('adminzone.lesson.edit',['id'=>$lesson->id]) }}">{{ strlen($lesson->title)>55?substr($lesson->title,0,55).' ...':$lesson->title }} </a>
                            <hr style="margin-top:2px;margin-bottom:2px">
                            {!! Form::close() !!}
                              
                            @endforeach
                            
                          </td>
                          <td><a href="{{ route('adminzone.section.edit',['id'=>$section->id]) }}" class="btn btn-danger btn-sm"><i class="far fa-edit"></i></a></td>
                          <td>
                              {{ Form::open(['url'=>route('adminzone.section.destroy',['id'=>$section->id]),'method'=>'delete'])}}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                              {{ Form::close()}}
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Section Name</th>
                          <th>Lesson</th>
                          <th></th>
                          <th></th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
  </div>
</section>
@endsection