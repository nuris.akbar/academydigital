@extends('admin.layout')
@section('title','Edit Course')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Horizontal Form</h3>
            </div>
            {{ Form::model($course,['url'=>route('adminzone.course.update',['id'=>$course->id]),'class'=>'form-horizontal','method'=>'PUT','files'=>true])}}
            @include('validation')
            @include('admin.course.form')
            </form>
        </div>
    </div>
</section>
@endsection