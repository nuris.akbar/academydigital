@extends('admin.layout')
@section('title','Manage Course')
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
          <div class="card-header">
             
              <a href="{{ route('adminzone.course.create') }}" class="btn btn-danger"><i class="fas fa-edit"></i> Add New Data</a>
          </div>
          <div class="card-body">
              @include('alert')

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th width="7">No</th>
                          <th>Title</th>
                          <th>User</th>
                          <th width="150">Category</th>
                          <th>Publish</th>
                          <th width="7">#</th>
                          <th width="7">#</th>
                          <th width="7">#</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($courses as $course)
                      <tr>
                          <td>{{ $loop->iteration}}</td>
                          <td>{{ $course->title}}</td>
                          <td>{{ $course->user->name}}</td>
                          <td>{{ $course->category->name}}</td>
                          <td class="text-center">{{ $course->publish==1?'Yes':'No'}}</td>
                          <td><a href="{{ route('adminzone.course.show',['id'=>$course->id]) }}" class="btn btn-danger btn-sm"><i class="far fa-play-circle"></i></a></td>
                          <td><a href="{{ route('adminzone.course.edit',['id'=>$course->id]) }}" class="btn btn-danger btn-sm"><i class="far fa-edit"></i></a></td>
                          <td>
                              {{ Form::open(['url'=>route('adminzone.course.destroy',['id'=>$course->id]),'method'=>'delete'])}}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                              {{ Form::close()}}
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Title</th>
                          <th>User</th>
                          <th>Category</th>
                          <th>Publish</th>
                          <th>#</th>
                          <th>#</th>
                          <th>#</th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
  </div>
</section>
@endsection