<div class="card-body">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10">
           {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Title'])}}
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Short Description</label>
        <div class="col-sm-10">
           {{ Form::textarea('short_description',null,['class'=>'form-control','placeholder'=>'Description'])}}
        </div>
    </div>
    
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
           {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Description'])}}
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Publish / Category</label>
        <div class="col-sm-2">
           {{ Form::select('publish',[1=>'Yes',2=>'No'],null,['class'=>'form-control'])}}
        </div>
        <div class="col-sm-4">
            {!! Form::select('category_id', $categories, null,['class' => 'form-control']) !!}
         </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Trainer & Video URL</label>
        <div class="col-sm-2">
           {!! Form::select('user_id', $users, null,['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-8">
            {{ Form::text('video_preview_url',null,['class'=>'form-control','placeholder'=>'Video Priview URL'])}}
         </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Price</label>
        <div class="col-sm-3">
           {{ Form::text('price',null,['class'=>'form-control','placeholder'=>'Price'])}}
        </div>
        <div class="col-sm-3">
            {{ Form::text('sale_price',null,['class'=>'form-control','placeholder'=>'Sale Price '])}}
         </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Note</label>
        <div class="col-sm-10">
           {{ Form::text('note',null,['class'=>'form-control','placeholder'=>'Note For This Course'])}}
        </div>
    </div>

</div>

<div class="card-footer">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Save Data</button>
            
            <a href="{{ route('adminzone.course.index') }}" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
        </div>
    </div>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>
@endpush