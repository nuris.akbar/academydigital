

<ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link {{Request::segment(4)==''?'active':''}}" href="/adminzone/user/{{ Request::segment(3) }}">Profile</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Course</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{Request::segment(4)=='follow'?'active':''}}" href="/adminzone/user/{{ Request::segment(3) }}/follow">Follow Course</a>
    </li>
    <li class="nav-item">
    <a class="nav-link {{Request::segment(4)=='orders'?'active':''}}" href="/adminzone/user/{{ Request::segment(3) }}/orders">Transaction</a>
    </li>
  </ul>