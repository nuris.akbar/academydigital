@extends('admin.layout')
@section('title','Manage '.$title)
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
          <div class="card-header">
             
              <a href="{{ route('adminzone.user.create') }}" class="btn btn-danger"><i class="fas fa-edit"></i> Add New Data</a>
          </div>
          <div class="card-body">
              @include('alert')

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th width="10"></th>
                          <th>Full Name</th>
                          <th>Email</th>
                          @if($title!='Member')
                          <th width="100">Role</th>
                          @endif
                          <th width="10">#</th>
                          <th width="10">#</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($users as $user)
                      <tr>
                          <td># {{ $loop->iteration}}</td>
                          <td>{{ $user->name}}</td>
                          <td>{{ $user->email}}</td>
                          @if($title!='Member')
                            <td width="100">{{ $user->role}}</td>
                          @endif
                          <td><a href="{{ route('adminzone.user.edit',['id'=>$user->id]) }}" class="btn btn-danger btn-sm"><i class="far fa-eye"></i></a></td>
                          <td>
                              {{ Form::open(['url'=>route('adminzone.user.destroy',['id'=>$user->id]),'method'=>'delete'])}}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                              {{ Form::close()}}
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th></th>
                          <th></th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
  </div>
</section>
@endsection