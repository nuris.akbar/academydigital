@extends('admin.layout')
@section('title','Follow Course')
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
        @include('admin.user.tab')

          <div class="card-body">
              @include('alert')

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th width="7">No</th>
                          <th>Title</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($follows as $follow)
                      <tr>
                          <td>{{ $loop->iteration}}</td>
                          <td>{{ $follow->course->title}}</td>
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Title</th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
  </div>
</section>
@endsection