@extends('admin.layout')
@section('title','Orders History From '.$user->name)
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
        @include('admin.user.tab')
          <div class="card-body">
              @include('alert')

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th width="10"></th>
                          <th>Invoice Number</th>
                          <th>Payment To</th>
                          <th>Created At</th>
                          <th>Total</th>
                          <th>Payment Status</th>
                          <th width="10">#</th>
                          {{-- <th width="10">#</th> --}}
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($orders as $order)
                      <tr>
                          <td># {{ $loop->iteration}}</td>
                          <td>{{ $order->invoice}}</td>
                          <td>{{ $order->account_number}} {{ $order->bank_name}}</td>
                          <td>{{ $order->created_at}}</td>
                          <td>{{ $order->total}}</td>
                          <td>{{ $order->payment_status}}</td>
                          <td><a href="{{ route('adminzone.order.show',['id'=>$order->invoice]) }}" class="btn btn-danger btn-sm"><i class="far fa-eye"></i></a></td>
                          {{-- <td>
                              {{ Form::open(['url'=>route('adminzone.order.destroy',['id'=>$order->id]),'method'=>'delete'])}}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                              {{ Form::close()}}
                          </td> --}}
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th></th>
                          <th></th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
  </div>
</section>
@endsection