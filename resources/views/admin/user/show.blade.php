@extends('admin.layout')
@section('title','Profile '.$user->name)
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
        @include('admin.user.tab')
          <div class="card-body">
              @include('alert')
          </div>
          <table class="table table-bordered">
              <tr>
                  <td></td>
              </tr>
          </table>
      </div>
  </div>
</section>
@endsection