      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="/adminzone/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard<span class="right badge badge-danger">New</span></p>
            </a>
          </li>
          

          <li class="nav-item">
            <a href="{{ route('adminzone.user.member') }}" class="nav-link">
              <i class="nav-icon fas fa-address-card"></i>
              <p>Manage Member</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('adminzone.user.member') }}" class="nav-link">
              <i class="nav-icon fab fa-discourse"></i>
              <p>Manage Discussion</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('adminzone.order.index') }}" class="nav-link">
              <i class="nav-icon fas fa-money-check-alt"></i>
              <p>Report Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('adminzone.discount.index') }}" class="nav-link">
              <i class="nav-icon fa fa-tags"></i>
              <p>Manage Discount</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('adminzone.category.index') }}" class="nav-link">
              <i class="nav-icon fas fa-cube"></i>
              <p>Module Category</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('adminzone.course.index') }}" class="nav-link">
              <i class="nav-icon fab fa-youtube"></i>
              <p>Module Course</p>
            </a>
          </li>
          
          

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
               Data Master
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('adminzone.user.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>Manage Administrator</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sub Menu 2</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('adminzone.logout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->