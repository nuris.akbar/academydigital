@extends('admin.layout')
@section('title','Edit Category')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Horizontal Form</h3>
            </div>
            {{ Form::model($category,['url'=>route('adminzone.category.update',['id'=>$category->id]),'class'=>'form-horizontal','method'=>'PUT'])}}
            @include('validation')
            @include('admin.category.form')
            </form>
        </div>
    </div>
</section>
@endsection