@extends('admin.layout')
@section('title','Edit Lesson')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Edit Lesson </h3>
            </div>
            {{ Form::model($lesson,['url'=>route('adminzone.lesson.update',['id'=>$lesson->id]),'class'=>'form-horizontal','method'=>'PUT'])}}
            @include('validation')
            @include('admin.lesson.form')
            </form>
        </div>
    </div>
</section>
@endsection