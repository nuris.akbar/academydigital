<div class="card-body">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Course Name</label>
        <div class="col-sm-10">
           {{ Form::text('course_id',$course->title,['class'=>'form-control','disabled'=>'disabled'])}}
           {{ Form::hidden('course_id',$course->id)}}
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Section Name</label>
        <div class="col-sm-10">
           {{ Form::select('section_id',$sections,null,['class'=>'form-control'])}}
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Lesson Title</label>
        <div class="col-sm-10">
           {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Lesson Title'])}}
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Public / URL / Duration</label>
        <div class="col-sm-2">
           {{ Form::select('public',[0=>'Private',2=>'Public'],null,['class'=>'form-control'])}}
        </div>
        <div class="col-sm-7">
            {{ Form::text('video_url',null,['class'=>'form-control','placeholder'=>'Video URL'])}}
         </div>
        <div class="col-sm-1">
            {{ Form::text('duration',null,['class'=>'form-control','placeholder'=>'04:50'])}}
         </div>

    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
           {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Description'])}}
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Save Data</button>
            <a href="{{ route('adminzone.course.show',['id'=>$course->id]) }}" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
            @if(isset($lesson))
            {{ Form::open(['url'=>route('adminzone.lesson.destroy',['id'=>$lesson->id]),'method'=>'delete'])}}
            <button type="submit" class="btn btn-info"><i class="fas fa-trash"></i> Delete Lesson</button>
            {!! Form::close() !!}
            @endif
        </div>
    </div>
    
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>
@endpush