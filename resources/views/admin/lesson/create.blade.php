@extends('admin.layout')
@section('title','Create Lesson')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create Lesson </h3>
            </div>
            {{ Form::open(['url'=>route('adminzone.lesson.store'),'class'=>'form-horizontal'])}}
            @include('validation')
            @include('admin.lesson.form')
            </form>
        </div>
    </div>
</section>
@endsection