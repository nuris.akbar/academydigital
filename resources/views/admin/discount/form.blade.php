<div class="card-body">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10">
           {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Discount Title'])}}
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Discount Code</label>
        <div class="col-sm-3">
           {{ Form::text('discount_code',null,['class'=>'form-control','placeholder'=>'Discount Code'])}}
        </div>
        <div class="col-sm-2">
            {{ Form::text('value',null,['class'=>'form-control','placeholder'=>'Value'])}}
         </div>
         <div class="col-sm-2">
            {{ Form::select('type',['fix'=>'Fix Value','percen'=>'Percen %'],null,['class'=>'form-control'])}}
         </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Periode</label>
        <div class="col-sm-3">
           {{ Form::date('date_start',null,['class'=>'form-control','placeholder'=>'Periode Start'])}}
        </div>
        <div class="col-sm-3">
            {{ Form::date('date_end',null,['class'=>'form-control','placeholder'=>'Periode Start'])}}
         </div>
    </div>
</div>
<div class="card-footer">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Save Data</button>
            
            <a href="{{ route('adminzone.category.index') }}" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
        </div>
    </div>
</div>