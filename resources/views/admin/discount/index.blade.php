@extends('admin.layout')
@section('title','Manage Discount')
@section('content')
<section class="content">
  <div class="container-fluid">
      <div class="card">
          <div class="card-header">
             
              <a href="{{ route('adminzone.discount.create') }}" class="btn btn-danger"><i class="fas fa-edit"></i> Add New Data</a>
          </div>
          <div class="card-body">
              @include('alert')

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th width="10"></th>
                          <th>Title</th>
                          <th>Discount Code</th>
                          <th width="170">Periode</th>
                          <th width="50">Value</th>
                          <th width="100">Type</th>
                          <th width="10">#</th>
                          <th width="10">#</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($discounts as $discount)
                      <tr>
                          <td># {{ $loop->iteration}}</td>
                          <td>{{ $discount->title}}</td>
                          <td>{{ $discount->discount_code}}</td>
                          <td>{{ date_format(date_create($discount->date_start),"d M Y")}} -  {{ date_format(date_create($discount->date_end),"d M Y") }}</td>
                          <td>{{ $discount->value}} {{ $discount->type=='fix'?'':'%'}}</td>
                          <td>{{ $discount->type=='fix'?'Fix Price':'Percent Discount'}}</td>
                          <td><a href="{{ route('adminzone.discount.edit',['id'=>$discount->id]) }}" class="btn btn-danger btn-sm"><i class="far fa-edit"></i></a></td>
                          <td>
                              {{ Form::open(['url'=>route('adminzone.discount.destroy',['id'=>$discount->id]),'method'=>'delete'])}}
                              <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                              {{ Form::close()}}
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                        <th width="10"></th>
                        <th>Title</th>
                        <th>Discount Code</th>
                        <th>Periode</th>
                        <th>Value</th>
                        <th>Type</th>
                        <th width="10">#</th>
                        <th width="10">#</th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
  </div>
</section>
@endsection