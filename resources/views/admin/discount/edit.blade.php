@extends('admin.layout')
@section('title','Edit Discount')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Horizontal Form</h3>
            </div>
            {{ Form::model($discount,['url'=>route('adminzone.discount.update',['id'=>$discount->id]),'class'=>'form-horizontal','method'=>'PUT'])}}
            @include('validation')
            @include('admin.discount.form')
            </form>
        </div>
    </div>
</section>
@endsection