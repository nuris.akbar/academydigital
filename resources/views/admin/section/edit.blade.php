@extends('admin.layout')
@section('title','Update Section Course')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Horizontal Form</h3>
            </div>
            {{ Form::model($section,['url'=>route('adminzone.section.update',['id'=>$section->id]),'class'=>'form-horizontal','method'=>'PUT'])}}
            @include('validation')
            @include('admin.section.form')
            </form>
        </div>
    </div>
</section>
@endsection