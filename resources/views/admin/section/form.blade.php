<div class="card-body">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Course</label>
        <div class="col-sm-10">
           {{ Form::text('course_id',$course->title,['class'=>'form-control','disabled'=>'disabled'])}}
           {{ Form::hidden('course_id',$course->id)}}
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Section Name</label>
        <div class="col-sm-10">
           {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Section Title'])}}
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
           {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Section Description'])}}
        </div>
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Save Data</button>
    <a href="{{ route('adminzone.course.index') }}" class="btn btn-danger"><i class="fas fa-backward"></i> Back</a>
</div>
