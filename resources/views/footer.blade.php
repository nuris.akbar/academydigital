<footer class="footer">
    <div class="container footer__container clearfix">
        <div class="row">
            <div class="col-sm-6 col-md-2 dc-u-mb-48">
                <h3 class="footer__title chapeau-title">Halaman Penting</h3>
                <ul class="footer__list">
                    <li><a class="ds-snowplow-link-footer-courses" href="">Cara Pembayaran</a></li>
                    <li><a class="ds-snowplow-link-footer-skill-tracks" href="">Layanan Konsultasi</a></li>
                    <li><a class="ds-snowplow-link-footer-career-tracks" href="">Career Tracks</a></li>
                    <li><a class="ds-snowplow-link-footer-pricing" href="">Pricing</a></li>
                    <li><a class="ds-snowplow-link-footer-become-an-instructor" href="">Become an Instructor</a></li>
                </ul>
            </div>

            <div class="col-sm-6 col-md-2 dc-u-mb-48">
                <h3 class="footer__title chapeau-title">Resources</h3>
                <ul class="footer__list">
                    <li><a class="ds-snowplow-link-footer-resource-center" href="">Resource Center</a></li>
                    <li><a class="ds-snowplow-link-footer-community" href="">Community</a></li>
                    <li><a class="ds-snowplow-link-footer-r-documentation" target="_blank" href="">RDocumentation</a></li>
                    <li><a class="ds-snowplow-link-footer-course-editor" href="">Course Editor</a></li>
                    <li><a class="ds-snowplow-link-footer-upcoming-courses" target="_blank" href="">Upcoming Courses</a></li>
                    <li><a class="ds-snowplow-link-footer-support" target="_blank" href="">Support</a></li>
                </ul>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-sm-6 col-md-2 dc-u-mb-48">
                <h3 class="footer__title chapeau-title">Plans</h3>
                <ul class="footer__list">
                    <li><a class="ds-snowplow-link-footer-for-business" href="/groups/business">For Business</a></li>
                    <li><a class="ds-snowplow-link-footer-for-classroom" href="/groups/education">For Classrooms</a></li>
                </ul>
            </div>

            <div class="col-sm-6 col-md-2 dc-u-mb-48">
                <h3 class="footer__title chapeau-title">About</h3>
                <ul class="footer__list">
                    <li><a class="ds-snowplow-link-footer-stories" href="/stories">Stories</a></li>
                    <li><a class="ds-snowplow-link-footer-careers" href="/careers">Careers</a></li>
                    <li><a class="ds-snowplow-link-footer-contact" href="/contact-us">Contact</a></li>
                    <li><a class="ds-snowplow-link-footer-press" href="/press/">Press</a></li>
                    <li><a class="ds-snowplow-footer-about-partner" href="mailto:partnerships@datacamp.com?subject=Become%20a%20Partner">Become a Partner</a></li>
                    <li><a class="ds-snowplow-link-footer-privacy-policy" href="/privacy-policy">Privacy Policy</a></li>
                    <li><a class="ds-snowplow-link-footer-terms-of-use" href="/terms-of-use/">Terms of Use</a></li>
                </ul>
            </div>

            <div class="col-sm-12 col-md-4">
                <div class="logo-block">
                    <img class="logo-block__img" src="image/logo.png" alt="Logo full filled white" />
                </div>

                <p style="margin-top: 1em;">
                    Academy Digital menawarkan kursus PHP, SQL dan Android. Belajar dari tim guru ahli dalam bidangnya dengan pembelajaran berupa video.
                </p>

                <a class="footer__learn-more ds-snowplow-link-footer-about" href="/about/">About the company</a>

                <ul class="footer__social clearfix">
                    <li>
                        <a target="_blank" class="footer__social-link ds-snowplow-link-footer-facebook" href="https://www.facebook.com/Academy-Digital-100813088221009">
                            <span class="dc-icon dc-icon--size-18 dc-icon--white dc-icon--hover-primary-dark">
                                <svg class="dc-icon__svg"><use xlink:href="#facebook" /></svg>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a target="_blank" class="footer__social-link ds-snowplow-link-footer-twitter" href="https://twitter.com/datacamp">
                            <span class="dc-icon dc-icon--size-18 dc-icon--white dc-icon--hover-primary-dark">
                                <svg class="dc-icon__svg"><use xlink:href="#twitter" /></svg>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a target="_blank" class="footer__social-link ds-snowplow-link-footer-linked-in" href="https://www.linkedin.com/company/datamind-org">
                            <span class="dc-icon dc-icon--size-18 dc-icon--white dc-icon--hover-primary-dark">
                                <svg class="dc-icon__svg"><use xlink:href="#linkedin" /></svg>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" class="footer__social-link ds-snowplow-link-footer-youtube" href="https://www.youtube.com/channel/UCcFoOygCS7qTS_xV3-yqg0w">
                            <span class="dc-icon dc-icon--size-18 dc-icon--white dc-icon--hover-primary-dark">
                                <svg class="dc-icon__svg"><use xlink:href="#youtube" /></svg>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="copyright">
    <div class="container">
        <small>&copy; 2020 Academy Digital Inc.</small>
    </div>
</div>