@extends('layout')
@section('title','List Course Buy')
@section('content')
@foreach($items as $item)
    <p>{{ $item->name }}</p>
    <p>{{ $item->price }} | {{ Form::open(['url'=>'cart/'.$item->id,'method'=>'DELETE'])}} <button type="submit">Hapus</button></form></p>
    <hr>
@endforeach
<hr><hr>
Total : {{ $total }}

{{ Form::open(['url'=>'cart/checkout'])}}
Pilih Rekening : {{ Form::select('account_number_id',$accountNumber,null)}}<br>
<br>
{{ Form::text('discount_code',null,['placeholder'=>'Kode Diskon'])}}
<br>
Catatan : {{ Form::text('note',null)}}
{{ Form::submit('bayar')}}
{{ Form::close()}}
@endsection