<!DOCTYPE html>
<html>
<head>
	<title></title>

	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
	<div id="app">
		<app></app>
	</div>

	<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>