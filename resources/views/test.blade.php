<html>
    <head>
        <title>Kelas Online Membangun Aplikasi</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css')}}">
        <link rel="stylesheet" href="{{ asset('style.css')}}">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <style>

        </style>
    </head>
    <body>
      <div id="nav" style="height:150px;"></div>
      <div id="breadcrumbs">
        <div class="container-fluid">
          LMS Sites > Udemy Affiliate & Course Importer Demo > Development > Web Development > Complete WordPress Development Themes and Plugins Course
        </div>
      </div>

      <div id="head">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="start" style="color: #fff;margin-bottom: 20px;margin-top:20px;"> <i class="fas fa-star review-star"></i><i class="fas fa-star review-star"></i><i class="fas fa-star review-star"></i><i class="fas fa-star review-star"></i> 4.7 From 722 Review Students</div>
              <h2>Kelas Fullstack Android Java Development</h2>
                <p>Pelajari Cara Membangun Aplikasi Android Java Dan Jadilah Fullstack Android Developer.</p>
                <p>Instructor :  Nuris Akbar</p>
            </div>
            <div class="col-md-4">
              <iframe width="100%" height="315" src="https://www.youtube.com/embed/sufrcnOwCOo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>

      <div id="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-7 offset-1">
              <div class="row">
                <div class="col-md-12" style="background-color: #ffff;padding:20px;margin-bottom:20px;margin-top:20px;">
                  <div class="row">
                    <?php for($i=1;$i<=6;$i++){?>
                    <div class="col-md-6"><i class="far fa-check-circle fa-2x" style="color: red"></i> PHP for WordPress - The Loop, Conditionals, Hooks and More!</div>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-12"  style="background-color: #ffff;padding:20px;margin-bottom:20px;margin-top:10px;">
                  <p class="course-description">
                    WordPress is the leading Content Management System on the market, powering a large percentage of the Web.  The need for WordPress Developers who can build and customize themes and plugins is ever growing.  Learn from one of the most recognized educators in the WordPress world, Zac Gordon, who has taught thousands of people now employed as WordPress Developers.
                    <br>
                    If you want to learn everything from customizing existing themes, building custom themes or starting to build plugins, this course is for you.  You will learn in depth how WordPress works under the hood, from template files and tags to hooks and internal APIs.  If you are looking to build bigger and more custom projects with WordPress or just get a good job with a great company building WordPress projects, then this course is for you.  Make sure though you can already build and style a basic web page with HTML and CSS as we assume you already know this and focus more on learning PHP.
                    <br>
                    When you learn the skills this course contains you will feel incredibly empowered to build almost anything you can imagine with WordPress.  You should also feel confident working professionally in the field as a WordPress Developer.  You will have built a theme and plugin along with the course as well as a theme and plugin of your own.  Follow in the path of thousands of others of Zac’s students who learned WordPress Development and went on to do great work in the field.</p>
                    <hr>
                    <h5 style="font-family: Open Sans;">Section 1 : Membangun Aplikasi Android</h5>  
                    <ol>
                      <li class="lesson" style="padding-right:20px;"> <i class="fas fa-play-circle"></i> Sample Text</li>
                      <li class="lesson" style="padding-right:20px;"> <i class="fas fa-play-circle"></i> Sample Text</li>
                      <li class="lesson" style="padding-right:20px;"> <i class="fas fa-play-circle"></i> Sample Text</li>
                    </ol>              
                </div>
              </div>
            </div>
            <div class="col-md-3 offset-1" style="background-color: #ffff;padding:20px;margin-top:20px;font-family: Open Sans;font-weight: bold;">
              <p style="font-size:36px;">Rp. 200.000</p>
              <button type="submit" class="btn btn-success btn-lg" style="padding:20px;">Gabung Dikelas Sekarang</button>
              <hr>
              <h3>Termasuk</h3>
              <p style="vertical-align: top;"><i class="fab fa-youtube fa-2x" style="color: red"></i> 26 Video 720 HD</p>
              <p style="vertical-align: top;"><i class="fab fa-youtube fa-2x" style="color: red"></i> Akses Materi Selamanya</p>
              <p style="vertical-align: top;"><i class="fab fa-youtube fa-2x" style="color: red"></i> Sertifikat Kelulusan</p>
            </div>
          </div>
        </div>
      </div>

      <div id="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <h3>About</h3>
              <p>Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel, rutrum eu ipsum. Mauris accumsan eros eget libero posuere vulputate.</p>
            </div>
            <div class="col-md-3">
              <h3>Contact</h3>
              <p> USA, Callifornia 20, First Avenue, Callifornia</p>
              <p>Tel.: +1 212 458 300 32
              <br>Fax: +1 212 375 24 14</p>
              <p>info@masterstudy.com</p> 
            </div>
            <div class="col-md-3">Sample text</div>
            <div class="col-md-3">Sample text</div>
          </div>
        </div>
      </div>
    </body>
</html>