<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to the site {{$order->user->name}}</h2>
<br/>
Hai {{$order->user->name}}, Pembelian Dengan No Invoice {{ $order->invoice }} Sudah Terkonfirmasi Dan Anda Bisa Akses Kelas Anda Pada Link Dibawah Ini :
<br/>
<a href="/">Belajar Sekarang</a>
</body>

</html>