<!DOCTYPE html>
<html>
<head>
    <title>Konfirmasi Pemesanan</title>
</head>

<body>
<h2>Konfirmasi Pembelian</h2>
<br/>
Hai {{ $order->user->name }}, Anda Telah Melakukan Pembelian Untuk Kelas Online  : <Br>
    <ul>
        @foreach($order->orderDetails as $item)
            <li>{{ $item->course->title}} | {{ $item->course->price}}</li>
        @endforeach
    </ul>
Silahkan Lakukan Pembayaran Ke Rekening {{ $order->accountNumber->bank_name}} Atas Nama {{ $order->accountNumber->account_name}} Sejumlah {{ $order->total}}
</body>

</html>