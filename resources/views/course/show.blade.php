@extends('layout3')
@section('title',$course->title)
@section('content')
<section class="course__main">
  <div class="container">
    <div class="row">
      <!-- Main content -->
      <div style="max-width: 1000px;" class="dc-u-m-auto">
        <div class="container dc-u-mh-0 dc-u-w-100pc" style="margin-bottom: 44px;">
          <h3 class="dc-u-color-grey-oslo dc-u-ta-center dc-u-mb-12 dc-u-mt-4">Dicintai oleh pelajar di ribuan perusahaan papan atas :</h3>
          <div>
            <div class="col-xs-6 col-sm-4 col-md-2">
              <div class="dc-u-w-100pc dc-u-h-64 dc-u-mv-16 dc-u-fx dc-u-fx-aic">
                <img class="dc-u-maxw-100pc dc-u-h-auto dc-u-mh-auto dc-u-d-b" alt="lego-grey.svg" src="\image/brainmatics.png" />
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
              <div class="dc-u-w-100pc dc-u-h-64 dc-u-mv-16 dc-u-fx dc-u-fx-aic">
                <img class="dc-u-maxw-100pc dc-u-h-auto dc-u-mh-auto dc-u-d-b" alt="intel-grey.svg" src="\image/pondokIT.png" />
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
              <div class="dc-u-w-100pc dc-u-h-64 dc-u-mv-16 dc-u-fx dc-u-fx-aic">
                <img class="dc-u-maxw-100pc dc-u-h-auto dc-u-mh-auto dc-u-d-b" alt="credit-suisse-grey.svg" src="\image/pondokit1.png" />
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
              <div class="dc-u-w-100pc dc-u-h-64 dc-u-mv-16 dc-u-fx dc-u-fx-aic">
                <img class="dc-u-maxw-100pc dc-u-h-auto dc-u-mh-auto dc-u-d-b" alt="whole-foods-grey.svg" src="\image/belajarphp.png" />
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
              <div class="dc-u-w-100pc dc-u-h-64 dc-u-mv-16 dc-u-fx dc-u-fx-aic">
                <img class="dc-u-maxw-100pc dc-u-h-auto dc-u-mh-auto dc-u-d-b" alt="ea-grey.svg" src="\image/codepolitan.png" />
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
              <div class="dc-u-w-100pc dc-u-h-64 dc-u-mv-16 dc-u-fx dc-u-fx-aic">
                <img class="dc-u-maxw-100pc dc-u-h-auto dc-u-mh-auto dc-u-d-b" alt="forrester-grey.svg" src="\image/file.png" />
              </div>
            </div>
          </div>
        </div>
        <div class="dc-u-fx dc-u-fx-aic">
          <div class="dc-u-p-none dc-u-fx-fxg-1">
            <h3 class="course__description-title dc-u-fs-h2">Course Description</h3>
            <p class="course__description">{!! $course->description !!}</p>
          </div>
          <div class="hidden-xs hidden-sm dc-u-p-none dc-u-ml-48">
            <div class="header-hero__image-wrapper">
              <img class="header-hero__image" alt="Introduction to Deep Learning in Python" src="https://assets.datacamp.com/production/course_1975/shields/original/shield_image_course_1975_20200218-1-evtdts?1582014981" />
            </div>
          </div>
        </div>
        <div class="course__body dc-u-m-auto">
          <ol class="chapters">
            <div class="row">
              @foreach($sections as $section)
              <div class="col-md-6">
            <div class="chapter-column dc-u-fxi-fb-50pc js-chapter-column--even">
              <li style="height: 17em" class="chapter dc-u-bgc-white dc-card--shadowed-sm dc-u-b-none dc-u-fx dc-u-fx-fdc dc-u-pl-0 dc-u-pr-0 dc-u-pb-0">
                <!-- Header of the chapter -->
                <div class="chapter__header dc-u-pr-24 dc-u-pl-24">
                  <div class="chapter__title-wrapper">
                    <span class="chapter__number">
                      <span class="chapter-number locked">
                        <span class="
                          dc-icon 
                          dc-icon--size-12
                          dc-icon--currentColor
                          ">
                          <svg class="dc-icon__svg">
                            <use xlink:href="#lock" />
                          </svg>
                        </span>
                      </span>
                    </span>
                    <h4 class="chapter__title">
                      {{ $section->name }}
                    </h4>
                  </div>
                </div>
                <p class="chapter__description dc-u-fxi-fg-1 dc-u-pr-24 dc-u-pl-24 dc-u-mb-18">
                    {{ $section->description }} 
                </p>
                <!-- !Header of the chapter -->
                <!-- Body of the chapter -->
                
                <ul class="chapter__exercises hidden dc-u-ta-center">
                  @foreach($section->lessons as $lesson)
                  <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                    <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=1">
                      <span class="chapter__exercise-icon exercise-icon locked">
                      <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                      </span>
                        <h5 class="chapter__exercise-title" title='{{ $lesson->title}}'>{{ $lesson->title}}</h5>
                      <span class="chapter__exercise-xp">
                      {{ $lesson->duration }}
                      </span>
                    </a>
                  </li>
                  @endforeach
                  
                  
                </ul>
          
                <!-- !Body of the chapter -->
                <!-- Footer of the chapter -->
                <div class="dc-u-b dc-u-bb-none dc-u-br-none dc-u-bl-none dc-u-fx dc-u-pv-16 dc-u-ph-24 chapter__footer--experiment">
                  <a class="chapter__footer-btn-link js-toggle-chapter-details--experiment chapter__footer-btn-link-view dc-u-fx-jcc dc-u-fw-regular dc-u-ml-0"
                    href="" data-toggle-details chevron-direction="down">
                  View Chapter Details
                <a class="chapter__footer-btn dc-btn dc-btn--secondary" data-url="/sign_in_or_up_modal?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5366%2Fcontinue" href="/play/{{ $course->slug }}">Play Chapter Now</a>
                  </a>
                </div>
                <!-- !Footer of the chapter -->
              </li>
            </div>
            

          </div>
          @endforeach
        </div>
          </ol>
          <ol class="chapters chapters--single-column">
            <li class="chapter dc-u-bgc-white dc-card--shadowed-sm dc-u-b-none dc-u-fx dc-u-fx-fdc dc-u-pl-0 dc-u-pr-0 dc-u-pb-0">
              <!-- Header of the chapter -->
              <div class="chapter__header dc-u-pr-24 dc-u-pl-24">
                <div class="chapter__title-wrapper">
                  <span class="chapter__number">
                  <span class="chapter-number">1</span>
                  </span>
                  <h4 class="chapter__title">
                    Basics of deep learning and neural networks
                  </h4>
                  <span class="chapter__price">
                  Free
                  </span>
                </div>
              </div>
              <p class="chapter__description dc-u-fxi-fg-1 dc-u-pr-24 dc-u-pl-24 dc-u-mb-18">
                In this chapter, you'll become familiar with the fundamental concepts and terminology used in deep learning, and understand why deep learning techniques are so powerful today. You'll build simple neural networks and generate predictions with them.
              </p>
              <!-- !Header of the chapter -->
              <!-- Body of the chapter -->
              <ul class="chapter__exercises hidden dc-u-ta-center">
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=1">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Introduction to  deep learning'>Introduction to  deep learning</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=2">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Comparing neural network models to classical regression models'>Comparing neural network models to classical regression models</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=3">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Forward  propagation'>Forward  propagation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=4">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Coding the forward propagation algorithm'>Coding the forward propagation algorithm</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=5">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Activation  functions'>Activation  functions</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=6">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='The Rectified Linear Activation Function'>The Rectified Linear Activation Function</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=7">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Applying the network to many observations/rows of data'>Applying the network to many observations/rows of data</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=8">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Deeper networks'>Deeper networks</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=9">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Forward propagation in a deeper network'>Forward propagation in a deeper network</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=10">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Multi-layer neural networks'>Multi-layer neural networks</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=11">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Representations are learned'>Representations are learned</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/basics-of-deep-learning-and-neural-networks?ex=12">
                    <span class="chapter__exercise-icon exercise-icon ">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Levels of representation'>Levels of representation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
              </ul>
              <!-- !Body of the chapter -->
              <!-- Footer of the chapter -->
              <div class="dc-u-b dc-u-bb-none dc-u-br-none dc-u-bl-none dc-u-fx dc-u-pv-16 dc-u-ph-24 chapter__footer--experiment">
                <a class="chapter__footer-btn-link js-toggle-chapter-details--experiment chapter__footer-btn-link-view dc-u-fx-jcc dc-u-fw-regular dc-u-ml-0"
                  href="" data-toggle-details chevron-direction="down">
                View Chapter Details
                <a class="chapter__footer-btn dc-btn dc-btn--secondary" data-url="/sign_in_or_up_modal?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5362%2Fcontinue" href="/users/sign_up?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5362%2Fcontinue">Play Chapter Now</a>
                </a>
              </div>
              <!-- !Footer of the chapter -->
            </li>
            <li class="chapter dc-u-bgc-white dc-card--shadowed-sm dc-u-b-none dc-u-fx dc-u-fx-fdc dc-u-pl-0 dc-u-pr-0 dc-u-pb-0">
              <!-- Header of the chapter -->
              <div class="chapter__header dc-u-pr-24 dc-u-pl-24">
                <div class="chapter__title-wrapper">
                  <span class="chapter__number">
                    <span class="chapter-number locked">
                      <span class="
                        dc-icon 
                        dc-icon--size-12
                        dc-icon--currentColor
                        ">
                        <svg class="dc-icon__svg">
                          <use xlink:href="#lock" />
                        </svg>
                      </span>
                    </span>
                  </span>
                  <h4 class="chapter__title">
                    Optimizing a neural network with backward propagation
                  </h4>
                </div>
              </div>
              <p class="chapter__description dc-u-fxi-fg-1 dc-u-pr-24 dc-u-pl-24 dc-u-mb-18">
                Learn how to optimize the predictions generated by your neural networks. You'll use a method called backward propagation, which is one of the most important techniques in deep learning. Understanding how it works will give you a strong foundation to build on in the second half of the course.
              </p>
              <!-- !Header of the chapter -->
              <!-- Body of the chapter -->
              <ul class="chapter__exercises hidden dc-u-ta-center">
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=1">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='The need  for optimization'>The need  for optimization</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=2">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Calculating model errors'>Calculating model errors</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=3">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Understanding how weights change model accuracy'>Understanding how weights change model accuracy</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=4">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Coding how weight changes affect accuracy'>Coding how weight changes affect accuracy</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=5">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Scaling up to multiple data points'>Scaling up to multiple data points</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=6">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Gradient descent'>Gradient descent</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=7">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Calculating slopes'>Calculating slopes</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=8">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Improving model weights'>Improving model weights</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=9">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Making multiple updates to weights'>Making multiple updates to weights</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=10">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Backpropagation'>Backpropagation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=11">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='The relationship between forward and backward propagation'>The relationship between forward and backward propagation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=12">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Thinking about backward propagation'>Thinking about backward propagation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=13">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Backpropagation in practice'>Backpropagation in practice</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/optimizing-a-neural-network-with-backward-propagation?ex=14">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='A round of backpropagation'>A round of backpropagation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
              </ul>
              <!-- !Body of the chapter -->
              <!-- Footer of the chapter -->
              <div class="dc-u-b dc-u-bb-none dc-u-br-none dc-u-bl-none dc-u-fx dc-u-pv-16 dc-u-ph-24 chapter__footer--experiment">
                <a class="chapter__footer-btn-link js-toggle-chapter-details--experiment chapter__footer-btn-link-view dc-u-fx-jcc dc-u-fw-regular dc-u-ml-0"
                  href="" data-toggle-details chevron-direction="down">
                View Chapter Details
                <a class="chapter__footer-btn dc-btn dc-btn--secondary" data-url="/sign_in_or_up_modal?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5363%2Fcontinue" href="/users/sign_up?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5363%2Fcontinue">Play Chapter Now</a>
                </a>
              </div>
              <!-- !Footer of the chapter -->
            </li>
            <li class="chapter dc-u-bgc-white dc-card--shadowed-sm dc-u-b-none dc-u-fx dc-u-fx-fdc dc-u-pl-0 dc-u-pr-0 dc-u-pb-0">
              <!-- Header of the chapter -->
              <div class="chapter__header dc-u-pr-24 dc-u-pl-24">
                <div class="chapter__title-wrapper">
                  <span class="chapter__number">
                    <span class="chapter-number locked">
                      <span class="
                        dc-icon 
                        dc-icon--size-12
                        dc-icon--currentColor
                        ">
                        <svg class="dc-icon__svg">
                          <use xlink:href="#lock" />
                        </svg>
                      </span>
                    </span>
                  </span>
                  <h4 class="chapter__title">
                    Building deep learning models with keras
                  </h4>
                </div>
              </div>
              <p class="chapter__description dc-u-fxi-fg-1 dc-u-pr-24 dc-u-pl-24 dc-u-mb-18">
                In this chapter, you'll use the Keras library to build deep learning models for both regression and classification. You'll learn about the Specify-Compile-Fit workflow that you can use to make predictions, and by the end of the chapter, you'll have all the tools necessary to build deep neural networks.
              </p>
              <!-- !Header of the chapter -->
              <!-- Body of the chapter -->
              <ul class="chapter__exercises hidden dc-u-ta-center">
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=1">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Creating a  keras model'>Creating a  keras model</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=2">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Understanding your data'>Understanding your data</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=3">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Specifying a model'>Specifying a model</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=4">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Compiling and  fitting a model'>Compiling and  fitting a model</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=5">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Compiling the model'>Compiling the model</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=6">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Fitting the model'>Fitting the model</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=7">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Classification  models'>Classification  models</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=8">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Understanding your classification data'>Understanding your classification data</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=9">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Last steps in classification models'>Last steps in classification models</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=10">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Using models'>Using models</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/building-deep-learning-models-with-keras?ex=11">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Making predictions'>Making predictions</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
              </ul>
              <!-- !Body of the chapter -->
              <!-- Footer of the chapter -->
              <div class="dc-u-b dc-u-bb-none dc-u-br-none dc-u-bl-none dc-u-fx dc-u-pv-16 dc-u-ph-24 chapter__footer--experiment">
                <a class="chapter__footer-btn-link js-toggle-chapter-details--experiment chapter__footer-btn-link-view dc-u-fx-jcc dc-u-fw-regular dc-u-ml-0"
                  href="" data-toggle-details chevron-direction="down">
                View Chapter Details
                <a class="chapter__footer-btn dc-btn dc-btn--secondary" data-url="/sign_in_or_up_modal?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5365%2Fcontinue" href="/users/sign_up?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5365%2Fcontinue">Play Chapter Now</a>
                </a>
              </div>
              <!-- !Footer of the chapter -->
            </li>
            <li class="chapter dc-u-bgc-white dc-card--shadowed-sm dc-u-b-none dc-u-fx dc-u-fx-fdc dc-u-pl-0 dc-u-pr-0 dc-u-pb-0">
              <!-- Header of the chapter -->
              <div class="chapter__header dc-u-pr-24 dc-u-pl-24">
                <div class="chapter__title-wrapper">
                  <span class="chapter__number">
                    <span class="chapter-number locked">
                      <span class="
                        dc-icon 
                        dc-icon--size-12
                        dc-icon--currentColor
                        ">
                        <svg class="dc-icon__svg">
                          <use xlink:href="#lock" />
                        </svg>
                      </span>
                    </span>
                  </span>
                  <h4 class="chapter__title">
                    Fine-tuning keras models
                  </h4>
                </div>
              </div>
              <p class="chapter__description dc-u-fxi-fg-1 dc-u-pr-24 dc-u-pl-24 dc-u-mb-18">
                Learn how to optimize your deep learning models in Keras. Start by learning how to validate your models, then understand the concept of model capacity, and finally, experiment with wider and deeper networks. 
              </p>
              <!-- !Header of the chapter -->
              <!-- Body of the chapter -->
              <ul class="chapter__exercises hidden dc-u-ta-center">
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=1">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Understanding  model  optimization'>Understanding  model  optimization</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=2">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Diagnosing optimization problems'>Diagnosing optimization problems</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=3">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Changing optimization parameters'>Changing optimization parameters</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=4">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Model validation'>Model validation</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=5">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Evaluating model accuracy on validation dataset'>Evaluating model accuracy on validation dataset</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=6">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Early stopping: Optimizing the optimization'>Early stopping: Optimizing the optimization</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=7">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Experimenting with wider networks'>Experimenting with wider networks</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=8">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Adding layers to a network'>Adding layers to a network</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=9">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Thinking  about model  capacity'>Thinking  about model  capacity</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=10">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_mc-69aa2787cd6ac83c3f024a606d566bd22584a3e3ac14866b91fa3eff8ce53349.svg" alt="Icon exercise mc" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Experimenting with model structures'>Experimenting with model structures</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=11">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Stepping up  to images'>Stepping up  to images</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=12">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_interactive-ffbdcfe7ac38bbc150655a01c7c52c1157fb825c6afe296fa68a275133d17d85.svg" alt="Icon exercise interactive" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Building your own digit recognition model'>Building your own digit recognition model</h5>
                    <span class="chapter__exercise-xp">
                    100 xp
                    </span>
                  </a>
                </li>
                <li class="chapter__exercise dc-u-ml-0 dc-u-mr-0 ">
                  <a class="chapter__exercise-link" href="https://campus.datacamp.com/courses/introduction-to-deep-learning-in-python/fine-tuning-keras-models?ex=13">
                    <span class="chapter__exercise-icon exercise-icon locked">
                    <img width="23" height="23" src="https://cdn.datacamp.com/main-app/assets/courses/icon_exercise_video-3b15ea50771db747f7add5f53e535066f57d9f94b4b0ebf1e4ddca0347191bb8.svg" alt="Icon exercise video" />
                    </span>
                    <h5 class="chapter__exercise-title" title='Final thoughts'>Final thoughts</h5>
                    <span class="chapter__exercise-xp">
                    50 xp
                    </span>
                  </a>
                </li>
              </ul>
              <!-- !Body of the chapter -->
              <!-- Footer of the chapter -->
              <div class="dc-u-b dc-u-bb-none dc-u-br-none dc-u-bl-none dc-u-fx dc-u-pv-16 dc-u-ph-24 chapter__footer--experiment">
                <a class="chapter__footer-btn-link js-toggle-chapter-details--experiment chapter__footer-btn-link-view dc-u-fx-jcc dc-u-fw-regular dc-u-ml-0"
                  href="" data-toggle-details chevron-direction="down">
                View Chapter Details
                <a class="chapter__footer-btn dc-btn dc-btn--secondary" data-url="/sign_in_or_up_modal?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5366%2Fcontinue" href="/users/sign_up?redirect=%2Fcourses%2Fintroduction-to-deep-learning-in-python%2Fchapters%2F5366%2Fcontinue">Play Chapter Now</a>
                </a>
              </div>
              <!-- !Footer of the chapter -->
            </li>
          </ol>
          <style>
            .dc-home__story {
            padding: 0 20px 20px;
            }
          </style>
          <h3 class="dc-u-color-grey-oslo dc-u-ta-center dc-u-mb-0 dc-u-mt-64">What do other learners have to say?</h3>
          <div class="container dc-u-mb-64 dc-u-mt-32 dc-u-w-100pc">
            <div class="row dc-u-fx@md">
              <div class="col-md-4 dc-u-fx@md dc-u-pl-0 dc-u-pr-4">
                <section class="dc-card dc-card--bordered dc-home__story">
                  <div class="dc-home__story-img-wrapper dc-headstone">
                    <img srcset="" class="dc-home__story-img dc-u-brad-circle" src="https://cdn.datacamp.com/main-app/assets/stories/devon-befdb70daa1db19864156997ce0e3a7c8215be5e5d13111115b8cb8143f39a50.jpg" alt="Devon" />
                  </div>
                  <div class="dc-u-fxi-fg-1 dc-u-fx dc-u-fx-fdc dc-u-fx-jcsb">
                    <p class="dc-u-fs-h5 dc-u-fst-italic dc-u-color-grey-dark dc-u-mt-16 dc-u-mb-16 dc-u-fxi-fg-1">“I&#39;ve used other sites, but DataCamp&#39;s been the one that I&#39;ve stuck with.”</p>
                    <p class="dc-u-mt-0 dc-u-mb-0">Devon Edwards Joseph</p>
                    <p class="dc-u-mt-0 dc-u-mb-0">Lloyd&#39;s Banking Group</p>
                  </div>
                </section>
              </div>
              <div class="col-md-4 dc-u-fx@md dc-u-pl-4 dc-u-pr-4">
                <section class="dc-card dc-card--bordered dc-home__story">
                  <div class="dc-home__story-img-wrapper dc-headstone">
                    <img srcset="" class="dc-home__story-img dc-u-brad-circle" src="https://cdn.datacamp.com/main-app/assets/stories/louis-1e7bdb5d367ada1689bd328c02d7c740a8d14f11410ddda6cf95d9047f49c5ec.jpg" alt="Louis" />
                  </div>
                  <div class="dc-u-fxi-fg-1 dc-u-fx dc-u-fx-fdc dc-u-fx-jcsb">
                    <p class="dc-u-fs-h5 dc-u-fst-italic dc-u-color-grey-dark dc-u-mt-16 dc-u-mb-16 dc-u-fxi-fg-1">“DataCamp is the top resource I recommend for learning data science.”</p>
                    <p class="dc-u-mt-0 dc-u-mb-0">Louis Maiden</p>
                    <p class="dc-u-mt-0 dc-u-mb-0">Harvard Business School</p>
                  </div>
                </section>
              </div>
              <div class="col-md-4 dc-u-fx@md dc-u-pl-4 dc-u-pr-0">
                <section class="dc-card dc-card--bordered dc-home__story">
                  <div class="dc-home__story-img-wrapper dc-headstone">
                    <img srcset="" class="dc-home__story-img dc-u-brad-circle" src="https://cdn.datacamp.com/main-app/assets/experiments/ronbowers-e85b1497632eb3574ffe1389eba50aa4e6cc6872547eac51a1e19d5317a5b82e.jpg" alt="Ronbowers" />
                  </div>
                  <div class="dc-u-fxi-fg-1 dc-u-fx dc-u-fx-fdc dc-u-fx-jcsb">
                    <p class="dc-u-fs-h5 dc-u-fst-italic dc-u-color-grey-dark dc-u-mt-16 dc-u-mb-16 dc-u-fxi-fg-1">“DataCamp is by far my favorite website to learn from.”</p>
                    <p class="dc-u-mt-0 dc-u-mb-0">Ronald Bowers</p>
                    <p class="dc-u-mt-0 dc-u-mb-0">Decision Science Analytics @ USAA</p>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <!-- Sidebar -->
          <div class="course-sidebar dc-u-mt-32 dc-u-mb-48">
            <div class="course__instructor dc-u-mb-8 dc-u-bgc-white dc-card--shadowed-sm dc-u-b-none dc-u-mr-0 dc-u-ml-0">
              <div class="course__instructor-header">
                <img alt="Dan Becker" class="course__instructor-image" src="https://assets.datacamp.com/users/avatars/000/245/897/square/danbecker.png?1489428668" />
                <div>
                  <h5 class="course__instructor-name">Dan Becker</h5>
                  <p class="course__instructor-occupation">Data Scientist and contributor to Keras and TensorFlow libraries</p>
                </div>
              </div>
              <p class="course__instructor-description">Dan Becker is a data scientist with years of deep learning experience. He has contributed to the Keras and TensorFlow libraries, finishing 2nd (out of 1353 teams) in the $3million Heritage Health Prize competition, and supervised consulting projects for 6 companies in the Fortunate 100. He previously worked as a data scientist at Google. Now he is the CEO and co-founder of Decision.ai, which helps companies apply their machine learning models to make better real-world decisions.</p>
              <a class="dc-btn dc-btn--tertiary" href="/instructors/dansbecker">See More</a>
            </div>
            <div class="dc-u-fx dc-u-fx-jcsb dc-u-fx-ais course-sidebar-flex-container">
              <div class="course__tracks-wrapper dc-u-bgc-white dc-card--shadowed-sm dc-u-fxi-fg-1 dc-u-b-none dc-u-mb-0">
                <h5 class="course__collaborators-title dc-u-mb-0">Tracks</h5>
                <ul class="course__tracks dc-u-pb-0">
                  <li class="course__track">
                    <a class="dc-u-color-primary dc-u-fw-regular link-borderless" href="/tracks/data-science-for-everyone">
                    Data Science for Everyone
                    </a>
                  </li>
                  <li class="course__track">
                    <a class="dc-u-color-primary dc-u-fw-regular link-borderless" href="/tracks/machine-learning-for-everyone">
                    Machine Learning for Everyone
                    </a>
                  </li>
                  <li class="course__track">
                    <a class="dc-u-color-primary dc-u-fw-regular link-borderless" href="/tracks/machine-learning-fundamentals-with-python">
                    Machine Learning Fundamentals with Python
                    </a>
                  </li>
                  <li class="course__track">
                    <a class="dc-u-color-primary dc-u-fw-regular link-borderless" href="/tracks/machine-learning-scientist-with-python">
                    Machine Learning Scientist with Python
                    </a>
                  </li>
                </ul>
              </div>
              <div class="course__collaborators-wrapper dc-u-bgc-white dc-card--shadowed-sm dc-u-fxi-fg-1 dc-u-b-none dc-u-mb-0">
                <h5 class="course__collaborators-title">Collaborators</h5>
                <ul class="course__collaborators">
                  <li class="course__collaborator">
                    <img alt="Hugo Bowne-Anderson" class="course__collaborator-image" src="https://assets.datacamp.com/users/avatars/000/301/837/square/hugoaboutpic.jpg?1493154678" />
                    <p class="course__collaborator-name">Hugo Bowne-Anderson</p>
                  </li>
                  <li class="course__collaborator">
                    <img alt="Yashas Roy" class="course__collaborator-image" src="https://assets.datacamp.com/users/avatars/000/804/054/square/headshot.png?1490022370" />
                    <p class="course__collaborator-name">Yashas Roy</p>
                  </li>
                </ul>
              </div>
              <div class="course__prerequisites-wrapper dc-u-bgc-white dc-card--shadowed-sm dc-u-fxi-fg-1 dc-u-b-none dc-u-mb-0">
                <h5 class="course__prerequisites-title">Prerequisites</h5>
                <ul class="course__prerequisites">
                  <li class="course__prerequisite">
                    <a class="link-borderless" href="/courses/supervised-learning-with-scikit-learn">
                    Supervised Learning with scikit-learn
                    </a>                      
                  </li>
                </ul>
              </div>
              <div class="course__datasets-wrapper dc-u-bgc-white dc-card--shadowed-sm dc-u-fxi-fg-1 dc-u-b-none dc-u-mb-0">
                <h5 class="course__datasets-title">Datasets</h5>
                <ul class="course__datasets">
                  <li class="course__dataset">
                    <a class="link-borderless" target="_blank" rel="noopener noreferrer" href="https://assets.datacamp.com/production/repositories/654/datasets/8a57adcdb5bfb3e603dad7d3c61682dfe63082b8/hourly_wages.csv">
                    Hourly wages
                    </a>                    
                  </li>
                  <li class="course__dataset">
                    <a class="link-borderless" target="_blank" rel="noopener noreferrer" href="https://assets.datacamp.com/production/repositories/654/datasets/24769dae9dc51a77b9baa785d42ea42e3f8f7538/mnist.csv">
                    MNIST
                    </a>                    
                  </li>
                  <li class="course__dataset">
                    <a class="link-borderless" target="_blank" rel="noopener noreferrer" href="https://assets.datacamp.com/production/repositories/654/datasets/92b75b9bc0c0a8a30999d76f4a1ee786ef072a9c/titanic_all_numeric.csv">
                    Titanic
                    </a>                    
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- !Sidebar -->
        </div>
        <!-- !Main Content -->
      </div>
    </div>
</section>
</div>
@endsection