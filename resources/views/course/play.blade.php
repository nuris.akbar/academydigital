<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $lesson->title }} | Academy Digital</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<link href="{{asset('bootstrap4/css/style.css')}}" rel="stylesheet">

  </head>
  <body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Academy Digital</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		</button>
	  
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		  <ul class="navbar-nav mr-auto">
			<li class="nav-item active">
			  <a class="nav-link" href="/">Back To Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#">Help</a>
			</li>
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li class="nav-item"><a class="nav-link" href="#">Selamat Belajar : <b>Nuris Akbar</b></a></li>
			<li class="nav-item"><a class="nav-link" href="#">Logout</a></li>
		  </ul>
		</div>
	  </nav>

	<div class="container-fluid" style="margin-top: 20px;;">
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="/">Home</a>
				</li>
				<li class="breadcrumb-item">
                    <a href="#">{{ $course->title}}</a>
				</li>
				<li class="breadcrumb-item active">
					{{ $lesson->title }}
				</li>
			</ol>
		</nav>
	<div class="row">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<video width="880" controls>
						<source src="{{ $lesson->video_url }}" type="video/mp4">
						Your browser does not support HTML5 video.
					  </video>
				</div>
					<div class="col-md-12" style="margin-top:20px;">
						<h3>
							{{ $lesson->title }}
						</h3>
						<p class="text-justify">
							{{ $lesson->description }}
						</p>
					</div>
				</div>
		</div>
		<div class="col-md-4">
			<div id="card-170144">
				<div class="card">
					<div class="card-header">
						 <a class="card-link" data-toggle="collapse" data-parent="#card-170144" href="#card-element-699209">Collapsible Group Item #1</a>
					</div>
					<div id="card-element-699209" class="collapse show">
						<div class="card-body">
							<p><i class="far fa-play-circle"></i> Cara Melakukan Instalasi Xampp...
								<span style="float: right;"><i class="far fa-clock"></i> 10:40</span>
							</p>
							<hr>
							<p><i class="far fa-play-circle"></i> Melakukan Instalasi Visual Studio Code ...
								<span style="float: right;"><i class="far fa-clock"></i> 10:40</span>
							</p>
							<hr>
							<p><i class="far fa-play-circle"></i> Instalasi Package VsCode HTML Autotag
								<span style="float: right;"><i class="far fa-clock"></i> 10:40</span>
							</p>
							<hr>
							<p><i class="far fa-play-circle"></i> Anim pariatur cliche...
								<span style="float: right;"><i class="far fa-clock"></i> 10:40</span>
							</p>
							<hr>
							<p><i class="far fa-play-circle"></i> Anim pariatur cliche...
								<span style="float: right;"><i class="far fa-clock"></i> 10:40</span>
							</p>
							<hr>
						</div>
						
					</div>
                </div>
                
                @foreach($sections as $section)
				<div class="card">
					<div class="card-header">
                    <a class="collapsed card-link" data-toggle="collapse" data-parent="#card-170144" href="#card-element-{{ $section->id}}">{{ $section->name }}</a>
					</div>
					<div id="card-element-{{ $section->id}}" class="collapse">
						<div class="card-body">
                            @foreach($section->lessons as $lesson)
							<p><i class="far fa-play-circle"></i>
								<a href="/play/{{ $course->slug }}/{{ $lesson->slug }}">
								{{ strlen($lesson->title)>35?substr($lesson->title,0,35).' ...':$lesson->title }}
								</a>
								<span style="float: right;"><i class="far fa-clock"></i> {!! $lesson->duration !!}</span>
							</p>
							<hr>
                            @endforeach
						</div>
					</div>
                </div>
                @endforeach
			</div>
		</div>
	</div>
</div>

    <script src="{{asset('bootstrap4/js/jquery.min.js')}}"></script>
    <script src="{{asset('bootstrap4/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('bootstrap4/js/scripts.js')}}"></script>
  </body>
</html>