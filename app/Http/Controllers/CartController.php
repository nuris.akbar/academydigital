<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use Illuminate\Support\Str;
use App\Repositories\Eloquent\EloquentOrderRepository;
use App\Repositories\Eloquent\EloquentDiscountRepository;

class CartController extends Controller
{

    public function index()
    {
        //return $this->invoiceNumber();

        if(\Auth::user()==null)
        {
            return redirect('course/')->with('message','Harus Login Dulu');
        }else
        {
            $data['items'] = \Cart::session(\Auth::user()->id)->getContent();
            $data['total'] = \Cart::getTotal();
            $data['accountNumber'] = \App\Models\AccountNumber::pluck('bank_name','id');
            return view('cart.index',$data);
        }
    }

    public function store(Request $request)
    {
        $course = Course::find($request->course_id);

        if(\Auth::user()==null)
        {
            return redirect('course/'.$course->slug)->with('message','Harus Login Dulu');
        }else
        {
            // add the product to cart
            \Cart::session(\Auth::user()->id)->add(array(
                'id'                => (string) Str::uuid(),
                'name'              => $course->title,
                'price'             => $course->sale_price,
                'quantity'          => 1,
                'attributes'        => array(),
                'associatedModel'   => $course
            ));
            return redirect('/cart');
        }
    }

    function invoiceNumber()
    {
        $char = "INV/".date('mY').'/';
        $order = \DB::select("SELECT max(invoice) as maxInvoice FROM orders where left(invoice,11)='".$char."'");
        $maxInvoice = $order[0]->maxInvoice;
        $noUrut = (int) substr($maxInvoice, 11, 4);
        $noUrut++;
        return $char . sprintf("%03s", $noUrut);
    }

    public function applyDiscount(Request $request, EloquentDiscountRepository $discountRepo)
    {
        if(isset($request->discount_code))
        {
            $discount = $discountRepo->findWhere('discount_code',$request->discount_code);
            return $discount;
            return redirect('/cart')->with('message','Berhasil Apply Diskon');

        }else
        {
            return redirect('/cart')->with('message','Kode Kosong');
        }
    }

    public function checkout(Request $request, EloquentOrderRepository $orderRepo)
    {
        

        return $request->all();
        $invoice    = $this->invoiceNumber();

        \Cart::session(\Auth::user()->id)->getContent();
        $order = [
            'user_id'               =>  \Auth::user()->id,
            'account_number_id'     =>  $request->account_number_id,
            'note'                  =>  $request->note,
            'payment_status'        =>  'pending',
            'invoice'               =>  $invoice,
            'total'                 =>  \Cart::getTotal()
        ];

       $orderResult = $orderRepo->create($order);

       // Order Detail
       $items = \Cart::session(\Auth::user()->id)->getContent();

       foreach($items as $item)
       {
           $detail = [
                        'course_id' =>  $item->associatedModel->id,
                        'invoice'   =>  $invoice,
                        'price'     =>  $item->price
                    ];

           \App\Models\OrderDetail::create($detail);
       }

       \Mail::to(\Auth::user()->email)->send(new \App\Mail\OrderNotification($orderResult));


        \Cart::clear();
        \Cart::session(\Auth::user()->id)->clear();
    }

    public function destroy($id)
    {
        \Cart::session(\Auth::user()->id)->remove($id);
        return redirect('cart');
    }
}
