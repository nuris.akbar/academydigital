<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    // halaman verifikasi user
    public function verification()
    {
        return view('page.verification');
    }

    // halaman index
    public function index(){
        return view('page.index');
    }
}
