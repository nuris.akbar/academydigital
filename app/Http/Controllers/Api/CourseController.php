<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentCourseRepository;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseDetailResource;

class CourseController extends Controller
{
    public $courseRepo;

    public function __construct(EloquentCourseRepository $courseRepo)
    {
        $this->courseRepo = $courseRepo;
    }

    public function index()
    {
        return CourseResource::collection($this->courseRepo->all());       
    }

    public function show($slug)
    {
        return new CourseDetailResource(\App\Models\Course::where('slug',$slug)->first());
    }
}
