<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentCategoryRepository;

class CategoryController extends Controller
{
    public $categoryRepo;

    public function __construct(EloquentCategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function index()
    {

    }
}
