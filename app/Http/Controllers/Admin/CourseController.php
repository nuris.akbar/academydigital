<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentCourseRepository;
use App\Repositories\Eloquent\EloquentSectionRepository;
use App\Repositories\Eloquent\EloquentLessonRepository;
use App\Repositories\Eloquent\EloquentFollowCourseRepository;
use App\Http\Requests\CourseCreate;
use App\user;
use App\Models\Category;
use Kurt\Repoist\Repositories\Eloquent\Criteria\EagerLoad;

class CourseController extends Controller
{
    public $categoryRepo;

    public function __construct(EloquentCourseRepository $courseRepo)
    {
        $this->middleware('auth');
        $this->courseRepo = $courseRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['courses'] = $this->courseRepo->all();
        return view('admin.course.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users']      = User::where('role','trainer')->pluck('name', 'id');
        $data['categories'] = Category::pluck('name', 'id');
        return view('admin.course.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseCreate $request)
    {
        //$file       = $request->file('video_preview_url');
        //\Storage::disk('do_spaces')->putFile('video', request()->file('video_preview_url'), 'public');

        $input                      = $request->all();
        $input['slug']              = \Str::slug($request->title, '-');
        //$input['video_preview_url'] = $file->getClientOriginalName();
        $this->courseRepo->create($input);
        return redirect(route('adminzone.course.index'))->with('message','Success Add New Course With Title '.$request->title);
    }

    public function upload_video($request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, EloquentSectionRepository $sectionRepo)
    {
        $data['course']     = $this->courseRepo->find($id);
        $data['sections']   = $sectionRepo->withCriteria([
        	new EagerLoad(['lessons']),
        ])->findWhere('course_id',$id);
        return view('admin.course.section',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['course']     =   $this->courseRepo->find($id);
        $data['users']      =   User::where('role','trainer')->pluck('name', 'id');
        $data['categories'] =   Category::pluck('name', 'id');
        return view('admin.course.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input          = $request->all();
        // if ($request->hasFile('video_preview_url')) {
        //     $file       = $request->file('video_preview_url');
        //     \Storage::disk('do_spaces')->putFile('video', request()->file('video_preview_url'), 'public');
        //     $input['video_preview_url'] = $file->getClientOriginalName();
        // }
        
        $input['slug']  = \Str::slug($request->title, '-');
        $this->courseRepo->update($id,$input);
        return redirect(route('adminzone.course.index'))->with('message','Success Update Course With Title '.$request->title);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, EloquentLessonRepository $lessonRepo, EloquentSectionRepository $sectionRepo)
    {
        $this->courseRepo->delete($id);
        \App\Models\Lesson::where('course_id',$id)->delete();
        \App\Models\Section::where('course_id',$id)->delete();
        return redirect(route('adminzone.course.index'))->with('message','Success Delete Course');
    }

    public function follow($id, EloquentFollowCourseRepository $followRepo)
    {
        $data['follows'] = $followRepo->findWhere('user_id',$id);
        return view('admin.user.follow',$data);
    }
}
