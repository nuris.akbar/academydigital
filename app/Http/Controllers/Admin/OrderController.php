<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentOrderRepository;

class OrderController extends Controller
{
    public $orderRepo;

    public function __construct(EloquentOrderRepository $orderRepo)
    {
        $this->middleware('auth');
        $this->orderRepo = $orderRepo;
    }

    public function index()
    {
        $data['orders'] = $this->orderRepo->all();
        return view('admin.order.index',$data);
    }

    public function show($invoice)
    {
        $data['order'] = $this->orderRepo->findWhere('invoice',$invoice)->first();
        return view('admin.order.show',$data);
    }

    public function update($invoice,Request $request)
    {
        if($request->payment_status=='success')
        {
            $order = $this->orderRepo->findWhere('invoice',$invoice)->first();
            \Mail::to($request->user_email)->send(new \App\Mail\PaymentSuccessConfirmation($order));
        }
        
        $this->orderRepo->update($invoice,$request->all());
        return redirect()->route('adminzone.order.show',['id'=>$invoice])->with('message','Success Update Payment Status To '.$request->payment_status);
    }
}
