<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentUserRepository;
use Auth;

class AuthController extends Controller
{
    public function formLogin()
    {
        return view('admin.login');
    }

    public function login(Request $request, EloquentUserRepository $userRepo)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect('adminzone/dashboard');
        }else
        {
            return redirect('adminzone/login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/adminzone/login');
    }
}
