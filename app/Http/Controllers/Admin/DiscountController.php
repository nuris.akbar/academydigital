<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentDiscountRepository;
use App\Http\Requests\DiscountCreate;

class DiscountController extends Controller
{
    
    public $discountRepo;

    public function __construct(EloquentDiscountRepository $discountRepo)
    {
        $this->middleware('auth');
        $this->discountRepo = $discountRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['discounts'] = $this->discountRepo->all();
        return view('admin.discount.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.discount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscountCreate $request)
    {
        $this->discountRepo->create($request->all());
        return redirect(route('adminzone.discount.index'))->with('message','Success Add New discount With Name '.$request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['discount'] = $this->discountRepo->find($id);
        return view('admin.discount.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input          = $request->all();
        $input['slug']  = \Str::slug($request->name, '-');
        $this->discountRepo->update($id,$input);
        return redirect(route('adminzone.discount.index'))->with('message','Success Update discount With Name '.$request->name);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->discountRepo->delete($id);
        return redirect(route('adminzone.discount.index'))->with('message','Success Delete discount');
    }
}
