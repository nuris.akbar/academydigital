<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentCourseRepository;
use App\Repositories\Eloquent\EloquentSectionRepository;
class SectionController extends Controller
{
    public $sectionRepo;

    public function __construct(EloquentSectionRepository $sectionRepo)
    {
        $this->sectionRepo = $sectionRepo;
    }

    public function show($id, EloquentCourseRepository $courseRepo)
    {
        $data['course'] = $courseRepo->find($id);
        return view('admin.section.create',$data);
    }

    public function store(Request $request)
    {
        $this->sectionRepo->create($request->all());
        return redirect(route('adminzone.course.show',['id'=>$request->course_id]))->with('message','Success Add New Section With Name '.$request->name);
    }

    public function edit($id, EloquentCourseRepository $courseRepo)
    {
        $data['section']    = $this->sectionRepo->find($id);
        $data['course']     = $courseRepo->find($data['section']->course_id);
        return view('admin.section.edit',$data);
    }

    public function update($id, Request $request)
    {
        $this->sectionRepo->update($id,$request->all());
        return redirect(route('adminzone.course.show',['id'=>$request->course_id]))->with('message','Update Section With Name '.$request->name.' Has Updated');
    }

    public function destroy($id)
    {
        $section = $this->sectionRepo->find($id);
        $this->sectionRepo->delete($id);
        return redirect(route('adminzone.course.show',['id'=>$section->course_id]))->with('message','A Section With Name '.$section->name.' Has Deleted');
    }
}
