<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\EloquentCourseRepository;
use App\Repositories\Eloquent\EloquentLessonRepository;

class LessonController extends Controller
{
    public $lessonRepo;

    public function __construct(EloquentLessonRepository $lessonRepo)
    {
        $this->lessonRepo = $lessonRepo;
    }

    public function show($id, EloquentCourseRepository $courseRepo)
    {
        $data['course'] = $courseRepo->find($id);
        $data['sections'] = \App\Models\Section::where('course_id',$data['course']->id)->pluck('name','id');
        return view('admin.lesson.create',$data);
    }

    public function store(\App\Http\Requests\LessonCreateRequest $request)
    {
        $input          = $request->all();
        $input['slug']  = \Str::slug($request->title, '-');
        $this->lessonRepo->create($input);
        return redirect(route('adminzone.course.show',['id'=>$request->course_id]))->with('message','Success Add New Lesson With Title '.$request->title);
    }

    public function edit($id, EloquentCourseRepository $courseRepo)
    {
        $data['lesson'] = $this->lessonRepo->find($id);
        $data['section'] = $data['lesson']->section->name;
        $data['course'] = $courseRepo->findWhere('id',$data['lesson']->course_id)->first();
        $data['sections'] = \App\Models\Section::where('course_id',$data['lesson']->course_id)->pluck('name','id');
        return view('admin.lesson.edit',$data);
    }

    public function update($id,Request $request)
    {
        $input          = $request->all();
        $input['slug']  = \Str::slug($request->title, '-');
        return $input;
        $this->lessonRepo->update($id,$input);
        return redirect(route('adminzone.course.show',['id'=>$request->course_id]))->with('message','Success Add New Lesson With Title '.$request->title);
    }

    public function destroy($id)
    {
        $lesson = $this->lessonRepo->find($id);
        $this->lessonRepo->delete($id);
        return redirect(route('adminzone.course.show',['id'=>$lesson->course_id]))->with('message','A Lesson With Title '.$lesson->title.' Has Deleted');
    }
}
