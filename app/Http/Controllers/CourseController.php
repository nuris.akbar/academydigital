<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\EloquentCourseRepository;
use App\Models\Section;
use App\Models\Lesson;

class CourseController extends Controller
{
    public $courseRepo;

    public function __construct(EloquentCourseRepository $courseRepo)
    {
        $this->courseRepo = $courseRepo;
    }

    public function index(){
        $data['courses'] = $this->courseRepo->findWhere('publish',1)->take(10);
        return view('courses.index',$data);
    }

    public function show($slug)
    {
        $data['course']   = $this->courseRepo->findWhere('slug',$slug)->first();
        $data['sections'] = Section::all();
        return view('course.show',$data);
    }


    public function play($courseSlug,$lessonSlug=null)
    {
        if($lessonSlug==null)
        {
            $course = $this->courseRepo->findWhere('slug',$courseSlug)->first();
            $data['lesson']     = \App\Models\Lesson::where('course_id',$course->id)->first();
        }else
        {
            $data['lesson']     = \App\Models\Lesson::where('slug',$lessonSlug)->first();
        }

        if($data['lesson']==null)
        {
            abort(404);
        }
        
        $data['course']     = \App\Models\Course::find($data['lesson']->course_id);
        $data['sections']   = $data['course']->sections;
        return view('course.play',$data);
    }
}
