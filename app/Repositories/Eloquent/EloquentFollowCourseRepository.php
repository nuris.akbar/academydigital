<?php

namespace App\Repositories\Eloquent;

use App\Models\FollowCourse;
use App\Repositories\Contracts\FollowCourseRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentFollowCourseRepository extends AbstractRepository implements FollowCourseRepository
{
    public function entity()
    {
        return FollowCourse::class;
    }
}
