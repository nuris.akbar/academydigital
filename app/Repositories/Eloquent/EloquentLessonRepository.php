<?php

namespace App\Repositories\Eloquent;

use App\Models\Lesson;
use App\Repositories\Contracts\LessonRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentLessonRepository extends AbstractRepository implements LessonRepository
{
    public function entity()
    {
        return Lesson::class;
    }
}
