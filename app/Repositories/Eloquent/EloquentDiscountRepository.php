<?php

namespace App\Repositories\Eloquent;

use App\Models\Discount;
use App\Repositories\Contracts\DiscountRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentDiscountRepository extends AbstractRepository implements DiscountRepository
{
    public function entity()
    {
        return Discount::class;
    }
}
