<?php

namespace App\Repositories\Eloquent;

use App\Models\Section;
use App\Repositories\Contracts\SectionRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentSectionRepository extends AbstractRepository implements SectionRepository
{
    public function entity()
    {
        return Section::class;
    }
}
