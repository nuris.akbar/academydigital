<?php

namespace App\Repositories\Eloquent;

use App\Models\Course;
use App\Repositories\Contracts\CourseRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentCourseRepository extends AbstractRepository implements CourseRepository
{
    public function entity()
    {
        return Course::class;
    }
}
