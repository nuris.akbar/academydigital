<?php

namespace App\Repositories\Eloquent;

use App\Models\Order;
use App\Repositories\Contracts\OrderRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentOrderRepository extends AbstractRepository implements OrderRepository
{
    public function entity()
    {
        return Order::class;
    }
}
