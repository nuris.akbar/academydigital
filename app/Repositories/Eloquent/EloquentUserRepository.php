<?php

namespace App\Repositories\Eloquent;

use App\User;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Support\Facades\Hash;
use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentUserRepository extends AbstractRepository implements UserRepository
{
    public function entity()
    {
        return User::class;
    }

    public function ByRole(array $role)
    {
        return User::whereIn('role',$role)->where('verified',1)->get();
    }

    public function orders($user_id)
    {
        return \App\Models\Order::where('user_id',$user_id)
        ->select('orders.invoice','orders.note','orders.total','orders.created_at','orders.payment_status','account_numbers.account_name','account_numbers.account_number','account_numbers.bank_name')
        ->leftJoin('account_numbers','account_numbers.id','orders.account_number_id')
        ->orderBy('orders.created_at','DESC')
        ->get();
    }
    

    public function register($request)
    {
        if ($request->hasFile('photo')) {
            $photo              = $request->file('user_photo');
            $nama_file          = $photo->getClientOriginalName();
            $photo->move('users',$nama_file);
        }else
        {
            $nama_file = '';
        }

        $user               = new User();
        $user->name         = $request->name;
        $user->slug         = \Str::slug($request->name, '-');
        $user->email        = $request->email;
        $user->description  = $request->description;
        $user->role         = $request->role;
        $user->verified     = $request->role=='member'?0:1;
        $user->password     = Hash::make($request->password);
        $user->photo        = $nama_file;
        return $user->save();
    }
}
