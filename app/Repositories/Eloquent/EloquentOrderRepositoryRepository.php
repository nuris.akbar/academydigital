<?php

namespace App\Repositories\Eloquent;

use App\OrderRepository;
use App\Repositories\Contracts\OrderRepositoryRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentOrderRepositoryRepository extends AbstractRepository implements OrderRepositoryRepository
{
    public function entity()
    {
        return OrderRepository::class;
    }
}
