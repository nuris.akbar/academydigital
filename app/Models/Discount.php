<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = ['title','discount_code','date_start','date_end','value','type'];

    // public function getDateStartAttribute($value)
    // {
    //     return date_format(date_create($value),"d M Y");
    // }

    // public function getDateEndAttribute($value)
    // {
    //     return date_format(date_create($value),"d M Y");
    // }
}
