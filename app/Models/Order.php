<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'invoice';
    public $incrementing = false;

    public function getCreatedAtAttribute($value)
    {
        return date_format(date_create($value),"d M Y");
    }

    public function accountNumber(){
        return $this->belongsTo('App\Models\AccountNumber','account_number_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function orderDetails(){
        return $this->hasMany('App\Models\OrderDetail','invoice','invoice');
    }


    
    protected $fillable = ['user_id','note','invoice','account_number_id','total','payment_status'];
}
