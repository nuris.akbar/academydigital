<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

class Course extends Model
{
    protected $fillable = [
        'title', 'slug', 'description','note','publish','user_id','category_id','price','sale_price','video_preview_url','short_description'
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value, '-');
    }

    public function getCreatedAtAttribute($value)
    {
        return date_format(date_create($value),"d M Y");
    }

    public function _getVideoPreviewUrlAttribute($value)
    {
        return \Storage::disk('do_spaces')->url($value);
    }

    
    public function user()
    {
        return $this->belongsTo('App\User')->select('name','slug','description','photo');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category')->select('name','slug');
    }

    public function sections()
    {
        return $this->hasMany('App\Models\Section')->select('id','name');    
    }

    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
}
}
