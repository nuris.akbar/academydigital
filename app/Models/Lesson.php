<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = ['title','slug','section_id','course_id','duration','video_url','description','public'];

    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }
}
