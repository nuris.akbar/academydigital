<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['course_id','price','invoice'];

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }
}
