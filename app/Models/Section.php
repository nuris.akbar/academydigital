<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable=['course_id','name','description'];

    public function lessons()
    {
        return $this->hasMany('App\Models\Lesson')->select('id','title','slug','video_url','public','section_id','duration');
    }
}
