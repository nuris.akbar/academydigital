<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentSuccessConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->add_course_to_user();
        return $this->view('email.paymentSuccessConfirmation',['order'=>$this->order]);
    }

    public function add_course_to_user()
    {
        $course = [];
        foreach($this->order->orderDetails as $item)
        {
            $course[] = [
                'course_id'     =>  $item->course_id,
                'user_id'       =>  $this->order->user_id,
                'created_at'    =>now(),
                'updated_at'    =>now()
            ];
        }
        \DB::table('follow_courses')->insert($course);
    }
}
