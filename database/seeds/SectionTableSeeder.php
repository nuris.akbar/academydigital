<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Model\Section;
class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $courses = \DB::table('courses')->get();
        $section = [];
        foreach ($courses as $course)
        {
            for($i=1;$i<=5;$i++)
            {
                $section[] = ['name'=>$faker->sentence($nbWords = 6, $variableNbWords = true),'description'=>$faker->sentence($nbWords = 12, $variableNbWords = true),'course_id'=>$course->id,'created_at'=>now(),'updated_at'=>now()];
            }
        }

        \DB::table('sections')->insert($section);
    }
}
