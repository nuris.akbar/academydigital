<?php

use Illuminate\Database\Seeder;
use App\Model\Lesson;
use Faker\Generator as Faker;
class LessonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $sections = \DB::table('sections')->get();
        $lesson = [];
        foreach ($sections as $section){
            for($i=1;$i<=5;$i++)
            {
                $title      = $faker->sentence($nbWords = 6, $variableNbWords = true);
                $slug       = \Str::slug($title, '-');
                $lesson[] = [
                    'title'         =>  $title,
                    'slug'          =>  $slug,
                    'course_id'     =>  $section->course_id,
                    'section_id'    =>  $section->id,
                    'description'   =>  $faker->sentence($nbWords = 20, $variableNbWords = true),
                    'public'        =>  0,
                    'video_url'     => 'video.mp4',
                    'duration'      => '04:30',
                    'created_at'    =>  now(),
                    'updated_at'    =>  now()
                ];
            }
        }

        \DB::table('lessons')->insert($lesson);
    }
}
