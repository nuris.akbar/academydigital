<?php

use Illuminate\Database\Seeder;

class FollowCourse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user   = \DB::table('users')->first();
        $course = \DB::table('courses')->take(5)->get();
        $followCourse = [];
        foreach ($course as $row)
        {
            $followCourse[] = ['user_id'=>$user->id,'course_id'=>$course->id,'created_at'=>now(),'updated_at'=>now()];
        }
        \DB::table('follow_courses')->insert($followCourse);
    }
}
