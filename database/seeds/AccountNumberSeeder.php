<?php

use Illuminate\Database\Seeder;

class AccountNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accountBank = [
            ['bank_name'=>'Bank Central Asia','account_name'=>'Nuris Akbar','account_number'=>'1571432043','created_at'=>now(),'updated_at'=>now()],
            ['bank_name'=>'Bank Rakyat Indonesia','account_name'=>'Nuris Akbar','account_number'=>'329201012325533','created_at'=>now(),'updated_at'=>now()]
        ];
        \DB::table('account_numbers')->insert($accountBank);
    }
}
