<?php

use Illuminate\Database\Seeder;
use App\Repositories\Eloquent\EloquentUserRepository;
Use App\User;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(EloquentUserRepository $userRepo)
    {
        User::truncate();
        $user = [];
        $user[] = ['email'=>'admin@gmail.com','password'=>Hash::make('password'),'name'=>'Administrator','slug'=>'administrator','role'=>'administrator'];
        $user[] = ['email'=>'nuris.akbar@gmail.com','password'=>Hash::make('password'),'name'=>'Nuris Akbar','slug'=>'nuris-akbar','role'=>'member'];
        \DB::table('users')->insert($user);
    }
}
