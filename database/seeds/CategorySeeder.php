<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        $categories  = [
            ['name'=>'Internet Marketing','slug'=>'internet-marketing','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:')],
            ['name'=>'Web Programming','slug'=>'web-programming','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:')],
            ['name'=>'Mobile Programming','slug'=>'mobile-programming','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:')],
            ['name'=>'UX And Desain','slug'=>'ux-and-desain','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:')]
        ];

        \DB::table('categories')->insert($categories);
    }
}
