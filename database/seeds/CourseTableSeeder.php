<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::truncate();
        factory('App\Models\Course',30)->create();
    }
}
